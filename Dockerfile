FROM node:12.14.0-alpine3.11

WORKDIR /usr/src/app
RUN apk add gdal
RUN apk add python2
RUN apk add make
RUN apk add gcc
RUN apk add g++
COPY package.json .
RUN npm install
COPY . .

CMD ["npm", "start"]
