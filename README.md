# Niffler
![Coverage Report](https://srchetwynd.gitlab.io/niffler-database-and-api/coverage.svg)
![![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)
![Pipeline](https://gitlab.com/srchetwynd/niffler-database-and-api/badges/master/pipeline.svg)
![![Code Style](https://img.shields.io/badge/Code_Style-Prettier-ff69b4.svg)](https://prettier.io/)

## Overview

Niffler is an all in one dedicated archaeological fieldwork data storage tool
powered by a PostgreSQL database with a RESTful api. Intended for a straight to
digital method of site recording.

## This repository

This repository contains just the Database creation code and the API, nothing 
more.

###### more to follow as development continues.
