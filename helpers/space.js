//const aws = require("aws-sdk");
//const config = require("../config");
//
//const space = new aws.S3({
//    endpoint: new aws.Endpoint("fra1.digitaloceanspaces.com"),
//    accessKeyId: config.spaceKey,
//    secretAccessKey: config.spaceSecret,
//});
//
///**
// * @param file
// * @param doSpace
// */
//function upload(file, doSpace = space) {
//    return new Promise((resolve, reject) => {
//        doSpace.upload(
//            {
//                Bucket: config.spaceName,
//                Key: file.name,
//                Body: file.content,
//            },
//            (err, data) => {
//                if (err) {
//                    reject(err);
//                } else {
//                    resolve(data);
//                }
//            }
//        );
//    });
//}
//
///**
// * @param key
// * @param doSpace
// */
//function download(key, doSpace = space) {
//    return new Promise((resolve, reject) => {
//        doSpace.getObject(
//            {
//                Bucket: config.spaceName,
//                Key: key,
//            },
//            (err, data) => {
//                if (err) {
//                    reject(err);
//                } else {
//                    resolve(data);
//                }
//            }
//        );
//    });
//}
//
///**
// * @param key
// * @param doSpace
// */
//function getDownloadLink(key, doSpace = space) {
//    return doSpace.getSignedUrlPromise("getObject", {
//        Expires: 360,
//        Key: key,
//        Bucket: config.spaceName,
//    });
//}
//
///**
// * @param key
// * @param doSpace
// */
//function deleteFromSpace(key, doSpace = space) {
//    return new Promise((resolve, reject) => {
//        doSpace.deleteObject(
//            {
//                Bucket: config.spaceName,
//                Key: key,
//            },
//            (err, data) => {
//                if (err) {
//                    reject(err);
//                } else {
//                    resolve(data);
//                }
//            }
//        );
//    });
//}
//
//module.exports = {
//    upload,
//    download,
//    deleteFromSpace,
//    getDownloadLink,
//};
