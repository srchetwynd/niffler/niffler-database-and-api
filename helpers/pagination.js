//const { LinkObject } = require("../components/JsonApi");
//
///**
// * @function getPagination
// * @description gets the pagination details from the request
// * @param {object} arg - argumnets object
// * @param {Array} arg.data - array of data for response
// * @param {object} arg.req - express request object
// * @returns {object} - containing pagination links
// */
//function getPagination({ data, req }) {
//    let next, previous;
//    if (
//        Array.isArray(data) &&
//        data.length === Number(req.limit) &&
//        req.fullURL.includes("offset=")
//    ) {
//        next = req.fullURL.replace(
//            "offset=" + req.offset,
//            "offset=" + (Number(req.offset) + Number(req.limit))
//        );
//    } else if (
//        Array.isArray(data) &&
//        data.length === Number(req.limit) &&
//        req.fullURL.includes("limit=")
//    ) {
//        next = req.fullURL + "&offset=" + (req.offset + req.limit);
//    } else if (data.length === Number(req.limit) && Array.isArray(data)) {
//        next =
//            req.fullURL +
//            "?offset=" +
//            (req.offset + req.limit) +
//            "&limit=" +
//            req.limit;
//    } else {
//        next = null;
//    }
//
//    const previousNum = req.offset - req.limit;
//    if (
//        previousNum > -1 &&
//        Array.isArray(data) &&
//        Number(req.offset) !== 0 &&
//        req.fullURL.includes("offset=")
//    ) {
//        previous = req.fullURL.replace(
//            "offset=" + req.offset,
//            "offset=" + previousNum
//        );
//    } else {
//        previous = null;
//    }
//    return { next, previous };
//}
//
///**
// * @param root0
// * @param root0.next
// * @param root0.previous
// */
//function makePaginationLinks({ next, previous }) {
//    let links = {};
//    if (next) {
//        links.next = new LinkObject().setHref(next).setMethod("GET").build();
//    }
//    if (previous) {
//        links.previous = new LinkObject()
//            .setHref(previous)
//            .setMethod("GET")
//            .build();
//    }
//
//    return links;
//}
//
///**
// * @param {...any} funcs
// */
//function pipe(...funcs) {
//    return (val) => funcs.reduce((x, f) => f(x), val);
//}
//
//const getLinks = pipe(getPagination, makePaginationLinks);
//
//module.exports = {
//    getPagination,
//    makePaginationLinks,
//    getLinks,
//};
