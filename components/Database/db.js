const options = {
    // query(e) {
    //     console.log(e.query);
    // },
    noLocking: true,
};
const pgp = require("pg-promise")(options);

const config = require("../../config");

module.exports = pgp({
    connectionString: config.pgString,
    ssl: true,
});
