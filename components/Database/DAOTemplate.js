//const db = require("./db");
//
//class DAO {
//    constructor() {
//        this.db = db;
//        this.table = null;
//        this.schema = null;
//        this.columns = null;
//        this.columnNames = null;
//        this.sparseFields = [];
//        this.limit = 10;
//        this.offset = 0;
//    }
//
//    setDatabase(database) {
//        this.db = database;
//        return this;
//    }
//
//    getColumnNames() {
//        if (!this.columnNames) {
//            this.columnNames = Object.keys(this.columns);
//        }
//        return this.columnNames;
//    }
//
//    setSchema(comp) {
//        this.schema = comp;
//        return this;
//    }
//
//    setLimit(limit) {
//        this.limit = Number(limit);
//        return this;
//    }
//
//    setOffset(offset) {
//        this.offset = Number(offset);
//        return this;
//    }
//
//    setColumns(args, val) {
//        if (typeof args === "object") {
//            Object.keys(args)
//                .map((el) => {
//                    this.isValidColumnName(el);
//                    return el;
//                })
//                .map((el) => ({ name: el, value: args[el] }))
//                .map((el) => {
//                    this.validateColumn(el);
//                    return el;
//                })
//                .forEach((el) => (this.columns[el.name].value = el.value));
//        } else {
//            this.isValidColumnName(args);
//            this.validateColumn({ name: args, value: val });
//            this.columns[args].value = val;
//        }
//        return this;
//    }
//
//    getValues(incNull = false) {
//        return this.getColumnNames()
//            .filter((el) => this.columns[el].value !== null || incNull)
//            .map((el) => ({ [el]: this.columns[el].value }));
//    }
//
//    setSparseFields(fields) {
//        if (Array.isArray(fields)) {
//            fields
//                .map((el) => {
//                    this.isValidColumnName(el);
//                    return el;
//                })
//                .map((el) => {
//                    this.sparseFields.push(el);
//                    return el;
//                });
//        } else {
//            this.isValidColumnName(fields);
//            this.sparseFields.push(fields);
//        }
//        return this;
//    }
//
//    getSparseFields() {
//        return this.sparseFields.length > 0
//            ? this.sparseFields
//            : this.getColumnNames();
//    }
//
//    isValidColumnName(fieldName) {
//        if (this.getColumnNames().includes(fieldName)) {
//            return fieldName;
//        } else {
//            throw new Error(`Unkown Column Name: ${fieldName}`, __filename);
//        }
//    }
//
//    validateColumn(col) {
//        const isValid = this.columns[col.name].validators
//            .filter((el) => typeof el !== "undefined")
//            .reduce((acc, f) => f(col.value.toString()) && acc, true);
//
//        if (!isValid) {
//            throw new Error(`Invalid Column: ${col.name}, Value: ${col.value}`);
//        }
//    }
//
//    validateQuery(queryType) {
//        if (this.schema === null) {
//            throw new Error("Schema not provided", __filename);
//        }
//        if (this.table === null) {
//            throw new Error("Table not provided", __filename);
//        }
//        const inValidColumns = this.getColumnNames()
//            .filter((el) => this.columns[el].required.includes(queryType))
//            .filter(
//                (el) =>
//                    this.columns[el].value === null ||
//                    this.columns[el].value === undefined
//            );
//        if (inValidColumns.length > 0) {
//            throw new Error(
//                `Columns required but not provided: ${inValidColumns.toString()}`,
//                __filename
//            );
//        }
//    }
//
//    hasUnique(col) {
//        let colNames;
//        if (Array.isArray(col)) {
//            colNames = col;
//        } else if (typeof col === "object") {
//            colNames = [col];
//        } else {
//            colNames = this.getColumnNames();
//        }
//        const cols = colNames.filter(
//            (el) => this.columns[el].unique && this.columns[el].value !== null
//        );
//        if (cols.length === 0) {
//            throw new Error("No unique column specified");
//        }
//    }
//
//    fetchOne() {
//        this.validateQuery("fetchOne");
//        this.hasUnique();
//        let sql = `
//            SELECT
//                $1~
//            FROM
//                $2~.$3~
//            WHERE
//        `;
//        let values = this.getValues();
//        sql = values.reduce(
//            (acc, el, index) =>
//                acc + ` $${index + 4}:name = $${index + 4}:csv AND `,
//            sql
//        );
//        sql = sql.slice(0, -4);
//        return this.db.oneOrNone(sql, [
//            this.getSparseFields(),
//            this.schema,
//            this.table,
//            ...values,
//        ]);
//    }
//
//    fetchAll() {
//        this.validateQuery("fetchAll");
//        let sql = `
//            SELECT
//                $1~
//            FROM
//                $2~.$3~
//        `;
//        let values = this.getValues();
//        if (values.length > 0) {
//            sql += " WHERE ";
//            sql = values.reduce(
//                (acc, el, index) =>
//                    acc + ` $${index + 4}:name = $${index + 4}:csv AND `,
//                sql
//            );
//            sql = sql.slice(0, -4);
//        }
//        sql += ` LIMIT $${values.length + 4} OFFSET $${values.length + 5}`;
//        return this.db.any(sql, [
//            this.getSparseFields(),
//            this.schema,
//            this.table,
//            ...values,
//            this.limit,
//            this.offset,
//        ]);
//    }
//
//    insert() {
//        this.validateQuery("insert");
//        const sql = `
//            INSERT INTO $1~.$2~
//                ($3:name)
//            VALUES
//                ($3:csv)
//            RETURNING $4~
//        `;
//        const values = this.getValues().reduce((acc, el) => ({
//            ...el,
//            ...acc,
//        }));
//        return this.db.any(sql, [
//            this.schema,
//            this.table,
//            values,
//            this.getSparseFields(),
//        ]);
//    }
//
//    update(where, force = false) {
//        this.validateQuery("update");
//        this.hasUnique(Object.keys(where));
//        let sql = `
//            UPDATE $1~.$2~
//        `;
//        const values = this.getValues(force);
//        sql = values.reduce(
//            (acc, el, index) => `SET $${index + 3}:name = $${index + 3}:csv`,
//            sql
//        );
//        sql += `
//            WHERE
//                $${values.length + 3}~ = $${values.length + 4}
//            RETURNING
//                $${values.length + 5}~
//            `;
//        return this.db.one(sql, [
//            this.schema,
//            this.table,
//            ...values,
//            where[Object.keys(where)[0]],
//            where[Object.values(where)[0]],
//            this.getSparseFields(),
//        ]);
//    }
//
//    delete() {
//        this.validateQuery();
//        this.hasUnique();
//        let sql = `
//            DELETE FROM $1~.$2~ WHERE
//        `;
//        const values = this.getValues();
//        sql = values.reduce(
//            (acc, el, index) => ` $${index + 3}:name = $${index + 3}:csv AND`,
//            sql
//        );
//        sql = sql.slice(0, -4);
//        return this.db.result(sql, [this.schema, this.table, ...values]);
//    }
//}
//
//module.exports = DAO;
