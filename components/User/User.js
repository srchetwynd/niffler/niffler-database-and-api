//const User = require("./User_DAO");
//const bcrypt = require("bcrypt");
//const jwt = require("jsonwebtoken");
//const config = require("../../config");
//
//const createError = require("http-errors");
//
//async function authenticateFactory(options) {
//    console.log("cat");
//    let company;
//
//    try {
//        company = options.company
//            .toLowerCase()
//            .trim()
//            .replace(" ", "-")
//            .replace(/[^a-z]/, "");
//    } catch (err) {
//        throw new Error("Invalid Company");
//    }
//
//    const user = await new User()
//        .setSchema(company)
//        .setColumns("email_address", options.email)
//        .fetchOne();
//
//    if (!user) {
//        throw createError(401, "Incorrect Username or Password");
//    }
//
//    let result = await new Promise((resolve) => {
//        bcrypt.compare(options.password, user.password, (err, res) => {
//            resolve([err, res]);
//        });
//    });
//    if (result[0]) {
//        throw new Error(result[0].message);
//    }
//
//    if (!result[1]) {
//        throw createError(401, "Incorrect Username or Password");
//    }
//
//    const token = jwt.sign(
//        {
//            data: {
//                userid: user.id,
//                email: user.email_address,
//                company: company,
//            },
//        },
//        config.secret,
//        {
//            expiresIn: "8h",
//        }
//    );
//    user.token = token; // eslint-disable-line
//    delete user.password;
//    return user;
//}
//
//async function getUser(options) {
//    const user = new User()
//        .setSchema(options.company)
//        .setLimit(options.limit)
//        .setOffset(options.offset);
//    if (options.email) {
//        const data = await user
//            .setColumns("email_address", options.email)
//            .fetchOne();
//        if (!data) {
//            throw createError(404, "User not found");
//        }
//        return data;
//    } else if (options.id) {
//        const data = await user.setColumns("id", options.id).fetchOne();
//        if (!data) {
//            throw createError(404, "User not found");
//        }
//        return data;
//    } else {
//        return user.fetchAll();
//    }
//}
//
//async function postUser(options) {
//    return new User()
//        .setSchema(options.company)
//        .setColumns({
//            email_address: options.email,
//            password: await bcrypt.hash(options.password, 10),
//        })
//        .insert();
//}
//
//module.exports = {
//    authenticateFactory,
//    postUser,
//    getUser,
//};
