//const users = require("../User");
//const assert = require("assert");
//const sinon = require("sinon");
//const { db } = require("../../Database");
//
//describe("users handlers", () => {
//    describe.skip("authenticate", () => {
//        let auth;
//        before(() => {
//            auth = users.authenticateFactory({
//                getByEmail(email) {
//                    if (email === 1) {
//                        return;
//                    } else if (email === 2) {
//                        return {
//                            password:
//                                "$2b$10$uu5iOCdZc7RUIa5bRD.ILO.raYug5VpKi3EDNZ.8wf.LsxRczEmQi",
//                        };
//                    }
//                },
//            });
//        });
//
//        it("Should return a function", () => {
//            assert.strictEqual(typeof users.authenticateFactory(), "function");
//        });
//
//        it("Should return an error if user does not exist", async () => {
//            let req = {
//                body: {
//                    company: "",
//                    email: 1,
//                },
//            };
//            await auth(req, "", (result) => {
//                assert.strictEqual(result.message, "No user");
//            });
//        });
//
//        it("Should return an error if password does not match", async () => {
//            let req = {
//                body: {
//                    company: "",
//                    email: 2,
//                    password: "tst",
//                },
//            };
//            await auth(req, "", (result) => {
//                assert.strictEqual(result.message, "Incorrect Password");
//            });
//        });
//
//        it("Should pass if correct password is supplied", async () => {
//            let req = {
//                body: {
//                    company: "",
//                    email: 2,
//                    password: "test",
//                },
//            };
//            let res = {
//                send() {},
//            };
//            await auth(req, res);
//        });
//    });
//    describe("getUser", () => {
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should get a user by email", async () => {
//            sinon
//                .stub(db, "oneOrNone")
//                .callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 3,
//                offset: 3,
//                email: "test@test.com",
//            };
//            const result = await users.getUser(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if no user matches email address", async () => {
//            sinon.stub(db, "oneOrNone").callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 3,
//                offset: 3,
//                email: "test@test.com",
//            };
//            const test = () => users.getUser(req);
//            await assert.rejects(test);
//        });
//
//        it("Should get a user by id", async () => {
//            sinon
//                .stub(db, "oneOrNone")
//                .callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 3,
//                offset: 3,
//                id: 5,
//            };
//            const result = await users.getUser(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if no user matches id", async () => {
//            sinon.stub(db, "oneOrNone").callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 3,
//                offset: 3,
//                id: 5,
//            };
//            const test = () => users.getUser(req);
//            await assert.rejects(test);
//        });
//
//        it("Should return all users", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 3,
//                offset: 3,
//            };
//            const result = await users.getUser(req);
//            assert.strictEqual(result, "test");
//        });
//    });
//    describe("postUser", () => {
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should insert a new user", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                email: "test@test.com",
//                password: "test",
//            };
//            const result = await users.postUser(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject inserting a new user without email", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                password: "test",
//            };
//            const test = () => users.postUser(req);
//            await assert.rejects(test);
//        });
//    });
//});
