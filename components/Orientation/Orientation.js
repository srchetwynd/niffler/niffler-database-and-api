//"use strict";
//
///**
// * @function bulidOrientation
// * @description Builds the orientation functions
// * @param {Object} dependancies
// * @param {Function} dependancies.fetchAll
// * @return {Object}
// */
//function buildOrientation({ fetchAll }) {
//    if (typeof fetchAll !== "function") {
//        throw new Error(`fetchAll must be a function got: ${typeof fetchAll}`);
//    }
//    /**
//     * @function getOrientation
//     * @description returns all orientations
//     * @return {Array}
//     */
//    function getOrientation() {
//        return fetchAll();
//    }
//    return {
//        getOrientation,
//    };
//}
//
//module.exports = buildOrientation;
