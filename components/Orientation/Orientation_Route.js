//"use strict";
///**
// * @function makeRoute
// * @description Adds the orientation routes to the router
// * @param {Object} dependancies
// * @param {Function} dependancies.router
// * @param {Function} dependancies.fetchAll
// * @param {Function} dependancies.getResponder
// * @param {Function} dependancies.setResource
// * @param {Function} dependancies.setFunction
// * @param {String} dependancies.url
// */
//function makeRoute({
//    router,
//    fetchAll,
//    getResponder,
//    setResource,
//    setFunction,
//    url,
//} = {}) {
//    if (typeof router !== "object") {
//        throw new Error(`router must be a object got: ${typeof router}`);
//    }
//    if (typeof fetchAll !== "function") {
//        throw new Error(`fetchAll must be a function got: ${typeof fetchAll}`);
//    }
//    if (typeof getResponder !== "function") {
//        throw new Error(
//            `getResponder must be a function got: ${typeof getResponder}`
//        );
//    }
//    if (typeof setResource !== "function") {
//        throw new Error(
//            `setResource must be a function got: ${typeof setResource}`
//        );
//    }
//    if (typeof setFunction !== "function") {
//        throw new Error(
//            `setFunction must be a function got: ${typeof setFunction}`
//        );
//    }
//    if (typeof url !== "string") {
//        throw new Error(`url must be a string got: ${typeof url}`);
//    }
//
//    router.get(
//        url,
//        setResource("Orientation"),
//        setFunction(fetchAll),
//        getResponder
//    );
//
//    return router;
//}
//
//module.exports = makeRoute;
