//"use strict";
///**
// * @function buildOrientationDAL
// * @description builds Orientation DAL
// * @param {Object} dependancies
// * @param {Object} dependancies.db
// * @param {Function} dependancies.makeOrientation
// * @return {Object}
// */
//function buildOrientationDAL({ db, makeOrientation } = {}) {
//    if (typeof db !== "object") {
//        throw new Error(`db must be an object got: ${typeof db}`);
//    }
//    if (typeof makeOrientation !== "function") {
//        throw new Error(
//            `makeOrientation must be a function got: ${typeof makeOrientation}`
//        );
//    }
//
//    /**
//     * @function fetchAll
//     * @description returns all Orientations
//     * @return {Array}
//     */
//    async function fetchAll() {
//        const results = await db.any(`
//            SELECT
//                *
//            FROM
//                public.orientations
//        `);
//
//        return results.map(makeOrientation);
//    }
//
//    return {
//        fetchAll,
//    };
//}
//
//module.exports = buildOrientationDAL;
