//"use strict";
///**
// * @function buildMakeInterpretation
// * @description creates a function which makes an interpretations
// * @return {Function}
// */
//function buildMakeOrientation() {
//    /**
//     * @function makeInterpretation
//     * @description makes an interpretation
//     * @param {Object} orientation
//     * @param {Number} orientation.id
//     * @param {String} orientation.interpretation
//     * @return {Object}
//     */
//    return function makeOrientation({ id, orientation }) {
//        if (typeof id !== "number") {
//            throw new Error(`id must be a number got: ${typeof id}`);
//        }
//        if (typeof orientation !== "string") {
//            throw new Error(
//                `orientation must be a string got: ${typeof orientation}`
//            );
//        }
//
//        return Object.freeze({
//            id,
//            orientation,
//        });
//    };
//}
//
//module.exports = buildMakeOrientation;
