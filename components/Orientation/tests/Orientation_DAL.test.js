////const buildOrientation = require("../Orientation_DAL");
////
////const assert = require("assert");
////
////describe("Orientation DOL", () => {
////    it("Should throw if no dependancies are passed", () => {
////        const test = () => buildOrientation();
////        assert.throws(test);
////    });
////    it("Should throw if db is not an object", () => {
////        const test = () =>
////            buildOrientation({
////                db: "test",
////                makeOrientation: () => {},
////            });
////        assert.throws(test);
////    });
////    it("Should throw if makeOrientation is not a function", () => {
////        const test = () =>
////            buildOrientation({
////                db: {},
////                makeOrientation: "test",
////            });
////        assert.throws(test);
////    });
////    it("should not throw if the dependancies are right", () => {
////        const test = () => {
////            const result = buildOrientation({
////                db: {},
////                makeOrientation: () => ({}),
////            });
////            assert.strictEqual(typeof result, "object");
////        };
////        assert.doesNotThrow(test);
////    });
////    describe("OrientationDAL", () => {
////        let orientationDAL;
////        before(() => {
////            orientationDAL = buildOrientation({
////                db: {
////                    any: () => [],
////                },
////                makeOrientation: () => ({}),
////            });
////        });
////        describe("fetchAll", () => {
////            it("should be a function", () => {
////                assert.strictEqual(typeof orientationDAL.fetchAll, "function");
////            });
////            it("should return an array", async () => {
////                assert.deepStrictEqual(await orientationDAL.fetchAll(), []);
////            });
////        });
////    });
////});
