////const buildMakeOrientation = require("../Orientation_Entity");
////
////const assert = require("assert");
////
////describe("buildMakeOrientation", () => {
////    it("Should not throw if the dependancies are right", () => {
////        const test = () => {
////            const result = buildMakeOrientation();
////            assert.strictEqual(typeof result, "function");
////        };
////        assert.doesNotThrow(test);
////    });
////    describe("makeOrientation", () => {
////        let makeOrientation;
////        before(() => {
////            makeOrientation = buildMakeOrientation();
////        });
////        it("should throw if id is not a number", () => {
////            const test = () =>
////                makeOrientation({
////                    id: "test",
////                    orientation: "test",
////                });
////            assert.throws(test);
////        });
////        it("should throw if orientation is not string", () => {
////            const test = () =>
////                makeOrientation({
////                    id: 2,
////                    orientation: 2,
////                });
////            assert.throws(test);
////        });
////        it("should not throw if correct types", () => {
////            const test = () => {
////                makeOrientation({
////                    id: 2,
////                    orientation: "test",
////                });
////                assert.strictEqual(typeof test, "function");
////            };
////            assert.doesNotThrow(test);
////        });
////        describe("returned Object", () => {
////            let orientation;
////            before(() => {
////                orientation = makeOrientation({
////                    id: 2,
////                    orientation: "test",
////                });
////            });
////            it("Should not allow the id to change", () => {
////                orientation.id = 5;
////                assert.strictEqual(orientation.id, 2);
////            });
////            it("Should not allow the orientation to be changed", () => {
////                orientation.orientation = "foo";
////                assert.strictEqual(orientation.orientation, "test");
////            });
////        });
////    });
////});
