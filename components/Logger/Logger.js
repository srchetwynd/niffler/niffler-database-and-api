const split = require("split2");
const pump = require("pump");
const through = require("through2");

require("dotenv").config();

const logger = require("./helpers/database/logger");

const transport = through.obj(function (chunk, enc, cb) {
    if (typeof chunk.res !== "undefined") {
        logger.insertIntoRequests({
            method: chunk.req.method,
            url: chunk.req.url,
            remote_address: chunk.req.remoteAddress,
            response_time: chunk.responseTime,
            status_code: chunk.res.statusCode,
            user_agent: chunk.req.headers["user-agent"],
        });
        cb();
    }
});

pump(process.stdin, split(JSON.parse), transport);
