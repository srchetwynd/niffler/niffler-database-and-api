const db = require("./db");

/**
 * @function insertIntoRequests
 * @description insert a request into the db
 * @param {object} data - request to be inserted db
 * @param {string} data.method - the request method
 * @param {string} data.url - the url for the request
 * @param {string} data.user_agent - the user agent the request was sent from
 * @param {string} data.remote_address - ip address the request was made from
 * @param {string} data.response_time - the response time of the request
 * @param {string} data.status_code - the status_code of the response
 * @returns {Promise} - Empty promise for db insert
 */
function insertIntoRequests(data) {
    return db.none(
        `
        INSERT INTO logging.requests
            (
                method,
                url,
                user_agent,
                remote_address,
                response_time,
                status_code
            )
        VALUES
            ($1, $2, $3, $4, $5, $6)`,
        [
            data.method,
            data.url,
            data.user_agent,
            data.remote_address,
            data.response_time,
            data.status_code,
        ]
    );
}

module.exports = {
    insertIntoRequests,
};
