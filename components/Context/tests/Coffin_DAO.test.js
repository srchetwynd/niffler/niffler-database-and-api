//const Coffin = require("../Coffin_DAO");
//
//const assert = require("assert");
//
//describe("Coffin DAO", () => {
//    const db = {
//        any: () => Promise.resolve(),
//        one: () => Promise.resolve(),
//        oneOrNone: () => Promise.resolve(),
//    };
//    it("Should return an object", () => {
//        assert.strictEqual(typeof Coffin, "function");
//    });
//    it("Should return an object", () => {
//        let coffin = new Coffin();
//        assert.strictEqual(typeof coffin, "object");
//    });
//    it("Should fetch all coffins", () => {
//        let coffin = new Coffin();
//        return coffin.setSchema("template").setDatabase(db).fetchAll();
//    });
//    it("Should fetch one coffin", () => {
//        let coffin = new Coffin();
//        return coffin
//            .setSchema("template")
//            .setDatabase(db)
//            .setColumns("id", "12")
//            .fetchOne();
//    });
//    it("Should fetch one coffin", () => {
//        let coffin = new Coffin();
//        return coffin
//            .setSchema("template")
//            .setDatabase(db)
//            .setColumns({ id: "12" })
//            .fetchOne();
//    });
//    it("Should error if a required field is not entered", () => {
//        let coffin = new Coffin();
//        function test() {
//            coffin.setSchema("template").setDatabase(db).fetchOne();
//        }
//        assert.throws(test);
//    });
//    it("Should error if column is set to invalid type", () => {
//        let coffin = new Coffin();
//        function test() {
//            coffin
//                .setSchema("template")
//                .setDatabase(db)
//                .setColumns({ id: "twelve" });
//        }
//        assert.throws(test);
//    });
//});
