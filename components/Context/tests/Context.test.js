//const context = require("../Context");
//const { db } = require("../../Database");
//const assert = require("assert");
//const sinon = require("sinon");
//
//describe("context handlers", () => {
//    describe("getContext", () => {
//        const dbStub = {};
//        afterEach(() => {
//            sinon.restore();
//        });
//        it("Should return data from the database", async () => {
//            dbStub.oneOrNone = sinon.stub(db, "oneOrNone");
//            dbStub.oneOrNone.callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 5,
//                offset: 0,
//                id: 5,
//            };
//            const result = await context.getContext(req);
//            assert.strictEqual(result, "test");
//        });
//        it("Should reject if no data", async () => {
//            dbStub.oneOrNone = sinon.stub(db, "oneOrNone");
//            dbStub.oneOrNone.callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 5,
//                offset: 0,
//                id: 5,
//            };
//            const test = () => context.getContext(req);
//            assert.rejects(test);
//        });
//        it("Should return data for search all", async () => {
//            dbStub.any = sinon.stub(db, "any");
//            dbStub.any.callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 5,
//                offset: 0,
//                number: 5,
//            };
//            const result = await context.getContext(req);
//            assert.strictEqual(result, "test");
//        });
//        it("Should return data for search all even if no data", async () => {
//            dbStub.any = sinon.stub(db, "any");
//            dbStub.any.callsFake(() => Promise.resolve([]));
//            const req = {
//                company: "test",
//                limit: 5,
//                offset: 0,
//                number: 5,
//            };
//            const result = await context.getContext(req);
//            assert.deepStrictEqual(result, []);
//        });
//        it("Should return data for a trench", async () => {
//            dbStub.any = sinon.stub(db, "any");
//            dbStub.any.callsFake(() => Promise.resolve([]));
//            const req = {
//                company: "test",
//                limit: 5,
//                offset: 0,
//                trenchID: 5,
//            };
//            const result = await context.getContext(req);
//            assert.deepStrictEqual(result, []);
//        });
//        it("Should reject if there is not enough data", async () => {
//            dbStub.any = sinon.stub(db, "any");
//            dbStub.any.callsFake(() => Promise.resolve([]));
//            const req = {
//                company: "test",
//                limit: 5,
//                offset: 0,
//            };
//            const test = () => context.getContext(req);
//            assert.rejects(test);
//        });
//    });
//    describe("deleteContext", () => {
//        const dbStub = {};
//
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Delete a context", async () => {
//            dbStub.result = sinon.stub(db, "result");
//            dbStub.result.callsFake(() =>
//                Promise.resolve({
//                    rowCount: 1,
//                })
//            );
//            const req = {
//                company: "test",
//                id: 2,
//            };
//            const test = () => context.deleteContext(req);
//            assert.doesNotReject(test);
//        });
//        it("Delete a context rejects if no context", async () => {
//            dbStub.result = sinon.stub(db, "result");
//            dbStub.result.callsFake(() =>
//                Promise.resolve({
//                    rowCount: 0,
//                })
//            );
//            const req = {
//                company: "test",
//                id: 2,
//            };
//            const test = () => context.deleteContext(req);
//            assert.rejects(test);
//        });
//    });
//    describe.skip("postContext", () => {});
//});
