//const Context = require("./Context_DAO");
//const { contextTypeDAO } = require("../ContextType");
//
//const createError = require("http-errors");
//
//async function getContext(options) {
//    const context = new Context()
//        .setSchema(options.company)
//        .setLimit(options.limit)
//        .setOffset(options.offset);
//    if (options.id) {
//        const data = await context.setColumns("id", options.id).fetchOne();
//        if (data) {
//            return data;
//        } else {
//            throw createError(404, "Context not found");
//        }
//    } else if (options.number) {
//        return context.setColumns("number", options.number).fetchAll();
//    } else if (options.trenchID && options.type) {
//        const type =
//            options.type.slice(0, 1).toUpperCase() +
//            options.type.slice(1).toLowerCase();
//
//        const contextType = await new contextTypeDAO()
//            .setColumns("context_type", type)
//            .fetchOne();
//        return context
//            .setColumns("trench", options.trenchID)
//            .setColumns("context_type", contextType.id)
//            .fetchAll();
//    } else if (options.trenchID) {
//        return context.setColumns("trench", options.trenchID).fetchAll();
//    } else {
//        throw createError(400, "Not enough data to return contexts");
//    }
//}
//
//async function deleteContext(options) {
//    let result = await new Context()
//        .setSchema(options.company)
//        .setColumns("id", options.id)
//        .delete();
//    if (result.rowCount === 0) {
//        throw createError(404, "Context not found.");
//    }
//    return;
//}
//
//function postContext() {
//    // return contextDetail.insert(
//    //    req.body.context,
//    //    req.body.above_belows,
//    //    req.body.same_as,
//    //    req.body.photo_context,
//    //    req.body.drawing_context,
//    //    req.body.context_details,
//    //    req.query.type,
//    //    req.company
//    // );
//}
//
//module.exports = {
//    getContext,
//    deleteContext,
//    postContext,
//};
