//const validMethods = ["POST", "PUT", "PATCH", "GET", "DELETE"];
//
//function isMethodValid(val) {
//    return validMethods.includes(val);
//}
//
//class LinkObject {
//    constructor() {
//        this.href = undefined;
//        this.methods = [];
//        this.fields = undefined;
//    }
//
//    setHref(val) {
//        if (typeof val === "string") {
//            this.href = val;
//            return this;
//        } else {
//            throw new Error(`href must be a string got: ${val}`);
//        }
//    }
//
//    setMethod(val) {
//        if (typeof val === "string" && isMethodValid(val)) {
//            this.methods.push(val);
//            return this;
//        } else if (Array.isArray(val)) {
//            val.forEach((el) => {
//                if (isMethodValid(el)) {
//                    this.methods.push(el);
//                } else {
//                    throw new Error(`Method is not valid: ${el}`);
//                }
//            });
//            return this;
//        } else {
//            throw new Error(`Method is not valid: ${val}`);
//        }
//    }
//
//    setField(val) {
//        if (Array.isArray(val)) {
//            this.fields = val;
//            return this;
//        } else if (typeof val === "object" && this.fields) {
//            this.fields.push(val);
//            return this;
//        } else if (typeof val === "object") {
//            this.fields = [];
//            this.fields.push(val);
//            return this;
//        } else {
//            throw new Error("Field must be an array or a field");
//        }
//    }
//
//    build() {
//        if (!this.href) {
//            throw new Error("href must be defined");
//        }
//        return {
//            href: this.href,
//            meta: {
//                method: this.methods,
//                fields: this.fields,
//            },
//        };
//    }
//}
//
//module.exports = LinkObject;
