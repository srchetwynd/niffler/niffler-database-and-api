//const JsonApi = require("../JsonApiObject");
//const assert = require("assert");
//
//describe("JsonApi", () => {
//    it("Should return an instance of itself", () => {
//        assert.strictEqual(new JsonApi() instanceof JsonApi, true);
//    });
//    it("Should not add an invalid meta", () => {
//        const test = () => new JsonApi().setMeta("Cat");
//        assert.throws(test);
//    });
//    it("Should allow an object as a meta", () => {
//        const jsonApi = new JsonApi().setMeta({ test: "test" });
//        assert.strictEqual(
//            jsonApi instanceof JsonApi,
//            true,
//            "Did not return an instance of JsonApi"
//        );
//    });
//    it("Should throw if setting an invalid Data", () => {
//        const test = () => new JsonApi().setData("test");
//        assert.throws(test);
//    });
//    it("Should allow setting an object as data", () => {
//        const jsonApi = new JsonApi().setData({ test: "test" });
//        assert.strictEqual(
//            jsonApi instanceof JsonApi,
//            true,
//            "Did not return an instance of JsonApi"
//        );
//    });
//    it("Should allow setting an array as data", () => {
//        const jsonApi = new JsonApi().setData(["test"]);
//        assert.strictEqual(
//            jsonApi instanceof JsonApi,
//            true,
//            "Did not return an instance of JsonApi"
//        );
//    });
//    it("Should throw if setting invalid Error", () => {
//        const test = () => new JsonApi().setError("test");
//        assert.throws(test);
//    });
//    it("Should allow setting an object as Error", () => {
//        const jsonApi = new JsonApi().setError({ test: "test" });
//        assert.strictEqual(
//            jsonApi instanceof JsonApi,
//            true,
//            "Did not return an instance of JsonApi"
//        );
//    });
//    it("Should allow adding an object as Error", () => {
//        let jsonApi = new JsonApi().setError({ test: "test" });
//        assert.strictEqual(
//            jsonApi instanceof JsonApi,
//            true,
//            "Did not return an instance of JsonApi"
//        );
//        jsonApi = new JsonApi().setError({ test2: "test2" });
//        assert.strictEqual(
//            jsonApi instanceof JsonApi,
//            true,
//            "Did not return an instance of JsonApi"
//        );
//    });
//    it("Should throw if links has invalid value", () => {
//        const test = () => new JsonApi().setLinks("cat", "cat");
//        assert.throws(test);
//    });
//    it("Should throw if links has invalid key", () => {
//        const test = () => new JsonApi().setLinks({ test: "test" });
//        assert.throws(test);
//    });
//    it("Should allow setting link value to object and key to string", () => {
//        const jsonApi = new JsonApi().setLinks({ test: "test" }, "test");
//        assert.strictEqual(
//            jsonApi instanceof JsonApi,
//            true,
//            "Did not reutrn an instance of JsonApi"
//        );
//    });
//    it("Should not throw if attempting to build without error or data", () => {
//        const test = () => new JsonApi().build();
//        assert.doesNotThrow(test);
//    });
//    it("Should build with data", () => {
//        const jsonApi = new JsonApi().setData(["test"]).build();
//        assert.deepStrictEqual(
//            jsonApi.data,
//            ["test"],
//            "Data was not the same as passed in"
//        );
//    });
//    it("Should build with error", () => {
//        const jsonApi = new JsonApi().setError({ test: "test" }).build();
//        assert.deepStrictEqual(
//            jsonApi.errors,
//            [{ test: "test" }],
//            "Error was not the same as passed in"
//        );
//    });
//    it("Should build with meta", () => {
//        const jsonApi = new JsonApi()
//            .setData({ test: "test" })
//            .setMeta({ cat: "meow" })
//            .build();
//        assert.deepStrictEqual(
//            jsonApi.meta,
//            { cat: "meow" },
//            "Meta was not the same as passed in"
//        );
//    });
//    it("Should build with links", () => {
//        const jsonApi = new JsonApi()
//            .setData({ test: "test" })
//            .setLinks({ cat: "meow" }, "animal")
//            .build();
//        assert.deepStrictEqual(
//            jsonApi.links,
//            { animal: { cat: "meow" } },
//            "Links was not the same as passed in"
//        );
//    });
//});
