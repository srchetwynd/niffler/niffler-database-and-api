//const assert = require("assert");
//const LinkObject = require("../LinksObject");
//
//describe("LinksObject", () => {
//    it("Should return instance of itself when contructed", () => {
//        const link = new LinkObject();
//        assert.strictEqual(link instanceof LinkObject, true);
//    });
//    describe("setHref", () => {
//        it("Should successfully add a href", () => {
//            const test = () => {
//                const link = new LinkObject().setHref("test");
//                assert.strictEqual(link instanceof LinkObject, true);
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should thorw if href is not a string", () => {
//            const test = () => new LinkObject().setHref(5);
//            assert.throws(test);
//        });
//    });
//    describe("setMethod", () => {
//        it("Should add a method", () => {
//            const test = () => {
//                const link = new LinkObject().setMethod("GET");
//                assert.strictEqual(link instanceof LinkObject, true);
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should add an array of methods", () => {
//            const test = () => {
//                const link = new LinkObject().setMethod(["GET", "POST"]);
//                assert.strictEqual(link instanceof LinkObject, true);
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should throw if method is not a string", () => {
//            const test = () => new LinkObject().setMethod(5);
//            assert.throws(test);
//        });
//        it("Should throw if array of methods has a none string", () => {
//            const test = () => new LinkObject().setMethod(["GET", 5]);
//            assert.throws(test);
//        });
//        it("Should throw if method is invalid", () => {
//            const test = () => new LinkObject().setMethod("DELETEALL");
//            assert.throws(test);
//        });
//        it("Should throw if array of methods has an invalid", () => {
//            const test = () => new LinkObject().setMethod(["GET", "DELETEALL"]);
//            assert.throws(test);
//        });
//    });
//    describe("setField", () => {
//        it("Should add a field", () => {
//            const test = () => {
//                const link = new LinkObject().setField({ test: "test" });
//                assert.strictEqual(link instanceof LinkObject, true);
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should add an array of fields", () => {
//            const test = () => {
//                const link = new LinkObject().setField([
//                    { test: "test" },
//                    { test: "test" },
//                ]);
//                assert.strictEqual(link instanceof LinkObject, true);
//            };
//            assert.doesNotThrow(test);
//        });
//        it("should add an additional field", () => {
//            const test = () => {
//                const link = new LinkObject()
//                    .setField({ test: "test" })
//                    .setField({ test2: "test" });
//                assert.strictEqual(link instanceof LinkObject, true);
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should throw is field is not an array of object", () => {
//            const test = () => new LinkObject().setField("test");
//            assert.throws(test);
//        });
//    });
//    describe("build", () => {
//        it("Should return an object", () => {
//            const result = new LinkObject().setHref("test").build();
//            assert.strictEqual(typeof result, "object");
//        });
//        it("Should throw is no href set", () => {
//            const test = () => new LinkObject().build();
//            assert.throws(test);
//        });
//    });
//});
