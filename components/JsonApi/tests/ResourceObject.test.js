//const assert = require("assert");
//const ResourceObject = require("../ResourceObject");
//
//describe("ResourceObject", () => {
//    describe("constructor", () => {
//        it("Should return an instance of itself", () => {
//            const resourceInst = new ResourceObject();
//            assert.strictEqual(resourceInst instanceof ResourceObject, true);
//        });
//    });
//    describe("setId", () => {
//        it("Should set a string id", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject().setId("test");
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should set a number id", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject().setId(5);
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should throw if id is not a string or number", () => {
//            const test = () => new ResourceObject().setId({});
//            assert.throws(test);
//        });
//    });
//    describe("setType", () => {
//        it("Should set a string type", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject().setType("test");
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should throw if type is not a string", () => {
//            const test = () => new ResourceObject().setType(7);
//            assert.throws(test);
//        });
//    });
//    describe("setAttributes", () => {
//        it("Should set an object attribute", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject().setAttributes({
//                    test: "test",
//                });
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should throw if attribute is not an object", () => {
//            const test = () => new ResourceObject().setAttributes(7);
//            assert.throws(test);
//        });
//    });
//    describe("setRelationships", () => {
//        it("Should add a relationship", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject().setRelationships(
//                    { test: "test" },
//                    "test"
//                );
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should add an additional relationship", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject()
//                    .setRelationships({ test: "test" }, "test")
//                    .setRelationships({ test2: "test2" }, "test2");
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should reject if key is not string", () => {
//            const test = () => new ResourceObject().setRelationships({}, 4);
//            assert.throws(test);
//        });
//        it("Should reject if val is not object", () => {
//            const test = () => new ResourceObject().setRelationships(3, "test");
//            assert.throws(test);
//        });
//    });
//    describe("setLinks", () => {
//        it("Should add a link object", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject().setLinks({
//                    test: "test",
//                });
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should add a link link by key", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject().setLinks(
//                    { test: "test" },
//                    "test"
//                );
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should add an additional link by key", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject()
//                    .setLinks({ test: "test" }, "test")
//                    .setLinks({ test2: "test2" }, "test2");
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//        it("Should throw if val is not oject or array", () => {
//            const test = () => new ResourceObject().setLinks(3);
//            assert.throws(test);
//        });
//    });
//    describe("setMeta", () => {
//        it("Should add a meta object", () => {
//            const test = () => {
//                const resourceInst = new ResourceObject().setMeta("test");
//                assert.strictEqual(
//                    resourceInst instanceof ResourceObject,
//                    true
//                );
//            };
//            assert.doesNotThrow(test);
//        });
//    });
//    describe("build", () => {
//        it("Should build the resouce object", () => {
//            const result = new ResourceObject()
//                .setId(1)
//                .setType("test")
//                .build();
//            assert.strictEqual(typeof result, "object");
//        });
//        it("Should throw if no id", () => {
//            const test = () => new ResourceObject().setType("test").build();
//            assert.throws(test);
//        });
//        it("Should throw if no type", () => {
//            const test = () => new ResourceObject().setId(4).build();
//            assert.throws(test);
//        });
//    });
//});
