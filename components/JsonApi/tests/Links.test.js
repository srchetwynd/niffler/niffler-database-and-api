//const links = require("../Links");
//const LinkObject = require("../LinksObject");
//const assert = require("assert");
//
//describe("links helper", () => {
//    it("Should throw an error if link is invalid", () => {
//        const test = () => links.setMethods({});
//        assert.throws(test);
//    });
//    it("Should not throw on correct params", () => {
//        const test = () => links.setMethods(new LinkObject());
//        assert.doesNotThrow(test);
//    });
//    it("Should set defaults if no permissions set", () => {
//        const result = links
//            .setMethods(new LinkObject())
//            .setHref("test.test")
//            .build();
//        assert.deepStrictEqual(result.meta.method, [
//            "PUT",
//            "POST",
//            "GET",
//            "DELETE",
//        ]);
//    });
//    it("Should only set methods user has permissions for", () => {
//        const result = links
//            .setMethods(new LinkObject(), { PUT: true, POST: false, GET: true })
//            .setHref("test.test")
//            .build();
//        assert.deepStrictEqual(result.meta.method, ["PUT", "GET"]);
//    });
//});
