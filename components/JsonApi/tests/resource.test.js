//const assert = require("assert");
//const resource = require("../Resource");
//
//describe("resource helper", () => {
//    describe("resourceBuilder", () => {
//        it("Should return an array of resources", () => {
//            const req = {
//                params: {},
//                query: {},
//                dataRelationships: { test: () => ({ test: "test" }) },
//                dataLinks: {},
//            };
//            const data = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
//            const type = "project";
//            const result = resource.resourceBuilder(req, data, type);
//            assert.strictEqual(Array.isArray(result), true);
//        });
//        it("Should return an object resource", () => {
//            const req = {
//                params: {},
//                query: {},
//                dataRelationships: {},
//                dataLinks: { test: { test: "test" } },
//            };
//            const data = { id: 1 };
//            const type = "project";
//            const result = resource.resourceBuilder(req, data, type);
//            assert.strictEqual(typeof result, "object");
//            assert.strictEqual(Array.isArray(result), false);
//        });
//    });
//});
