//const MetaObject = require("./MetaObject");
//
//class JsonApi {
//    constructor() {
//        this.meta = new MetaObject().build();
//        this.data = undefined;
//        this.errors = undefined;
//        this.links = undefined;
//    }
//
//    setMeta(val) {
//        if (typeof val === "object" && !Array.isArray(val)) {
//            this.meta = val;
//            return this;
//        } else {
//            throw new Error("Meta must be an Object");
//        }
//    }
//
//    setData(val) {
//        if (typeof val === "object") {
//            this.data = val;
//            return this;
//        } else {
//            throw new Error(
//                `Data must be an Object or an Array got: ${typeof val}`
//            );
//        }
//    }
//
//    setError(val) {
//        if (!this.errors) {
//            this.errors = [];
//        }
//        if (typeof val === "object" && !Array.isArray(val)) {
//            this.errors.push(val);
//            return this;
//        } else {
//            throw new Error("Error must be an Object");
//        }
//    }
//
//    setLinks(val, key) {
//        if (typeof val === "object" && typeof key === "string") {
//            if (!this.links) {
//                this.links = {};
//            }
//            this.links[key] = val;
//            return this;
//        } else {
//            throw new Error("Link must be an Object, and have a Key");
//        }
//    }
//
//    build() {
//        return {
//            meta: this.meta,
//            data: this.data,
//            links: this.links,
//            errors: this.errors,
//        };
//    }
//}
//
//module.exports = JsonApi;
