//class ResourceObject {
//    constructor() {
//        this.id = undefined;
//        this.type = undefined;
//        this.attributes = undefined;
//        this.relationships = undefined;
//        this.links = undefined;
//        this.meta = undefined;
//        return this;
//    }
//
//    setId(val) {
//        if (typeof val === "string" || typeof val === "number") {
//            this.id = val;
//            return this;
//        } else {
//            throw new Error(`ID must be a sting or a number. Got ${val}`);
//        }
//    }
//
//    setType(val) {
//        if (typeof val === "string") {
//            this.type = val;
//            return this;
//        } else {
//            throw new Error("Type must be a string");
//        }
//    }
//
//    setAttributes(val) {
//        if (typeof val === "object" && !Array.isArray(val)) {
//            this.attributes = val;
//            return this;
//        } else {
//            throw new Error("Attributes must be an Object");
//        }
//    }
//
//    setRelationships(val, key) {
//        if (
//            typeof val === "object" &&
//            !Array.isArray(val) &&
//            typeof key === "string" &&
//            this.relationships
//        ) {
//            this.relationships[key] = val;
//            return this;
//        } else if (
//            typeof val === "object" &&
//            !Array.isArray(val) &&
//            typeof key === "string"
//        ) {
//            this.relationships = {
//                [key]: val,
//            };
//            return this;
//        } else {
//            throw new Error("Relationships must be an object");
//        }
//    }
//
//    setLinks(val, key) {
//        if (
//            typeof val === "object" &&
//            !Array.isArray(val) &&
//            key &&
//            this.links
//        ) {
//            this.links[key] = val;
//            return this;
//        } else if (typeof val === "object" && !Array.isArray(val) && key) {
//            this.links = {
//                [key]: val,
//            };
//            return this;
//        } else if (typeof val === "object" && !Array.isArray(val)) {
//            this.links = val;
//            return this;
//        } else {
//            throw new Error("Links must be an object");
//        }
//    }
//
//    setMeta(val) {
//        this.meta = val;
//        return this;
//    }
//
//    build() {
//        if (!this.id || !this.type) {
//            throw new Error("ID and Type must be defined");
//        }
//        return {
//            id: this.id,
//            type: this.type,
//            attributes: this.attributes,
//            relationships: this.relationships,
//            links: this.links,
//            meta: this.meta,
//        };
//    }
//}
//
//module.exports = ResourceObject;
