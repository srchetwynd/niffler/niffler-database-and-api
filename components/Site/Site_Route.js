//const express = require("express");
//const router = express.Router();
//const links = require("../../middleware/links");
//
//const exp = require("../Express");
//const pagination = require("../../middleware/pagination");
//const site = require("./Site");
//const urls = require("../../helpers/urls");
//
//router.use(
//    urls.site.url,
//    links.buildResourceLinks([]),
//    links.buildDataLinks([]),
//    links.buildDataRelationships(["project", "site", "trench"])
//);
//
//router.get(
//    urls.site.url,
//    pagination.setLimitOffset,
//    (req, res, next) => {
//        exp.helpers.AddFunction(
//            site.getSite,
//            {
//                company: req.company,
//                limit: req.limit,
//                offset: req.offset,
//                id: req.query.id,
//                projectID: req.params.projectID,
//            },
//            req
//        );
//        return next();
//    },
//    exp.getResponder
//);
//
//router.put(
//    urls.site.url,
//    (req, res, next) => {
//        exp.helpers.addFunction(
//            site.putSite,
//            {
//                company: req.company,
//                project: req.body.project,
//                id: req.body.id,
//                ...req.body,
//            },
//            req
//        );
//        return next();
//    },
//    exp.putResponder
//);
//
//router.post(
//    urls.site.url,
//    (req, res, next) => {
//        exp.helpers.addFunction(
//            site.postSite,
//            {
//                company: req.company,
//                projectID: req.params.projectID,
//                ...req.body,
//            },
//            req
//        );
//        return next();
//    },
//    exp.postResponder
//);
//
//router.delete(
//    urls.site.url,
//    (req, res, next) => {
//        exp.helpers.addFunction(
//            site.deleteSite,
//            {
//                company: req.company,
//                id: req.query.id,
//            },
//            req
//        );
//        return next();
//    },
//    exp.deleteResponder
//);
//
//module.exports = router;
