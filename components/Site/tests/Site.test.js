//const site = require("../Site");
//const sinon = require("sinon");
//const assert = require("assert");
//const { db } = require("../../Database");
//
//describe("site handler", () => {
//    describe("getSite", () => {
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should get a site by id", async () => {
//            sinon
//                .stub(db, "oneOrNone")
//                .callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                id: 1,
//            };
//            const result = await site.getSite(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if site not found", async () => {
//            sinon.stub(db, "oneOrNone").callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//                id: 1,
//            };
//            const test = () => site.getSite(req);
//            assert.rejects(test);
//        });
//
//        it("Should get all sites in a project", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//                projectID: 5,
//            };
//            const result = await site.getSite(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if not enough information provided", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//            };
//            const test = () => site.getSite(req);
//            assert.rejects(test);
//        });
//    });
//    describe.skip("putSite", () => {});
//    describe.skip("postSite", () => {});
//    describe("deleteSite", () => {
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should delete a site", async () => {
//            sinon
//                .stub(db, "result")
//                .callsFake(() => Promise.resolve({ rowCount: 1 }));
//            const req = {
//                company: "test",
//                id: 1,
//            };
//            const test = () => site.deleteSite(req);
//            assert.doesNotReject(test);
//        });
//
//        it("Should reject if no site exists", async () => {
//            sinon
//                .stub(db, "result")
//                .callsFake(() => Promise.resolve({ rowCount: 0 }));
//            const req = {
//                company: "test",
//                id: 1,
//            };
//            const test = () => site.deleteSite(req);
//            assert.rejects(test);
//        });
//    });
//});
