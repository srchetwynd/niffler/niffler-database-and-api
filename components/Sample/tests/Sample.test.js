//const sample = require("../Sample");
//const sinon = require("sinon");
//const assert = require("assert");
//const { db } = require("../../Database");
//
//describe("sample handler", () => {
//    describe("getSample", () => {
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should get a sample by id", async () => {
//            sinon
//                .stub(db, "oneOrNone")
//                .callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//                id: 1,
//            };
//            const result = await sample.getSample(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if sample not found", async () => {
//            sinon.stub(db, "oneOrNone").callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//                id: 1,
//            };
//            const test = () => sample.getSample(req);
//            assert.rejects(test);
//        });
//
//        it("Should get all samples in a project", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//                trenchID: 5,
//            };
//            const result = await sample.getSample(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if not enough information provided", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//            };
//            const test = () => sample.getSample(req);
//            assert.rejects(test);
//        });
//    });
//});
