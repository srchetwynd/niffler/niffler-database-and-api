//"use strict";
///**
// * @function buildDegreeOfContamination
// * @description Builds the above below functions
// * @param {Object} dependancies
// * @param {Function} dependancies.fetchAll
// * @return {Object}
// */
//function buildDegreeOfContamination({ fetchAll } = {}) {
//    if (typeof fetchAll !== "function") {
//        throw new Error(`fetchAll must be a function got: ${typeof fetchAll}`);
//    }
//    /**
//     * @function getContamination
//     * @description reutrns all contaminations
//     * @return {Array}
//     */
//    function getContamination() {
//        return fetchAll();
//    }
//    return {
//        getContamination,
//    };
//}
//
//module.exports = buildDegreeOfContamination;
