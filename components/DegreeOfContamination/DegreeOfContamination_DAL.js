//"use strict";
///**
// * @function buildContaminationDAL
// * @description builds Contamination DAL
// * @param {Object} dependancies
// * @param {Object} dependancies.db
// * @param {Function} dependancies.makeContamination
// * @return {Object}
// */
//function buildContaminationDAL({ db, makeContamination } = {}) {
//    if (typeof db !== "object") {
//        throw new Error(`db must be an object got: ${typeof db}`);
//    }
//    if (typeof makeContamination !== "function") {
//        throw new Error(
//            `makeContamination must be a function got: ${typeof makeContamination}`
//        );
//    }
//
//    /**
//     * @function fetchAll
//     * @description returns all degrees of contamination
//     * @return {Array}
//     */
//    async function fetchAll() {
//        const results = await db.any(`
//            SELECT
//                id,
//                degree AS contamination
//            FROM
//                public.degree_of_contamination
//        `);
//
//        return results.map((el) => makeContamination(el));
//    }
//
//    return {
//        fetchAll,
//    };
//}
//
//module.exports = buildContaminationDAL;
