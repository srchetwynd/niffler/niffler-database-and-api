//"use strict";
///**
// * @function buildMakeDegreeOfContamination
// * @description creates the makeDegreeOfContamination function
// * @return {Function}
// */
//function buildMakeDegreeOfContamination() {
//    /**
//     * @function makeDegreeOfContamination
//     * @description creates a contamination object
//     * @param {Object} obj
//     * @param {Number} obj.id
//     * @param {String} obj.contamination
//     * @return {Object}
//     */
//    return function madeDegreeOfContamination({ id, contamination } = {}) {
//        if (typeof id !== "number") {
//            throw new Error(`id must be a number got: ${typeof id}`);
//        }
//        if (typeof contamination !== "string") {
//            throw new Error(
//                `contamination must be a string got: ${typeof contamination}`
//            );
//        }
//
//        return Object.freeze({
//            id,
//            contamination,
//        });
//    };
//}
//
//module.exports = buildMakeDegreeOfContamination;
