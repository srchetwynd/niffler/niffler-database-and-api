//const buildDegreeOfContamination = require("../DegreeOfContamination_DAL");
//
//const assert = require("assert");
//
//describe("DegreeOfContamination DAL", () => {
//    it("Should throw if no dependancies passed", () => {
//        const test = () => buildDegreeOfContamination();
//        assert.throws(test);
//    });
//    it("Should throw if db is not an object", () => {
//        const test = () =>
//            buildDegreeOfContamination({
//                db: "test",
//                makeContamination: (test) => test,
//            });
//        assert.throws(test);
//    });
//    it("Should throw if makeContamination is not a function", () => {
//        const test = () =>
//            buildDegreeOfContamination({
//                db: {
//                    any: () => [],
//                },
//                makeContamination: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should not throw if correct dependancies", () => {
//        const test = () => {
//            const result = buildDegreeOfContamination({
//                db: {
//                    any: () => [],
//                },
//                makeContamination: (test) => test,
//            });
//            assert.strictEqual(typeof result, "object");
//            assert.strictEqual(typeof result.fetchAll, "function");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("returned object", () => {
//        let obj;
//        before(() => {
//            obj = buildDegreeOfContamination({
//                db: {
//                    any: () => ["test"],
//                },
//                makeContamination: (test) => test,
//            });
//        });
//        it("Should have a fetchAll function", () => {
//            assert.strictEqual(typeof obj.fetchAll, "function");
//        });
//        describe("fetchAll", () => {
//            it("Should return an array", async () => {
//                const result = await obj.fetchAll();
//                assert.strictEqual(Array.isArray(result), true);
//            });
//        });
//    });
//});
