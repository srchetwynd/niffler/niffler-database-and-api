//const degreeOfContamination = require("../DegreeOfContamination_Entity");
//
//const assert = require("assert");
//
//describe("Degree Of Contamination Entity", () => {
//    it("Should build a function for creating entities", () => {
//        const test = () => degreeOfContamination();
//        assert.doesNotThrow(test);
//    });
//    describe("returned function", () => {
//        let func;
//        before(() => {
//            func = degreeOfContamination();
//        });
//        it("Should throw if no dependancies", () => {
//            const test = () => func();
//            assert.throws(test);
//        });
//        it("Should throw if id is not a number", () => {
//            const test = () =>
//                func({
//                    id: "1",
//                    contamination: "test",
//                });
//            assert.throws(test);
//        });
//        it("Should throw if contamination is not a string", () => {
//            const test = () =>
//                func({
//                    id: 1,
//                    contamination: 1,
//                });
//            assert.throws(test);
//        });
//        it("Should not allow the id to be changed", () => {
//            const result = func({
//                id: 1,
//                contamination: "test",
//            });
//            result.id = 5;
//            assert.strictEqual(result.id, 1);
//        });
//        it("Should not allow the contamination to be changed", () => {
//            const result = func({
//                id: 1,
//                contamination: "test",
//            });
//            result.contamination = "cat";
//            assert.strictEqual(result.contamination, "test");
//        });
//    });
//});
