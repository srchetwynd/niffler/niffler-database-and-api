//const buildDegreeOfContamination = require("../DegreeOfContamination");
//
//const assert = require("assert");
//
//describe("Degree of Contamination", () => {
//    it("Should throw if no dependancies passed", () => {
//        const test = () => buildDegreeOfContamination();
//        assert.throws(test);
//    });
//    it("Should throw if fetchAll is not a function", () => {
//        const test = () =>
//            buildDegreeOfContamination({
//                fetchAll: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should not throw if correct dependancies", () => {
//        const test = () => {
//            const result = buildDegreeOfContamination({
//                fetchAll: () => [],
//            });
//            assert.strictEqual(typeof result, "object");
//            assert.strictEqual(typeof result.getContamination, "function");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("Returned Obj", () => {
//        let obj;
//        before(() => {
//            obj = buildDegreeOfContamination({
//                fetchAll: () => [],
//            });
//        });
//        it("Should have a getContamination function", () => {
//            assert.strictEqual(typeof obj.getContamination, "function");
//        });
//        describe("getContamination", () => {
//            it("should return an array", () => {
//                const result = obj.getContamination();
//                assert.deepStrictEqual(result, []);
//            });
//        });
//    });
//});
