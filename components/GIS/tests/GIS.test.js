//const gis = require("../GIS");
//const sinon = require("sinon");
//const assert = require("assert");
//const { db } = require("../../Database");
//
//describe("gis handler", () => {
//    describe("getGIS", () => {
//        const dbStub = {};
//
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should reject if no type provided", async () => {
//            const req = {
//                company: "test",
//                limit: 4,
//                offset: 0,
//                trenchID: 5,
//            };
//            const test = () => gis.getGIS(req);
//            assert.rejects(test);
//        });
//
//        it("Should not reject if point type provided", async () => {
//            const req = {
//                company: "test",
//                limit: 4,
//                offset: 0,
//                type: "point",
//                trenchID: 5,
//            };
//            const test = () => gis.getGIS(req);
//            assert.doesNotReject(test);
//        });
//
//        it("Should not reject if polyline type provided", async () => {
//            const req = {
//                company: "test",
//                limit: 4,
//                offset: 0,
//                type: "polyline",
//                trenchID: 5,
//            };
//            const test = () => gis.getGIS(req);
//            assert.doesNotReject(test);
//        });
//
//        it("Should not reject if polygon type provided", async () => {
//            const req = {
//                company: "test",
//                limit: 4,
//                offset: 0,
//                type: "polygon",
//                trenchID: 5,
//            };
//            const test = () => gis.getGIS(req);
//            assert.doesNotReject(test);
//        });
//
//        it("Should get all GIS for a trench", async () => {
//            dbStub.any = sinon.stub(db, "any");
//            dbStub.any.callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 4,
//                offset: 0,
//                type: "point",
//                trenchID: 5,
//            };
//            const result = await gis.getGIS(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should get a GIS", async () => {
//            dbStub.oneOrNone = sinon.stub(db, "oneOrNone");
//            dbStub.oneOrNone.callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 4,
//                offset: 0,
//                id: 5,
//                type: "point",
//            };
//            const result = await gis.getGIS(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if no GIS", async () => {
//            dbStub.oneOrNone = sinon.stub(db, "oneOrNone");
//            dbStub.oneOrNone.callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 4,
//                offset: 0,
//                id: 5,
//            };
//            const test = () => gis.getGIS(req);
//            await assert.rejects(test);
//        });
//    });
//    describe.skip("postGIS", () => {});
//    describe("deleteGIS", () => {
//        const dbStub = {};
//
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should reject if no type is sent", async () => {
//            const req = {
//                company: "test",
//                type: "cat",
//            };
//            const test = () => gis.deleteGIS(req);
//            assert.rejects(test);
//        });
//
//        it("Should reject if no GIS is found", async () => {
//            dbStub.result = sinon.stub();
//            dbStub.result.callsFake(() => Promise.resolve({ rowCount: 0 }));
//            const req = {
//                company: "test",
//                type: "point",
//            };
//            const test = () => gis.deleteGIS(req);
//            assert.rejects(test);
//        });
//
//        it("Should delete a GIS", async () => {
//            dbStub.result = sinon.stub();
//            dbStub.result.callsFake(() => Promise.resolve({ rowCount: 1 }));
//            const req = {
//                company: "test",
//                type: "point",
//            };
//            const test = () => gis.deleteGIS(req);
//            assert.rejects(test);
//        });
//    });
//});
