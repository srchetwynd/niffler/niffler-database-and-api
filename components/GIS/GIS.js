//const geoConverter = require("./GeoConverter");
//const Point = require("./Point_DAO");
//const Polyline = require("./Polyline_DAO");
//const Polygon = require("./Polygon_DAO");
//
//const createError = require("http-errors");
//const fs = require("fs");
//
//function getGISType(type) {
//    if (type === "point") {
//        return new Point();
//    } else if (type === "polyline") {
//        return new Polyline();
//    } else if (type === "polygon") {
//        return new Polygon();
//    } else {
//        throw createError(400, "No GIS type provided");
//    }
//}
//
//async function getGIS(options) {
//    const dbType = getGISType(options.type);
//    dbType
//        .setSchema(options.company)
//        .setLimit(options.limit)
//        .setOffset(options.offset);
//    if (options.id) {
//        const data = dbType.setColumns("id", options.id).fetchOne();
//        if (!data) {
//            throw createError(404, "GIS not found");
//        }
//        return data;
//    } else {
//        return dbType.setColumns("trench", options.trenchID).fetchAll();
//    }
//}
//
//async function postGIS(options) {
//    const dbType = getGISType(options.type).setSchema(options.company);
//
//    await fs.renameSync(options.geometryFile, "/tmp/" + options.geometryName);
//    let geometry = await geoConverter.shxToGeoJSON(
//        "/tmp/" + options.geometryName
//    );
//    geometry = geometry.features.map((el) => {
//        el.geometry.crs = geometry.crs;
//        return el.geometry;
//    });
//
//    return dbType
//        .setColumns({
//            project: Number(options.projectID),
//            site: Number(options.siteID),
//            trench: Number(options.trenchID),
//            geometry: JSON.stringify(geometry),
//        })
//        .insert();
//}
//
//async function deleteGIS(options) {
//    const dbType = getGISType(options.type).setSchema(options.company);
//    const result = await dbType.setColumns("id", options.id).delete();
//    if (result.rowCount === 0) {
//        throw createError(404, "GIS not found");
//    }
//    return;
//}
//
//module.exports = {
//    getGIS,
//    postGIS,
//    deleteGIS,
//};
