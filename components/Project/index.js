"use strict";

/** @module Project */

const db = require("../Database");
const helpers = require("../Helpers");

const makeProject = require("./Project_Entity")({
    expectType: helpers.expectType,
});

const projectDAL = require("./Project_DAL")({
    db: db,
    makeProject,
    expectType: helpers.expectType,
});

const createAProject = require("./Project_Create")({
    insertProject: projectDAL.insert,
    expectType: helpers.expectType,
});

const deleteProject = require("./Project_Delete")({
    deleteProject: projectDAL.deleteProject,
    expectType: helpers.expectType,
});

const updateProject = require("./Project_Update")({
    updateProject: projectDAL.update,
    expectType: helpers.expectType,
});

const getProject = require("./Project_Get")({
    fetchProjects: projectDAL.fetch,
    countProjects: projectDAL.count,
    expectType: helpers.expectType,
});

module.exports = {
    createAProject,
    deleteProject,
    updateProject,
    getProject,
};
