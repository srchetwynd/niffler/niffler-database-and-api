"use strict";

/** @module Project */

/**
 * @function buildUpdateProjectUseCase
 * @description builds an object containing update use cases
 * @private
 * @param {object} deps Object containing dependancies
 * @param {Function} deps.expectType function to validate types
 * @param {Function} deps.updateProject function to update a project
 * @returns {object} contining update project functions
 */
function buildUpdateProjectUseCase({ expectType, updateProject }) {
    expectType("function", expectType, "expectType");
    expectType("function", updateProject, "updateProject");

    /**
     * @function updateAProject
     * @description updates a Project
     * @param {object} project projectObject
     * @param {string} project.company the company the project belongs to
     * @param {string} project.name the name of the project
     * @param {string} project.code the code for the project
     * @param {string} project.description description of the project
     * @returns {object} project just updated
     */
    function updateAProject({ company, name, code, description }) {
        expectType("string", company, "company");
        expectType("string", name, "name");
        expectType("string", code, "code");
        expectType("string", description, "description");

        return updateProject({
            company,
            name,
            code,
            description,
        });
    }

    return Object.freeze({
        updateAProject,
    });
}

module.exports = buildUpdateProjectUseCase;
