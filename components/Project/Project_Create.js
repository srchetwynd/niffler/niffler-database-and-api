"use strict";

/** @module Project */

/**
 * @function buildCreateProjectUseCase
 * @description builds a function which updates projects
 * @private
 * @param {object} deps Object containing dependancies
 * @param {Function} deps.insertProject inserts the project into storage
 * @param {Function} deps.expectType function to validate types
 * @returns {object} object containing create project functions
 */
function buildCreateProjectUseCase({ insertProject, expectType } = {}) {
    expectType("function", insertProject, "insertProject");
    expectType("function", expectType, "expectType");

    /**
     * @function createAProject
     * @description creates a Project
     * @param {object} project Project object
     * @param {string} project.company company the project belongs to
     * @param {string} project.name name of the project
     * @param {string} project.code code for the project
     * @param {string} project.description description of the project
     * @returns {object} created project
     */
    function createAProject({ company, name, code, description } = {}) {
        expectType("string", company, "company");
        expectType("string", name, "name");
        expectType("string", code, "code");
        expectType("string", description, "description");

        return insertProject({
            company,
            name,
            code,
            description,
        });
    }

    return Object.freeze({
        createAProject,
    });
}

module.exports = buildCreateProjectUseCase;
