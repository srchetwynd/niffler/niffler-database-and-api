"use strict";

/** @module Project */

/**
 * @function buildProjectDAL
 * @description builds Project DAL
 * @private
 * @param {object} dependancies object containing dependancies
 * @param {object} dependancies.db object containing database function
 * @param {Function} dependancies.makeProject function for making a project object
 * @param {Function} dependancies.expectType function to validate types
 * @returns {object} containing functions for the project DAL
 */
function buildProjectDAL({ db, makeProject, expectType } = {}) {
    expectType("object", db, "db");
    expectType("function", makeProject, "makeProject");
    expectType("function", expectType, "expectType");

    /**
     * @function count
     * @description returns a count of matching records
     * @param {object} project a project object with items to search for
     * @param {string} project.company the company which the project belogs to
     * @param {string} [project.name] the name of the project to search for
     * @param {string} [project.code] the code to search for
     * @returns {number} the number of results which match the search requirements
     */
    async function count({ company, name, code } = {}) {
        expectType("string", company, "company");
        expectType(["string", "undefined"], name, "name");
        expectType(["string", "undefined"], code, "code");

        const results = await db.any(
            `
            SELECT
                COUNT(id) as count
            FROM
                $1~.projects
            WHERE
                name = COALESCE($3, name) AND
                code = COALESCE($4, code)
        `,
            [company, name, code]
        );
        return results[0].count;
    }

    /**
     * @function fetch
     * @description returns all Project
     * @param {object} project object containing terms to search for
     * @param {string} project.company company which the project belogs to
     * @param {number} [project.id] id of project to search for
     * @param {string} [project.name] name of project to search for
     * @param {string} [project.code] code of project to search for
     * @param {number} project.limit the number of records to retrieve
     * @param {number} project.offset the offset to retrieve from
     * @returns {Array<object>} Array of project objects
     */
    async function fetch({ company, id, name, code, limit, offset } = {}) {
        expectType("string", company, "company");
        expectType(["number", "undefined"], id, "id");
        expectType(["string", "undefined"], name, "name");
        expectType(["string", "undefined"], code, "code");
        expectType("number", limit, "limit");
        expectType("number", offset, "offset");

        const results = await db.any(
            `
            SELECT
                *
            FROM
                $1~.projects
            WHERE
                id = COALESCE($2, id) AND
                name = COALESCE($3, name) AND
                code = COALESCE($4, code)
            LIMIT $5
            OFFSET $6
        `,
            [company, id, name, code, limit, offset]
        );

        return results.map(makeProject);
    }

    /**
     * @function insert
     * @description inserts a new project
     * @param {object} project a project object
     * @param {string} project.company the company the project belongs to
     * @param {string} project.name the name of the project
     * @param {string} project.code the code of the project
     * @param {string} project.description the description of the project
     * @returns {Array} an array of project objects
     */
    async function insert({ company, name, code, description } = {}) {
        expectType("string", company, "company");
        expectType("string", name, "name");
        expectType("string", code, "code");
        expectType("string", description, "description");

        const result = await db.any(
            `
            INSERT INTO $1~.projects
                (name, code, description)
            VALUES
                ($2, $3, $4)
            RETURNS *
        `,
            [company, name, code, description]
        );

        return result.map(makeProject);
    }

    /**
     * @function deleteProject
     * @description deletes a project
     * @param {object} project project object
     * @param {number} project.id the id of the project to delete
     * @param {string} project.company the company the project belongs to
     * @returns {Promise} promise which resolves if the project is deleted
     */
    async function deleteProject({ company, id } = {}) {
        expectType("string", company, "company");
        expectType("number", id, "id");

        return db.any(
            `
            DELETE FROM
                $1~.projects
            WHERE
                id = $2
        `,
            [company, id]
        );
    }

    /**
     * @function update
     * @description inserts a new project
     * @param {object} project project object
     * @param {number} project.id the id of the project to update
     * @param {string} project.company the compnay the project belongs to
     * @param {string} project.name the name of the project
     * @param {string} project.code the project code
     * @param {string} project.description a description of the project
     * @returns {object} the updated project object
     */
    async function update({ id, company, name, code, description } = {}) {
        expectType("number", id, "id");
        expectType("string", company, "company");
        expectType("string", name, "name");
        expectType("string", code, "code");
        expectType("string", description, "description");

        const result = db.any(
            `
            UPDATE
                $1~.project
            SET
                name = $2,
                code = $3,
                description = $4
            WHERE
                id = $5
            RETURNING *
        `,
            [company, name, code, description, id]
        );

        return result.map(makeProject)[0];
    }

    return {
        fetch,
        insert,
        deleteProject,
        update,
        count,
    };
}

module.exports = buildProjectDAL;
