const assert = require("assert");
const buildGetProjectUseCase = require("../Project_Get");
const helpers = require("../../Helpers");

describe("buildGetProjectUseCase", () => {
    it("Should throw if no dependancies", () => {
        assert.throws(buildGetProjectUseCase);
    });
    it("Should throw if fetchProjects is not a function", () => {
        const test = () =>
            buildGetProjectUseCase({
                countProjects: (val) => val,
                expectType: helpers.expectType,
            });
        assert.throws(test);
    });
    it("Should throw if countProjects is not a function", () => {
        const test = () =>
            buildGetProjectUseCase({
                fetchProjects: (val) => val,
                expectType: helpers.expectType,
            });
        assert.throws(test);
    });
    it("Should throw if expectType is not a function", () => {
        const test = () =>
            buildGetProjectUseCase({
                fetchProjects: (val) => val,
                countProjects: (val) => val,
            });
        assert.throws(test);
    });
    describe("getAProject", () => {
        let getAProject;
        before(() => {
            const result = buildGetProjectUseCase({
                fetchProjects: (val) => [val],
                countProjects: (val) => val,
                expectType: helpers.expectType,
            });
            getAProject = result.getAProject;
        });
        it("Should reject if no company", async () => {
            const test = () =>
                getAProject({
                    id: 1,
                });
            assert.rejects(test);
        });
        it("Should reject if id is not a number", async () => {
            const test = () =>
                getAProject({
                    company: "test",
                });
            assert.rejects(test);
        });
        it("should return a result", async () => {
            const result = await getAProject({
                company: "test",
                id: 1,
            });
            assert.deepStrictEqual(result, {
                company: "test",
                id: 1,
                limit: 100,
                offset: 0,
            });
        });
        it("Should reject if more than one result", async () => {
            const func = buildGetProjectUseCase({
                fetchProjects: (val) => [...val, "foo"],
                countProjects: (val) => val,
                expectType: helpers.expectType,
            }).getAProject;
            const test = () =>
                func({
                    company: "test",
                    id: 1,
                });
            assert.rejects(test);
        });
    });
});
