const assert = require("assert");
const buildDeleteProjectUseCase = require("../Project_Delete");
const helpers = require("../../Helpers");

describe("buildDeleteProjectUseCase", () => {
    it("Should throw with no dependancies", () => {
        assert.throws(buildDeleteProjectUseCase);
    });
    it("Should throw if deleteProject is not a function", () => {
        const test = () =>
            buildDeleteProjectUseCase({
                expectType: helpers.expectType,
            });
        assert.throws(test);
    });
    it("Should throw if expectType is not a function", () => {
        const test = () =>
            buildDeleteProjectUseCase({
                deleteProject: (val) => val,
            });
        assert.throws(test);
    });
    it("Should return a frozen object", () => {
        const result = buildDeleteProjectUseCase({
            expectType: helpers.expectType,
            deleteProject: (val) => val,
        });
        assert.strictEqual(typeof result, "object");
        assert.strictEqual(Object.isFrozen(result), true);
        assert.strictEqual(typeof result.deleteAProject, "function");
    });
    describe("deleteAProject", () => {
        let deleteAProject;
        before(() => {
            const result = buildDeleteProjectUseCase({
                expectType: helpers.expectType,
                deleteProject: (val) => val,
            });
            deleteAProject = result.deleteAProject;
        });
        it("Should throw if no params", () => {
            assert.throws(deleteAProject);
        });
        it("Should throw if company is not a string", () => {
            const test = () =>
                buildDeleteProjectUseCase({
                    id: 1,
                });
            assert.throws(test);
        });
        it("Should throw if id is not a string", () => {
            const test = () =>
                deleteAProject({
                    company: "test",
                });
            assert.throws(test);
        });
        it("Should not throw", () => {
            const test = () =>
                deleteAProject({
                    id: 1,
                    company: "test",
                });
            assert.doesNotThrow(test);
        });
    });
});
