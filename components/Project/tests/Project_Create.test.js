const assert = require("assert");
const buildCreateProjectUseCase = require("../Project_Create");
const helpers = require("../../Helpers");

describe("buildCreateProjectUseCase", () => {
    it("Should throw if no dependancies", () => {
        assert.throws(buildCreateProjectUseCase);
    });
    it("Should throw if insertProject is not a function", () => {
        const test = () =>
            buildCreateProjectUseCase({
                expectType: helpers.expectType,
            });
        assert.throws(test);
    });
    it("Should throw if expectType is not a function", () => {
        const test = () =>
            buildCreateProjectUseCase({
                insertProject: (val) => val,
            });
        assert.throws(test);
    });
    it("Should return an object", () => {
        const result = buildCreateProjectUseCase({
            insertProject: (val) => val,
            expectType: helpers.expectType,
        });
        assert.strictEqual(typeof result, "object");
        assert.strictEqual(Object.isFrozen(result), true);
        assert.strictEqual(typeof result.createAProject, "function");
    });
    describe("createAProject", () => {
        let createAProject;
        before(() => {
            const result = buildCreateProjectUseCase({
                insertProject: (val) => val,
                expectType: helpers.expectType,
            });
            createAProject = result.createAProject;
        });
        it("Should throw on no arguments", () => {
            assert.throws(createAProject);
        });
        it("Should throw on company not a string", () => {
            const test = () =>
                createAProject({
                    name: "test",
                    code: "test",
                    description: "test",
                });
            assert.throws(test);
        });
        it("Should throw on name not a string", () => {
            const test = () =>
                createAProject({
                    company: "test",
                    code: "test",
                    description: "test",
                });
            assert.throws(test);
        });
        it("Should throw on code not a string", () => {
            const test = () =>
                createAProject({
                    company: "test",
                    name: "test",
                    description: "test",
                });
            assert.throws(test);
        });
        it("Should throw on description not a string", () => {
            const test = () =>
                createAProject({
                    company: "test",
                    name: "test",
                    code: "test",
                });
            assert.throws(test);
        });
        it("Should return an object", () => {
            const result = createAProject({
                company: "test",
                name: "test",
                code: "test",
                description: "test",
            });
            assert.deepStrictEqual(result, {
                company: "test",
                name: "test",
                code: "test",
                description: "test",
            });
        });
    });
});
