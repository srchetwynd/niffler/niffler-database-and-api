const buildProjectDAL = require("../Project_DAL");
const { expectType } = require("../../Helpers");

const assert = require("assert");

describe("buildProjectDAL", () => {
    it("should be a function", () => {
        assert.strictEqual(typeof buildProjectDAL, "function");
    });
    it("Should throw if no dependancies", () => {
        assert.throws(buildProjectDAL);
    });
    it("Should throw if db is not an object", () => {
        const test = () =>
            buildProjectDAL({
                db: "test",
                makeProject: () => [],
                expectType,
            });
        assert.throws(test);
    });
    it("Should throw if makeProject is not a function", () => {
        const test = () =>
            buildProjectDAL({
                db: {},
                makeProject: "test",
                expectType,
            });
        assert.throws(test);
    });
    it("Should throw if expectType is not a function", () => {
        const test = () =>
            buildProjectDAL({
                db: {},
                makeProject: () => [],
                expectType: "test",
            });
        assert.throws(test);
    });
    describe("count", () => {
        let count;
        before(() => {
            const result = buildProjectDAL({
                db: {
                    any: () => [{ count: 1 }],
                },
                makeProject: (val) => val,
                expectType,
            });
            count = result.count;
        });
        it("Should throw if nothing is passed in", async () => {
            await assert.rejects(count);
        });
        it("Should throw if company is not a string", async () => {
            const test = () => count({ company: 1 });
            await assert.rejects(test);
        });
        it("Should be ok without a name or code", async () => {
            const result = await count({ company: "test" });
            assert.deepStrictEqual(result, 1);
        });
        it("Should count with a name and code", async () => {
            const result = await count({
                company: "test",
                name: "name",
                code: "code",
            });
            assert.deepStrictEqual(result, 1);
        });
    });
    describe("fetch", () => {
        let fetchProject;
        before(() => {
            const result = buildProjectDAL({
                db: {
                    any: (...val) => [val],
                },
                makeProject: (val) => val,
                expectType,
            });
            fetchProject = result.fetch;
        });
        it("Should reject if no input", async () => {
            await assert.rejects(fetchProject);
        });
        it("Should reject if company is not a string", async () => {
            const test = () =>
                fetchProject({
                    limit: 1,
                    offset: 1,
                });
            await assert.rejects(test);
        });
        it("Should reject if id is not a number", async () => {
            const test = () =>
                fetchProject({
                    company: "test",
                    id: "test",
                    limit: 1,
                    offset: 1,
                });
            await assert.rejects(test);
        });
        it("Should reject if name is not a string", async () => {
            const test = () =>
                fetchProject({
                    company: "test",
                    name: 1,
                    limit: 1,
                    offset: 1,
                });
            await assert.rejects(test);
        });
        it("Should reject if code is not a string", async () => {
            const test = () =>
                fetchProject({
                    company: "test",
                    code: 1,
                    limit: 1,
                    offset: 1,
                });
            await assert.rejects(test);
        });
        it("Should reject if limit is not a number", async () => {
            const test = () =>
                fetchProject({
                    company: "test",
                    offset: 1,
                });
            await assert.rejects(test);
        });
        it("Should reject if offset is not a number", async () => {
            const test = () =>
                fetchProject({
                    company: "test",
                    limit: 1,
                });
            await assert.rejects(test);
        });
        it("Should return an array of results", async () => {
            const result = await fetchProject({
                company: "test",
                limit: 1,
                offset: 1,
            });
            assert.strictEqual(Array.isArray(result), true);
            assert.deepStrictEqual(result[0][1], [
                "test",
                undefined,
                undefined,
                undefined,
                1,
                1,
            ]);
        });
    });
    describe("insert", () => {
        let insertProject;
        before(() => {
            const result = buildProjectDAL({
                db: {
                    any: (...val) => [val],
                },
                makeProject: (val) => val,
                expectType,
            });
            insertProject = result.insert;
        });
        it("Should reject if no arguments passed", async () => {
            await assert.rejects(insertProject);
        });
        it("Should reject if company is not a string", async () => {
            const test = () =>
                insertProject({
                    name: "test",
                    code: "test",
                    description: "test",
                });
            await assert.rejects(test);
        });
        it("Should reject if name is not a string", async () => {
            const test = () =>
                insertProject({
                    company: "test",
                    code: "test",
                    description: "test",
                });
            await assert.rejects(test);
        });
        it("Should reject if code is not a string", async () => {
            const test = () =>
                insertProject({
                    company: "test",
                    name: "test",
                    description: "test",
                });
            await assert.rejects(test);
        });
        it("Should reject if description is not a string", async () => {
            const test = () =>
                insertProject({
                    company: "test",
                    name: "test",
                    code: "test",
                });
            await assert.rejects(test);
        });
        it("Should return an array", async () => {
            const result = await insertProject({
                company: "company",
                name: "name",
                code: "code",
                description: "description",
            });
            assert.strictEqual(Array.isArray(result), true);
            assert.deepStrictEqual(result[0][1], [
                "company",
                "name",
                "code",
                "description",
            ]);
        });
    });
    describe("delete", () => {
        let deleteProject;
        before(() => {
            const result = buildProjectDAL({
                db: {
                    any: (...val) => [val],
                },
                makeProject: (val) => val,
                expectType,
            });
            deleteProject = result.deleteProject;
        });
        it("Should reject if no arguments passed", async () => {
            await assert.rejects(deleteProject);
        });
        it("Should reject if company is not a string", async () => {
            const test = () =>
                deleteProject({
                    id: 1,
                });
            await assert.rejects(test);
        });
        it("Should reject if id is not a number", async () => {
            const test = () =>
                deleteProject({
                    company: "test",
                });
            await assert.rejects(test);
        });
        it("Should not reject", async () => {
            const test = () =>
                deleteProject({
                    company: "test",
                    id: 1,
                });
            await assert.doesNotReject(test);
        });
    });
    describe("update", () => {
        let updateProject;
        before(() => {
            const result = buildProjectDAL({
                db: {
                    any: (...val) => [val],
                },
                makeProject: (val) => val,
                expectType,
            });
            updateProject = result.update;
        });
        it("Should reject if no arguments passed", async () => {
            await assert.rejects(updateProject);
        });
        it("Should reject if no id is passed", async () => {
            const test = () =>
                updateProject({
                    name: "test",
                    company: "test",
                    code: "test",
                    description: "test",
                });
            await assert.rejects(test);
        });
        it("Should reject if company is not a string", async () => {
            const test = () =>
                updateProject({
                    id: 1,
                    name: "test",
                    code: "test",
                    description: "test",
                });
            await assert.rejects(test);
        });
        it("Should reject if name is not a string", async () => {
            const test = () =>
                updateProject({
                    id: 1,
                    company: "test",
                    code: "test",
                    description: "test",
                });
            await assert.rejects(test);
        });
        it("Should reject if code is not a string", async () => {
            const test = () =>
                updateProject({
                    id: 1,
                    company: "test",
                    name: "test",
                    description: "test",
                });
            await assert.rejects(test);
        });
        it("Should reject if description is not a string", async () => {
            const test = () =>
                updateProject({
                    id: 1,
                    company: "test",
                    name: "test",
                    code: "test",
                });
            await assert.rejects(test);
        });
        it("Should return an object", async () => {
            const result = await updateProject({
                id: 1,
                company: "company",
                name: "name",
                code: "code",
                description: "description",
            });
            assert.strictEqual(typeof result, "object");
            assert.deepStrictEqual(result[1], [
                "company",
                "name",
                "code",
                "description",
                1,
            ]);
        });
    });
});
