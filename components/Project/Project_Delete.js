"use strict";

/** @module Project */

/**
 * @function buildDeleteProjectUseCase
 * @description builds an object containing delete project use case
 * @private
 * @param {object} deps object containing dependancies
 * @param {Function} deps.deleteProject function to delete a project
 * @param {Function} deps.expectType function to validate types
 * @returns {object} object containing delete project function
 */
function buildDeleteProjectUseCase({ deleteProject, expectType } = {}) {
    expectType("function", deleteProject, "deleteProject");
    expectType("function", expectType, "expectType");

    /**
     * @function deleteAProject
     * @description deletes a project
     * @param {object} project project object
     * @param {string} project.company company the project belongs to
     * @param {number} project.id id of the project
     * @returns {Promise} of project being deleted
     */
    function deleteAProject({ company, id } = {}) {
        expectType("string", company, "company");
        expectType("number", id, "id");

        return deleteProject({
            company,
            id,
        });
    }

    return Object.freeze({
        deleteAProject,
    });
}

module.exports = buildDeleteProjectUseCase;
