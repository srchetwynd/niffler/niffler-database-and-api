"use strict";

/** @module Project */

/**
 * @function buildGetProjectUseCase
 * @description Builds a function which gets projects
 * @private
 * @param {object} dependancies dependancies
 * @param {Function} dependancies.fetchProjects funciton to search for projects
 * @param {Function} dependancies.countProjects function to count matching projects
 * @param {Function} dependancies.expectType function to validate the type of a var
 * @returns {object} object containing functions
 */
function buildGetProjectUseCase({
    fetchProjects,
    countProjects,
    expectType,
} = {}) {
    expectType("function", fetchProjects, "fetchProjects");
    expectType("function", countProjects, "countProjects");
    expectType("function", expectType, "expectType");

    /**
     * @function getAProject
     * @description gets a project using its id
     * @throws Error if more than one result
     * @param {object} options options to get project by
     * @param {string} options.company company the project belongs to
     * @param {number} options.id id of the project
     * @returns {object} project object
     */
    async function getAProject({ company, id } = {}) {
        expectType("string", company, "company");
        expectType("number", id, "id");
        const result = await fetchProjects({
            company,
            id,
            limit: 100,
            offset: 0,
        });
        if (result.length > 1) {
            throw new Error(`Expected 1 result. Got: ${result.length}`);
        }
        return result[0];
    }

    /**
     * @function getProjects
     * @description gets multiple projects
     * @param {object} project project to search for
     * @param {string} project.company company whose project to search
     * @param {string} [project.name] name to search for
     * @param {string} [project.code] code to search for
     * @param {number} [project.limit] limit of results
     * @param {number} [project.offset] offset of results
     * @returns {Array} array of project objects
     */
    async function getProjects({
        company,
        name,
        code,
        limit = 0,
        offset = 10,
    } = {}) {
        expectType(["string", "undefined"], name, "name");
        expectType(["string", "undefined"], code, "code");
        expectType("string", company, "company");
        expectType("number", limit, "limit");
        expectType("number", offset, "offset");

        const results = await fetchProjects({
            name,
            code,
            limit,
            offset,
            company,
        });
        const count = await countProjects({
            name,
            code,
            company,
        });
        return {
            results: results,
            count: count,
        };
    }

    return {
        getAProject,
        getProjects,
    };
}

module.exports = buildGetProjectUseCase;
