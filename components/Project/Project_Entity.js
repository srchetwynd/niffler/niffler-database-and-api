"use strict";

/** @module Project */

/**
 * @function buildMakeProject
 * @description builds a function which makes projects
 * @private
 * @param {object} deps dependancies
 * @param {Function} deps.expectType functio to validate the type of values
 * @returns {Function} function which builds projects
 */
function buildMakeProject({ expectType }) {
    /**
     * @function makeProject
     * @description makes a project
     * @param {object} project project object
     * @param {number} project.id id of the project
     * @param {string} project.name name of the project
     * @param {string} project.code code given to the project
     * @param {string} project.description description of the project
     * @returns {object} frozen object of the project
     */
    return function makeProject({ id, name, code, description } = {}) {
        expectType("number", id, "id");
        expectType("string", name, "name");
        expectType("string", code, "code");
        expectType("string", description, "description");

        return Object.freeze({
            id,
            name,
            code,
            description,
        });
    };
}

module.exports = buildMakeProject;
