//const contextDetail = require("./ContextDetail");
//const links = require("../../middleware/links");
//const urls = require("../../helpers/urls");
//
//function convertValues(req, res, next) {
//    req.options = {
//        company: req.company,
//        contextID: req.params.contextID,
//    };
//    return next();
//}
//
//function setResource(resource) {
//    return (req, res, next) => {
//        req.resource = resource;
//        return next();
//    };
//}
//
//function setFunction(func) {
//    return (req, res, next) => {
//        req.func = func;
//        return next();
//    };
//}
//
//const allRoutes = [
//    urls.context_detail.url,
//    links.buildDataLinks([
//        "interpretations",
//        "metalDetectorLocations",
//        "orientation",
//        "photoTypes",
//        "sievingLocations",
//        "unit",
//    ]),
//    links.buildDataRelationships([
//        "project",
//        "site",
//        "trench",
//        "context",
//        "context_detail",
//        "aboveBelow",
//    ]),
//    links.buildResourceLinks([
//        "interpretations",
//        "metalDetectorLocations",
//        "orientation",
//        "photoTypes",
//        "sievingLocations",
//        "unit",
//    ]),
//    convertValues,
//    setResource("ContextDetail"),
//];
//
//module.exports = {
//    url:
//        "/project/:projectID/site/:siteID/trench/:trenchID/context/:contextID/details",
//    get: [...allRoutes, setFunction(contextDetail.getContextDetail)],
//};
