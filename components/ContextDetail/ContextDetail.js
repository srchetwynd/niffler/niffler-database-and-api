//const { contextTypeDAO } = require("../ContextType");
//const {
//    coffinDAO,
//    cutDAO,
//    depositDAO,
//    masonryDAO,
//    skeletonDAO,
//    timberDAO,
//    contextDAO,
//} = require("../Context");
//const { aboveBelowDAO } = require("../AboveBelow");
//const { contextSameAsDAO } = require("../ContextSameAs");
//const { photoContextDAO } = require("../PhotoContext");
//const { drawingContextDAO } = require("../DrawingContext");
//
//const createError = require("http-errors");
//
//async function getContextDetail(options) {
//    let data = {};
//    data.context = await new contextDAO()
//        .setSchema(options.company)
//        .setColumns("id", options.contextID)
//        .fetchOne();
//    if (!data.context) {
//        throw createError(404, "Context not found");
//    }
//    data.id = data.context.id;
//
//    const type = (
//        await new contextTypeDAO()
//            .setColumns("id", data.context.context_type)
//            .fetchOne()
//    ).context_type.toLowerCase();
//
//    let dbType;
//    if (type === "coffin") {
//        dbType = new coffinDAO();
//    } else if (type === "cut") {
//        dbType = new cutDAO();
//    } else if (type === "deposit") {
//        dbType = new depositDAO();
//    } else if (type === "masonry") {
//        dbType = new masonryDAO();
//    } else if (type === "skeleton") {
//        dbType = new skeletonDAO();
//    } else if (type === "timber") {
//        dbType = new timberDAO();
//    }
//    dbType.setSchema(options.company).setColumns("id", options.contextID);
//    data.details = await dbType.fetchOne();
//
//    data.aboves = (
//        await new aboveBelowDAO()
//            .setSchema(options.company)
//            .setColumns("below", options.contextID)
//            .fetchAll()
//    ).map((el) => el.above);
//
//    data.belows = (
//        await new aboveBelowDAO()
//            .setSchema(options.company)
//            .setColumns("above", options.contextID)
//            .fetchAll()
//    ).map((el) => el.below);
//
//    data.same_as = (
//        await new contextSameAsDAO()
//            .setSchema(options.company)
//            .setColumns("context_1", options.contextID)
//            .fetchAll()
//    ).map((el) => el.context_2);
//
//    data.photos = (
//        await new photoContextDAO()
//            .setSchema(options.company)
//            .setColumns("context", options.contextID)
//            .fetchAll()
//    ).map((el) => el.photo);
//
//    data.drawings = (
//        await new drawingContextDAO()
//            .setSchema(options.company)
//            .setColumns("context", options.contextID)
//            .fetchAll()
//    ).map((el) => el.photo);
//
//    return data;
//}
//
//module.exports = {
//    getContextDetail,
//};
