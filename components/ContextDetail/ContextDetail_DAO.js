//const { db } = require("../Database");
//const {
//    contextDAO,
//    coffinDAO,
//    cutDAO,
//    depositDAO,
//    masonryDAO,
//    skeletonDAO,
//    timberDAO,
//} = require("../Context");
//const { aboveBelowDAO } = require("../AboveBelow");
//const { contextSameAsDAO } = require("../ContextSameAs");
//const { photoContextDAO } = require("../PhotoContext");
//const { drawingContextDAO } = require("../DrawingContext");
//
//async function getByID(id, table, type, company) {
//    return db.one(
//        `
//        SELECT
//            *
//        FROM
//            $4~.$2~
//        WHERE
//            $4~.$2~.$3~ = $1`,
//        [id, table, type, company]
//    );
//}
//
//function insert(
//    context,
//    aboveBelows,
//    sameAs,
//    photoContext,
//    drawingContext,
//    details,
//    type,
//    company
//) {
//    return db.tx((transaction) => {
//        return contextDAO
//            .insert(context, company, transaction)
//            .then((resultContext) => {
//                let queries = [];
//                for (let above of aboveBelows.aboves) {
//                    queries.push(
//                        aboveBelowDAO.insert(
//                            {
//                                above: above,
//                                below: resultContext.id,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                }
//                for (let below of aboveBelows.belows) {
//                    queries.push(
//                        aboveBelowDAO.insert(
//                            {
//                                above: resultContext.id,
//                                below: below,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                }
//                for (let same of sameAs) {
//                    queries.push(
//                        contextSameAsDAO.insert(
//                            {
//                                context_1: resultContext.id,
//                                context_2: same,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                }
//                for (let photo of photoContext.photo) {
//                    queries.push(
//                        photoContextDAO.insert(
//                            {
//                                photo: photo,
//                                context: resultContext.id,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                }
//                for (let drawing of drawingContext.drawing) {
//                    queries.push(
//                        drawingContextDAO.insert(
//                            {
//                                context: resultContext.id,
//                                drawing: drawing,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                }
//                if (type === "coffin") {
//                    queries.push(
//                        coffinDAO.insert(
//                            {
//                                id: resultContext.id,
//                                ...details,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                } else if (type === "cut") {
//                    queries.push(
//                        cutDAO.insert(
//                            {
//                                id: resultContext.id,
//                                ...details,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                } else if (type === "deposit") {
//                    queries.push(
//                        depositDAO.insert(
//                            {
//                                id: resultContext.id,
//                                ...details,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                } else if (type === "masonry") {
//                    queries.push(
//                        masonryDAO.insert(
//                            {
//                                id: resultContext.id,
//                                ...details,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                } else if (type === "skeleton") {
//                    queries.push(
//                        skeletonDAO.insert(
//                            {
//                                id: resultContext.id,
//                                ...details,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                } else if (type === "timber") {
//                    queries.push(
//                        timberDAO.insert(
//                            {
//                                id: resultContext.id,
//                                ...details,
//                            },
//                            company,
//                            transaction
//                        )
//                    );
//                } else {
//                    throw new Error("Context Type Not Recognised");
//                }
//                return Promise.all(queries);
//            });
//    });
//}
//
//module.exports = {
//    getByID,
//    insert,
//};
