//const createError = require("http-errors");
//const pagination = require("../../helpers/pagination");
//const { JsonApiObject, resourceBuilder } = require("../JsonApi");
//const Sentry = require("@sentry/node");
//
//async function getResponder(req, res, next) {
//    try {
//        const data = await req.func(req.options);
//
//        let response = new JsonApiObject().setData(
//            resourceBuilder(req, data, req.resource)
//        );
//
//        const paginationLinks = pagination.getLinks({ data, req });
//
//        Object.entries(paginationLinks).forEach((el) => {
//            response.setLinks(el[1], el[0]);
//        });
//
//        Object.entries(req.resourceLinks).forEach((el) => {
//            response.setLinks(el[1], el[0]);
//        });
//
//        return res.send(response.build());
//    } catch (err) {
//        return next(err);
//    }
//}
//
//function putResponder(f, resource) {
//    return async (req, res, next) => {
//        try {
//            let data = await f(req);
//
//            let response = new JsonApiObject()
//                .setData(resourceBuilder(req, data, resource))
//                .setLinks(req.resourceLinks.self, "self");
//
//            return res.json(response.build());
//        } catch (err) {
//            return next(err);
//        }
//    };
//}
//
//function postResponder(f, resource) {
//    return async (req, res, next) => {
//        try {
//            let data = await f(req);
//
//            let response = new JsonApiObject()
//                .setData(resourceBuilder(req, data, resource))
//                .setLinks(req.resourceLinks.self, "self");
//
//            return res.json(response.build());
//        } catch (err) {
//            return next(err);
//        }
//    };
//}
//
//function deleteResponder(f) {
//    return async (req, res, next) => {
//        try {
//            await f(req);
//            res.status(200);
//            return res.json(new JsonApiObject().build());
//        } catch (err) {
//            return next(err);
//        }
//    };
//}
//
//function errorResponder() {
//    return (err, req, res) => {
//        console.log(err);
//        let message, status;
//        if (err.status !== undefined) {
//            res.status(err.status);
//            status = err.status;
//            message = err.message;
//        } else {
//            res.status(500);
//            status = 500;
//            message = "Unexpected Error";
//            Sentry.captureException(err);
//        }
//        return res.json(
//            new JsonApiObject().setError(createError(status, message)).build()
//        );
//    };
//}
//
//module.exports = {
//    getResponder,
//    postResponder,
//    deleteResponder,
//    putResponder,
//    errorResponder,
//};
