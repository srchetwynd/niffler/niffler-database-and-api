/**
 * @function setResource
 * @description sets the resource on the req object
 * @param {object} resource - resource to add to the req
 * @returns {Function} callback function
 */
function setResource(resource) {
    return (req, res, next) => {
        req.resource = resource;
        return next();
    };
}

/**
 * @function setFunction
 * @description sets the get data function on the req function
 * @param {Function} func - the function to add the req object
 * @returns {Function} - middleware function
 */
function setFunction(func) {
    return (req, res, next) => {
        req.func = func;
        return next();
    };
}

module.exports = {
    setResource,
    setFunction,
};
