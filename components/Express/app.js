var createError = require("http-errors");
var express = require("express");
const fileUpload = require("express-fileupload");
const pino = require("express-pino-logger")();
const Sentry = require("@sentry/node");

const routes = require("./routes");
const config = require("../../config");

Sentry.init({
    dsn: config.sentryDSN,
});

var app = express();

if (config.env !== "development") {
    app.use(pino);
} else {
    app.use((req, res, next) => {
        // eslint-disable-next-line no-console
        console.log(req.originalUrl);
        return next();
    });
}

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(
    fileUpload({
        useTempFiles: true,
        tempFileDir: "/tmp/",
    })
);

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,PUT,POST,DELETE");
    res.header("Access-Control-Expose-Headers", "Content-Length");
    res.header(
        "Access-Control-Allow-Headers",
        "Accept, Authorization, Content-Type, X-Request-With, Range"
    );
    if (req.method === "OPTIONS") {
        return res.sendStatus(200);
    } else {
        return next();
    }
});

app.use("/", routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    app.errorResponder(err, req, res, next);
});

module.exports = app;
