//const assert = require("assert");
//const routeFactories = require("../Responders");
//
//describe("routeFactories", () => {
//    describe("getFactory", () => {
//        it("Should return a function", () => {
//            assert.strictEqual(typeof routeFactories.getFactory(), "function");
//        });
//    });
//    describe("postFactory", () => {
//        it("Should return a function", () => {
//            assert.strictEqual(typeof routeFactories.postFactory(), "function");
//        });
//    });
//    describe("putFactory", () => {
//        it("Should return a function", () => {
//            assert.strictEqual(typeof routeFactories.putFactory(), "function");
//        });
//    });
//    describe("deleteFactory", () => {
//        it("Should return a function", () => {
//            assert.strictEqual(
//                typeof routeFactories.deleteFactory(),
//                "function"
//            );
//        });
//
//        describe("returned function", () => {
//            it("Should return a delete jsonApi message", async () => {
//                const res = {
//                    json(result) {
//                        assert.strictEqual(typeof result, "object");
//                    },
//                    status(status) {
//                        assert.strictEqual(status, 200);
//                    },
//                };
//                await routeFactories.deleteFactory(() => Promise.resolve())(
//                    {},
//                    res
//                );
//            });
//        });
//    });
//    describe("errorFactory", () => {
//        it("Should return a function", () => {
//            assert.strictEqual(
//                typeof routeFactories.errorFactory(),
//                "function"
//            );
//        });
//
//        describe("returned function", () => {
//            it("Should return an error jsonApi message", () => {
//                const error = {
//                    status: 400,
//                    message: "test",
//                };
//                const res = {
//                    json(result) {
//                        assert.strictEqual(typeof result, "object");
//                    },
//                    status(status) {
//                        assert.strictEqual(status, 400);
//                    },
//                };
//                routeFactories.errorFactory()(error, {}, res);
//            });
//        });
//    });
//});
