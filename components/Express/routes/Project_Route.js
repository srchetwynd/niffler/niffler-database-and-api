const express = require("express");
const router = express.Router();

const project = require("../../Project");
const pagination = require("../../../middleware/pagination");
const urls = require("../urls");

router.get(urls.project.url, pagination.setLimitOffset, (req, res) => {
    res.json(
        project.getProject({
            company: req.company,
            limit: req.limit,
            offset: req.offset,
            id: req.query.id,
            code: req.query.code,
        })
    );
});

router.post(urls.project.url, (req, res) => {
    res.json(
        project.createProject({
            company: req.company,
            ...req.body,
        })
    );
});

router.put(urls.project.url, (req, res) => {
    res.json(
        project.updateProject({
            company: req.company,
            id: req.query.id,
            ...req.body,
        })
    );
});

router.delete(urls.project.url, (req, res) => {
    res.json(
        project.deleteProject({
            company: req.company,
            id: req.query.id,
        })
    );
});

module.exports = router;
