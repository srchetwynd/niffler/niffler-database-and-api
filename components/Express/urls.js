const urlBuilder = {
    baseUrl: "",
    urlBuilder(urlTemplate) {
        let params = urlTemplate.split("/").reduce((acc, el) => {
            if (el.slice(0, 1) === ":") {
                acc.push(el.slice(1));
            }
            return acc;
        }, []);
        return (options) => {
            let url = params.reduce((acc, el) => {
                if (options && options.params && options.params[el]) {
                    return acc.replace(`:${el}`, options.params[el]);
                }
                throw new Error(`Param ${el} is required but not provided`);
            }, urlTemplate);

            if (options && options.query) {
                return Object.keys(options.query).reduce((acc, el) => {
                    return `${acc}&${el}=${options.query[el]}`;
                }, `${url}?`);
            } else {
                return url;
            }
        };
    },
    setBaseUrl(baseUrl) {
        this.baseUrl = baseUrl;
    },
};

const urls = {
    aboveBelow:
        "/project/:projectID/trench/:trenchID/context/:contextID/above_below",
    coffin:
        "/project/:projectID/site/:siteID/trench/:trenchID/context?type=coffin",
    context: "/project/:projectID/site/:siteID/trench/:trenchID/context",
    context_detail:
        "/project/:projectID/site/:siteID/trench/:trenchID/context/:contextID/details",
    cut: "/project/:projectID/site/:siteID/trench/:trenchID/context?type=cut",
    degreeOfContamination: "/degreeofcontamination",
    deposit:
        "/project/:projectID/site/:siteID/trench/:trenchID/context?type=deposit",
    drawing: "/project/:projectID/site/:siteID/trench/:trenchID/drawing",
    gis: "/project/:projectID/site/:siteID/trench/:trenchID/gis",
    interpretations: "/interpretation",
    masonry:
        "/project/:projectID/site/:siteID/trench/:trenchID/context?type=timber",
    metalDetectorLocations: "/metaldetectinglocation",
    orientation: "/orientation",
    point: "/project/:projectID/site/:siteID/trench/:trenchID/gis?type=point",
    polygon:
        "/project/:projectID/site/:siteID/trench/:trenchID/gis?type=polygon",
    polyline:
        "/project/:projectID/site/:siteID/trench/:trenchID/gis?type=polyline",
    photo: "/project/:projectID/site/:siteID/trench/:trenchID/photos",
    photoTypes: "/phototypes",
    project: "/project",
    sample: "/project/:projectID/site/:siteID/trench/:trenchID/sample",
    section: "/project/:projectID/site/:siteID/trench/:trenchID/section",
    sievingLocations: "/sievinglocation",
    site: "/project/:projectID/site",
    trench: "/project/:projectID/site/:siteID/trench",
    unit: "/unit",
    user: "/user",
    skeleton:
        "/project/:projectID/site/:siteID/trench/:trenchID/context?type=skeleton",
    timber:
        "/project/:projectID/site/:siteID/trench/:trenchID/context?type=timber",
};

const urlsWithBuilder = Object.entries(urls).reduce((acc, el) => {
    acc[el[0]] = {
        url: el[1],
        buildUrl: urlBuilder.urlBuilder(el[1]),
    };
    return acc;
}, {});

module.exports = urlsWithBuilder;
