module.exports.helpers = require("./Helpers");
const responders = require("./Responders");
const routes = require("./Router");
const helpers = require("./Helpers");

module.exports = {
    ...responders,
    routes,
    helpers: helpers,
};
