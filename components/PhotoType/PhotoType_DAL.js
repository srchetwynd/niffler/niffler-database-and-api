//"use strict";
///**
// * @function buildPhotoTypeDAL
// * @description builds PhotoType DAL
// * @param {Object} dependancies
// * @param {Object} dependancies.db
// * @param {Function} dependancies.makePhotoType
// * @return {Object}
// */
//function buildPhotoTypeDAL({ db, makePhotoType } = {}) {
//    if (typeof db !== "object") {
//        throw new Error(`db must be an object got: ${typeof db}`);
//    }
//    if (typeof makePhotoType !== "function") {
//        throw new Error(
//            `makePhotoType must be a function got: ${typeof makePhotoType}`
//        );
//    }
//
//    /**
//     * @function fetchAll
//     * @description returns all PhotoTypes
//     * @return {Array}
//     */
//    async function fetchAll() {
//        const results = await db.any(`
//            SELECT
//                *
//            FROM
//                public.photoTypes
//        `);
//
//        return results.map(makePhotoType);
//    }
//
//    return {
//        fetchAll,
//    };
//}
//
//module.exports = buildPhotoTypeDAL;
