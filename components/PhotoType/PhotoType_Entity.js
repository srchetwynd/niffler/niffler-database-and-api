//"use strict";
///**
// * @function buildMakeInterpretation
// * @description creates a function which makes an interpretations
// * @return {Function}
// */
//function buildMakePhotoType() {
//    /**
//     * @function makeInterpretation
//     * @description makes an interpretation
//     * @param {Object} photoType
//     * @param {Number} photoType.id
//     * @param {String} photoType.interpretation
//     * @return {Object}
//     */
//    return function makePhotoType({ id, photoType }) {
//        if (typeof id !== "number") {
//            throw new Error(`id must be a number got: ${typeof id}`);
//        }
//        if (typeof photoType !== "string") {
//            throw new Error(
//                `photoType must be a string got: ${typeof photoType}`
//            );
//        }
//
//        return Object.freeze({
//            id,
//            photoType,
//        });
//    };
//}
//
//module.exports = buildMakePhotoType;
