//"use strict";
//
///**
// * @function bulidPhotoType
// * @description Builds the photoType functions
// * @param {Object} dependancies
// * @param {Function} dependancies.fetchAll
// * @return {Object}
// */
//function buildPhotoType({ fetchAll }) {
//    if (typeof fetchAll !== "function") {
//        throw new Error(`fetchAll must be a function got: ${typeof fetchAll}`);
//    }
//    /**
//     * @function getPhotoType
//     * @description returns all photoTypes
//     * @return {Array}
//     */
//    function getPhotoType() {
//        return fetchAll();
//    }
//    return {
//        getPhotoType,
//    };
//}
//
//module.exports = buildPhotoType;
