//const buildPhotoType = require("../PhotoType_DAL");
//
//const assert = require("assert");
//
//describe("PhotoType DOL", () => {
//    it("Should throw if no dependancies are passed", () => {
//        const test = () => buildPhotoType();
//        assert.throws(test);
//    });
//    it("Should throw if db is not an object", () => {
//        const test = () =>
//            buildPhotoType({
//                db: "test",
//                makePhotoType: () => {},
//            });
//        assert.throws(test);
//    });
//    it("Should throw if makePhotoType is not a function", () => {
//        const test = () =>
//            buildPhotoType({
//                db: {},
//                makePhotoType: "test",
//            });
//        assert.throws(test);
//    });
//    it("should not throw if the dependancies are right", () => {
//        const test = () => {
//            const result = buildPhotoType({
//                db: {},
//                makePhotoType: () => ({}),
//            });
//            assert.strictEqual(typeof result, "object");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("PhotoTypeDAL", () => {
//        let photoTypeDAL;
//        before(() => {
//            photoTypeDAL = buildPhotoType({
//                db: {
//                    any: () => [],
//                },
//                makePhotoType: () => ({}),
//            });
//        });
//        describe("fetchAll", () => {
//            it("should be a function", () => {
//                assert.strictEqual(typeof photoTypeDAL.fetchAll, "function");
//            });
//            it("should return an array", async () => {
//                assert.deepStrictEqual(await photoTypeDAL.fetchAll(), []);
//            });
//        });
//    });
//});
