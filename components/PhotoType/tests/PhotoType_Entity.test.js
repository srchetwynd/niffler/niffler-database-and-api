//const buildMakePhotoType = require("../PhotoType_Entity");
//
//const assert = require("assert");
//
//describe("buildMakePhotoType", () => {
//    it("Should not throw if the dependancies are right", () => {
//        const test = () => {
//            const result = buildMakePhotoType();
//            assert.strictEqual(typeof result, "function");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("makePhotoType", () => {
//        let makePhotoType;
//        before(() => {
//            makePhotoType = buildMakePhotoType();
//        });
//        it("should throw if id is not a number", () => {
//            const test = () =>
//                makePhotoType({
//                    id: "test",
//                    photoType: "test",
//                });
//            assert.throws(test);
//        });
//        it("should throw if photoType is not string", () => {
//            const test = () =>
//                makePhotoType({
//                    id: 2,
//                    photoType: 2,
//                });
//            assert.throws(test);
//        });
//        it("should not throw if correct types", () => {
//            const test = () => {
//                makePhotoType({
//                    id: 2,
//                    photoType: "test",
//                });
//                assert.strictEqual(typeof test, "function");
//            };
//            assert.doesNotThrow(test);
//        });
//        describe("returned Object", () => {
//            let photoType;
//            before(() => {
//                photoType = makePhotoType({
//                    id: 2,
//                    photoType: "test",
//                });
//            });
//            it("Should not allow the id to change", () => {
//                photoType.id = 5;
//                assert.strictEqual(photoType.id, 2);
//            });
//            it("Should not allow the photoType to be changed", () => {
//                photoType.photoType = "foo";
//                assert.strictEqual(photoType.photoType, "test");
//            });
//        });
//    });
//});
