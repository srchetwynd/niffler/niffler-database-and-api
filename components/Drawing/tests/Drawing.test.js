//const drawing = require("../Drawing");
//const sinon = require("sinon");
//const assert = require("assert");
//const { db } = require("../../Database");
//
//describe("drawing handler", () => {
//    const dbStub = {};
//
//    afterEach(() => {
//        sinon.restore();
//    });
//
//    it("Should get all drawings for a trench", async () => {
//        dbStub.any = sinon.stub(db, "any");
//        dbStub.any.callsFake(() => Promise.resolve("test"));
//        const req = {
//            company: "test",
//            limit: 4,
//            offset: 0,
//            trenchID: 5,
//        };
//        const result = await drawing.getDrawing(req);
//        assert.strictEqual(result, "test");
//    });
//
//    it("Should get a drawing", async () => {
//        dbStub.oneOrNone = sinon.stub(db, "oneOrNone");
//        dbStub.oneOrNone.callsFake(() => Promise.resolve("test"));
//        const req = {
//            company: "test",
//            limit: 4,
//            offset: 0,
//            id: 5,
//        };
//        const result = await drawing.getDrawing(req);
//        assert.strictEqual(result, "test");
//    });
//
//    it("Should reject if no drawing", async () => {
//        dbStub.oneOrNone = sinon.stub(db, "oneOrNone");
//        dbStub.oneOrNone.callsFake(() => Promise.resolve());
//        const req = {
//            company: "test",
//            limit: 4,
//            offset: 0,
//            id: 5,
//        };
//        const test = () => drawing.getDrawing(req);
//        await assert.rejects(test);
//    });
//});
