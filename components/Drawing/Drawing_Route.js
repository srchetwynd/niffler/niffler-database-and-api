//const pagination = require("../../middleware/pagination");
//const links = require("../../middleware/links");
//const drawing = require("./Drawing");
//
//function convertValues(req, res, next) {
//    req.options = {
//        company: req.company,
//        limit: req.limit,
//        offset: req.offset,
//        id: req.query.id,
//        trenchID: req.params.trenchID,
//    };
//    return next();
//}
//
//function setResource(resource) {
//    return (req, res, next) => {
//        req.resource = resource;
//        return next();
//    };
//}
//
//function setFunction(func) {
//    return (req, res, next) => {
//        req.func = func;
//        return next();
//    };
//}
//
//const allRoutes = [
//    links.buildDataRelationships([]),
//    links.buildDataLinks([]),
//    links.buildResourceLinks([]),
//    setResource("Drawing"),
//    convertValues,
//];
//
//module.exports = {
//    url: "/project/:projectID/site/:siteID/trench/:trenchID/drawing",
//    get: [
//        ...allRoutes,
//        pagination.setLimitOffset,
//        setFunction(drawing.getDrawing),
//    ],
//};
