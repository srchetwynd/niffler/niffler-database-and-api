//const section = require("../Section");
//const sinon = require("sinon");
//const assert = require("assert");
//const { db } = require("../../Database");
//
//describe("section handler", () => {
//    describe("getSection", () => {
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should get a section by id", async () => {
//            sinon
//                .stub(db, "oneOrNone")
//                .callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//                id: 1,
//            };
//            const result = await section.getSection(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if section not found", async () => {
//            sinon.stub(db, "oneOrNone").callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//                id: 1,
//            };
//            const test = () => section.getSection(req);
//            assert.rejects(test);
//        });
//
//        it("Should get all sections in a project", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//                trenchID: 5,
//            };
//            const result = await section.getSection(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if not enough information provided", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offset: 0,
//            };
//            const test = () => section.getSection(req);
//            assert.rejects(test);
//        });
//    });
//});
