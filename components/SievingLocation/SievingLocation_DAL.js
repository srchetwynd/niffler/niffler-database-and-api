//"use strict";
///**
// * @function buildSievingLocationDAL
// * @description builds SievingLocation DAL
// * @param {Object} dependancies
// * @param {Object} dependancies.db
// * @param {Function} dependancies.makeSievingLocation
// * @return {Object}
// */
//function buildSievingLocationDAL({ db, makeSievingLocation } = {}) {
//    if (typeof db !== "object") {
//        throw new Error(`db must be an object got: ${typeof db}`);
//    }
//    if (typeof makeSievingLocation !== "function") {
//        throw new Error(
//            `makeSievingLocation must be a function got: ${typeof makeSievingLocation}`
//        );
//    }
//
//    /**
//     * @function fetchAll
//     * @description returns all SievingLocations
//     * @return {Array}
//     */
//    async function fetchAll() {
//        const results = await db.any(`
//            SELECT
//                *
//            FROM
//                public.sievingLocations
//        `);
//
//        return results.map(makeSievingLocation);
//    }
//
//    return {
//        fetchAll,
//    };
//}
//
//module.exports = buildSievingLocationDAL;
