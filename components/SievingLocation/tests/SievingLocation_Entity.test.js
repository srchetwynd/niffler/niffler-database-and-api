//const buildMakeSievingLocation = require("../SievingLocation_Entity");
//
//const assert = require("assert");
//
//describe("buildMakeSievingLocation", () => {
//    it("Should not throw if the dependancies are right", () => {
//        const test = () => {
//            const result = buildMakeSievingLocation();
//            assert.strictEqual(typeof result, "function");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("makeSievingLocation", () => {
//        let makeSievingLocation;
//        before(() => {
//            makeSievingLocation = buildMakeSievingLocation();
//        });
//        it("should throw if id is not a number", () => {
//            const test = () =>
//                makeSievingLocation({
//                    id: "test",
//                    sievingLocation: "test",
//                });
//            assert.throws(test);
//        });
//        it("should throw if sievingLocation is not string", () => {
//            const test = () =>
//                makeSievingLocation({
//                    id: 2,
//                    sievingLocation: 2,
//                });
//            assert.throws(test);
//        });
//        it("should not throw if correct types", () => {
//            const test = () => {
//                makeSievingLocation({
//                    id: 2,
//                    sievingLocation: "test",
//                });
//                assert.strictEqual(typeof test, "function");
//            };
//            assert.doesNotThrow(test);
//        });
//        describe("returned Object", () => {
//            let sievingLocation;
//            before(() => {
//                sievingLocation = makeSievingLocation({
//                    id: 2,
//                    sievingLocation: "test",
//                });
//            });
//            it("Should not allow the id to change", () => {
//                sievingLocation.id = 5;
//                assert.strictEqual(sievingLocation.id, 2);
//            });
//            it("Should not allow the sievingLocation to be changed", () => {
//                sievingLocation.sievingLocation = "foo";
//                assert.strictEqual(sievingLocation.sievingLocation, "test");
//            });
//        });
//    });
//});
