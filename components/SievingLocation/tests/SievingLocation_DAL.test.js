//const buildSievingLocation = require("../SievingLocation_DAL");
//
//const assert = require("assert");
//
//describe("SievingLocation DOL", () => {
//    it("Should throw if no dependancies are passed", () => {
//        const test = () => buildSievingLocation();
//        assert.throws(test);
//    });
//    it("Should throw if db is not an object", () => {
//        const test = () =>
//            buildSievingLocation({
//                db: "test",
//                makeSievingLocation: () => {},
//            });
//        assert.throws(test);
//    });
//    it("Should throw if makeSievingLocation is not a function", () => {
//        const test = () =>
//            buildSievingLocation({
//                db: {},
//                makeSievingLocation: "test",
//            });
//        assert.throws(test);
//    });
//    it("should not throw if the dependancies are right", () => {
//        const test = () => {
//            const result = buildSievingLocation({
//                db: {},
//                makeSievingLocation: () => ({}),
//            });
//            assert.strictEqual(typeof result, "object");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("SievingLocationDAL", () => {
//        let sievingLocationDAL;
//        before(() => {
//            sievingLocationDAL = buildSievingLocation({
//                db: {
//                    any: () => [],
//                },
//                makeSievingLocation: () => ({}),
//            });
//        });
//        describe("fetchAll", () => {
//            it("should be a function", () => {
//                assert.strictEqual(
//                    typeof sievingLocationDAL.fetchAll,
//                    "function"
//                );
//            });
//            it("should return an array", async () => {
//                assert.deepStrictEqual(await sievingLocationDAL.fetchAll(), []);
//            });
//        });
//    });
//});
