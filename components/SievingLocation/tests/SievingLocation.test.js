//const buildSievingLocation = require("../SievingLocation");
//const assert = require("assert");
//
//describe("buildsievingLocation", () => {
//    it("Should throw an error if fetchAll is not a function", () => {
//        const test = () =>
//            buildSievingLocation({
//                fetchAll: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should not throw if dependancies are correct", () => {
//        const test = () => {
//            const result = buildSievingLocation({
//                fetchAll: () => [],
//            });
//            assert.strictEqual(typeof result, "object");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("returned Object", () => {
//        let sievingLocation;
//        before(() => {
//            sievingLocation = buildSievingLocation({
//                fetchAll: () => [],
//            });
//        });
//        describe("getSievingLocation", () => {
//            it("Should be a function", () => {
//                assert.strictEqual(
//                    typeof sievingLocation.getSievingLocation,
//                    "function"
//                );
//            });
//            it("Should return an array", () => {
//                const result = sievingLocation.getSievingLocation();
//                assert.deepStrictEqual(result, []);
//            });
//        });
//    });
//});
