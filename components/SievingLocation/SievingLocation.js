//"use strict";
//
///**
// * @function bulidSievingLocation
// * @description Builds the sievingLocation functions
// * @param {Object} dependancies
// * @param {Function} dependancies.fetchAll
// * @return {Object}
// */
//function buildSievingLocation({ fetchAll }) {
//    if (typeof fetchAll !== "function") {
//        throw new Error(`fetchAll must be a function got: ${typeof fetchAll}`);
//    }
//    /**
//     * @function getSievingLocation
//     * @description returns all sievingLocations
//     * @return {Array}
//     */
//    function getSievingLocation() {
//        return fetchAll();
//    }
//    return {
//        getSievingLocation,
//    };
//}
//
//module.exports = buildSievingLocation;
