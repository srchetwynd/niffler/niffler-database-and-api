//"use strict";
///**
// * @function buildMakeInterpretation
// * @description creates a function which makes an interpretations
// * @return {Function}
// */
//function buildMakeSievingLocation() {
//    /**
//     * @function makeInterpretation
//     * @description makes an interpretation
//     * @param {Object} sievingLocation
//     * @param {Number} sievingLocation.id
//     * @param {String} sievingLocation.interpretation
//     * @return {Object}
//     */
//    return function makeSievingLocation({ id, sievingLocation }) {
//        if (typeof id !== "number") {
//            throw new Error(`id must be a number got: ${typeof id}`);
//        }
//        if (typeof sievingLocation !== "string") {
//            throw new Error(
//                `sievingLocation must be a string got: ${typeof sievingLocation}`
//            );
//        }
//
//        return Object.freeze({
//            id,
//            sievingLocation,
//        });
//    };
//}
//
//module.exports = buildMakeSievingLocation;
