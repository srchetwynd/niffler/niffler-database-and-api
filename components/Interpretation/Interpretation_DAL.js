//"use strict";
///**
// * @function buildInterpretationDAL
// * @description builds Interpretation DAL
// * @param {Object} dependancies
// * @param {Object} dependancies.db
// * @param {Function} dependancies.makeInterpretation
// * @return {Object}
// */
//function buildInterpretationDAL({ db, makeInterpretation } = {}) {
//    if (typeof db !== "object") {
//        throw new Error(`db must be an object got: ${typeof db}`);
//    }
//    if (typeof makeInterpretation !== "function") {
//        throw new Error(
//            `makeInterpretation must be a function got: ${typeof makeInterpretation}`
//        );
//    }
//
//    /**
//     * @function fetchAll
//     * @description returns all Interpretations
//     * @return {Array}
//     */
//    async function fetchAll() {
//        const results = await db.any(`
//            SELECT
//                *
//            FROM
//                public.interpretations
//        `);
//
//        return results.map(makeInterpretation);
//    }
//
//    return {
//        fetchAll,
//    };
//}
//
//module.exports = buildInterpretationDAL;
