//"use strict";
///**
// * @function bulidInterpretation
// * @description Builds the interpretation functions
// * @param {Object} dependancies
// * @param {Function} dependancies.fetchAll
// * @return {Object}
// */
//function buildInterpretation({ fetchAll }) {
//    if (typeof fetchAll !== "function") {
//        throw new Error(`fetchAll must be a function got: ${typeof fetchAll}`);
//    }
//    /**
//     * @function getInterpretation
//     * @description returns all interpretations
//     * @return {Array}
//     */
//    function getInterpretation() {
//        return fetchAll();
//    }
//    return {
//        getInterpretation,
//    };
//}
//
//module.exports = buildInterpretation;
