//const buildInterpretation = require("../Interpretation_DAL");
//
//const assert = require("assert");
//
//describe("Interpretation DOL", () => {
//    it("Should throw if no dependancies are passed", () => {
//        const test = () => buildInterpretation();
//        assert.throws(test);
//    });
//    it("Should throw if db is not an object", () => {
//        const test = () =>
//            buildInterpretation({
//                db: "test",
//                makeInterpretation: () => {},
//            });
//        assert.throws(test);
//    });
//    it("Should throw if makeInterpretation is not a function", () => {
//        const test = () =>
//            buildInterpretation({
//                db: {},
//                makeInterpretation: "test",
//            });
//        assert.throws(test);
//    });
//    it("should not throw if the dependancies are right", () => {
//        const test = () => {
//            const result = buildInterpretation({
//                db: {},
//                makeInterpretation: () => ({}),
//            });
//            assert.strictEqual(typeof result, "object");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("InterpretationDAL", () => {
//        let interpretationDAL;
//        before(() => {
//            interpretationDAL = buildInterpretation({
//                db: {
//                    any: () => [],
//                },
//                makeInterpretation: () => ({}),
//            });
//        });
//        describe("fetchAll", () => {
//            it("should be a function", () => {
//                assert.strictEqual(
//                    typeof interpretationDAL.fetchAll,
//                    "function"
//                );
//            });
//            it("should return an array", async () => {
//                assert.deepStrictEqual(await interpretationDAL.fetchAll(), []);
//            });
//        });
//    });
//});
