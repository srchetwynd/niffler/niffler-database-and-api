//const buildInterpretation = require("../Interpretation");
//const assert = require("assert");
//
//describe("buildinterpretation", () => {
//    it("Should throw an error if fetchAll is not a function", () => {
//        const test = () =>
//            buildInterpretation({
//                fetchAll: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should not throw if dependancies are correct", () => {
//        const test = () => {
//            const result = buildInterpretation({
//                fetchAll: () => [],
//            });
//            assert.strictEqual(typeof result, "object");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("returned Object", () => {
//        let interpretation;
//        before(() => {
//            interpretation = buildInterpretation({
//                fetchAll: () => [],
//            });
//        });
//        describe("getInterpretation", () => {
//            it("Should be a function", () => {
//                assert.strictEqual(
//                    typeof interpretation.getInterpretation,
//                    "function"
//                );
//            });
//            it("Should return an array", () => {
//                const result = interpretation.getInterpretation();
//                assert.deepStrictEqual(result, []);
//            });
//        });
//    });
//});
