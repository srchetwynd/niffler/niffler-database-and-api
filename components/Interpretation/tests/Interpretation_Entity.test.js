//const buildMakeInterpretation = require("../Interpretation_Entity");
//
//const assert = require("assert");
//
//describe("buildMakeInterpretation", () => {
//    it("Should not throw if the dependancies are right", () => {
//        const test = () => {
//            const result = buildMakeInterpretation();
//            assert.strictEqual(typeof result, "function");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("makeInterpretation", () => {
//        let makeInterpretation;
//        before(() => {
//            makeInterpretation = buildMakeInterpretation();
//        });
//        it("should throw if id is not a number", () => {
//            const test = () =>
//                makeInterpretation({
//                    id: "test",
//                    interpretation: "test",
//                });
//            assert.throws(test);
//        });
//        it("should throw if interpretation is not string", () => {
//            const test = () =>
//                makeInterpretation({
//                    id: 2,
//                    interpretation: 2,
//                });
//            assert.throws(test);
//        });
//        it("should not throw if correct types", () => {
//            const test = () => {
//                makeInterpretation({
//                    id: 2,
//                    interpretation: "test",
//                });
//                assert.strictEqual(typeof test, "function");
//            };
//            assert.doesNotThrow(test);
//        });
//        describe("returned Object", () => {
//            let interpretation;
//            before(() => {
//                interpretation = makeInterpretation({
//                    id: 2,
//                    interpretation: "test",
//                });
//            });
//            it("Should not allow the id to change", () => {
//                interpretation.id = 5;
//                assert.strictEqual(interpretation.id, 2);
//            });
//            it("Should not allow the interpretation to be changed", () => {
//                interpretation.interpretation = "foo";
//                assert.strictEqual(interpretation.interpretation, "test");
//            });
//        });
//    });
//});
