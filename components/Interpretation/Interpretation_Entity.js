//"use strict";
///**
// * @function buildMakeInterpretation
// * @description creates a function which makes an interpretations
// * @return {Function}
// */
//function buildMakeInterpretation() {
//    /**
//     * @function makeInterpretation
//     * @description makes an interpretation
//     * @param {Object} interpretation
//     * @param {Number} interpretation.id
//     * @param {String} interpretation.interpretation
//     * @return {Object}
//     */
//    return function makeInterpretation({ id, interpretation }) {
//        if (typeof id !== "number") {
//            throw new Error(`id must be a number got: ${typeof id}`);
//        }
//        if (typeof interpretation !== "string") {
//            throw new Error(
//                `interpretation must be a string got: ${typeof interpretation}`
//            );
//        }
//
//        return Object.freeze({
//            id,
//            interpretation,
//        });
//    };
//}
//
//module.exports = buildMakeInterpretation;
