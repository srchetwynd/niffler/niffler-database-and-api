//"use strict";
///**
// * @function buildMetalDetectorLocationDAL
// * @description builds MetalDetectorLocation DAL
// * @param {Object} dependancies
// * @param {Object} dependancies.db
// * @param {Function} dependancies.makeMetalDetectorLocation
// * @return {Object}
// */
//function buildMetalDetectorLocationDAL({ db, makeMetalDetectorLocation } = {}) {
//    if (typeof db !== "object") {
//        throw new Error(`db must be an object got: ${typeof db}`);
//    }
//    if (typeof makeMetalDetectorLocation !== "function") {
//        throw new Error(
//            `makeMetalDetectorLocation must be a function got: ${typeof makeMetalDetectorLocation}`
//        );
//    }
//
//    /**
//     * @function fetchAll
//     * @description returns all MetalDetectorLocations
//     * @return {Array}
//     */
//    async function fetchAll() {
//        const results = await db.any(`
//            SELECT
//                *
//            FROM
//                public.metalDetectorLocations
//        `);
//
//        return results.map(makeMetalDetectorLocation);
//    }
//
//    return {
//        fetchAll,
//    };
//}
//
//module.exports = buildMetalDetectorLocationDAL;
