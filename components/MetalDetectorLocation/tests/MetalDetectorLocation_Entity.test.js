//const buildMakeMetalDetectorLocation = require("../MetalDetectorLocation_Entity");
//
//const assert = require("assert");
//
//describe("buildMakeMetalDetectorLocation", () => {
//    it("Should not throw if the dependancies are right", () => {
//        const test = () => {
//            const result = buildMakeMetalDetectorLocation();
//            assert.strictEqual(typeof result, "function");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("makeMetalDetectorLocation", () => {
//        let makeMetalDetectorLocation;
//        before(() => {
//            makeMetalDetectorLocation = buildMakeMetalDetectorLocation();
//        });
//        it("should throw if id is not a number", () => {
//            const test = () =>
//                makeMetalDetectorLocation({
//                    id: "test",
//                    metalDetectorLocation: "test",
//                });
//            assert.throws(test);
//        });
//        it("should throw if metalDetectorLocation is not string", () => {
//            const test = () =>
//                makeMetalDetectorLocation({
//                    id: 2,
//                    metalDetectorLocation: 2,
//                });
//            assert.throws(test);
//        });
//        it("should not throw if correct types", () => {
//            const test = () => {
//                makeMetalDetectorLocation({
//                    id: 2,
//                    metalDetectorLocation: "test",
//                });
//                assert.strictEqual(typeof test, "function");
//            };
//            assert.doesNotThrow(test);
//        });
//        describe("returned Object", () => {
//            let metalDetectorLocation;
//            before(() => {
//                metalDetectorLocation = makeMetalDetectorLocation({
//                    id: 2,
//                    metalDetectorLocation: "test",
//                });
//            });
//            it("Should not allow the id to change", () => {
//                metalDetectorLocation.id = 5;
//                assert.strictEqual(metalDetectorLocation.id, 2);
//            });
//            it("Should not allow the metalDetectorLocation to be changed", () => {
//                metalDetectorLocation.metalDetectorLocation = "foo";
//                assert.strictEqual(
//                    metalDetectorLocation.metalDetectorLocation,
//                    "test"
//                );
//            });
//        });
//    });
//});
