//const buildMetalDetectorLocation = require("../MetalDetectorLocation_DAL");
//
//const assert = require("assert");
//
//describe("MetalDetectorLocation DOL", () => {
//    it("Should throw if no dependancies are passed", () => {
//        const test = () => buildMetalDetectorLocation();
//        assert.throws(test);
//    });
//    it("Should throw if db is not an object", () => {
//        const test = () =>
//            buildMetalDetectorLocation({
//                db: "test",
//                makeMetalDetectorLocation: () => {},
//            });
//        assert.throws(test);
//    });
//    it("Should throw if makeMetalDetectorLocation is not a function", () => {
//        const test = () =>
//            buildMetalDetectorLocation({
//                db: {},
//                makeMetalDetectorLocation: "test",
//            });
//        assert.throws(test);
//    });
//    it("should not throw if the dependancies are right", () => {
//        const test = () => {
//            const result = buildMetalDetectorLocation({
//                db: {},
//                makeMetalDetectorLocation: () => ({}),
//            });
//            assert.strictEqual(typeof result, "object");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("MetalDetectorLocationDAL", () => {
//        let metalDetectorLocationDAL;
//        before(() => {
//            metalDetectorLocationDAL = buildMetalDetectorLocation({
//                db: {
//                    any: () => [],
//                },
//                makeMetalDetectorLocation: () => ({}),
//            });
//        });
//        describe("fetchAll", () => {
//            it("should be a function", () => {
//                assert.strictEqual(
//                    typeof metalDetectorLocationDAL.fetchAll,
//                    "function"
//                );
//            });
//            it("should return an array", async () => {
//                assert.deepStrictEqual(
//                    await metalDetectorLocationDAL.fetchAll(),
//                    []
//                );
//            });
//        });
//    });
//});
