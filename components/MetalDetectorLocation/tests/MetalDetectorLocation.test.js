//const buildMetalDetectorLocation = require("../MetalDetectorLocation");
//const assert = require("assert");
//
//describe("buildmetalDetectorLocation", () => {
//    it("Should throw an error if fetchAll is not a function", () => {
//        const test = () =>
//            buildMetalDetectorLocation({
//                fetchAll: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should not throw if dependancies are correct", () => {
//        const test = () => {
//            const result = buildMetalDetectorLocation({
//                fetchAll: () => [],
//            });
//            assert.strictEqual(typeof result, "object");
//        };
//        assert.doesNotThrow(test);
//    });
//    describe("returned Object", () => {
//        let metalDetectorLocation;
//        before(() => {
//            metalDetectorLocation = buildMetalDetectorLocation({
//                fetchAll: () => [],
//            });
//        });
//        describe("getMetalDetectorLocation", () => {
//            it("Should be a function", () => {
//                assert.strictEqual(
//                    typeof metalDetectorLocation.getMetalDetectorLocation,
//                    "function"
//                );
//            });
//            it("Should return an array", () => {
//                const result = metalDetectorLocation.getMetalDetectorLocation();
//                assert.deepStrictEqual(result, []);
//            });
//        });
//    });
//});
