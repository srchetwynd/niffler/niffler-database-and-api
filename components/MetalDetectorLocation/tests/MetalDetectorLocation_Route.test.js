//const makeRoute = require("../MetalDetectorLocation_Route");
//
//const assert = require("assert");
//
//describe("MetalDetectorLocation Route", () => {
//    it("Should throw if no dependancies passed", () => {
//        const test = () => makeRoute();
//        assert.throws(test);
//    });
//    it("Should throw if router is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: 1,
//                fetchAll: () => [],
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if fetchAll is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                fetchAll: 1,
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if getResponder is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                fetchAll: () => [],
//                getResponder: 1,
//                setResource: () => [],
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if setResource is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                fetchAll: () => [],
//                getResponder: () => [],
//                setResource: 1,
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if setFunction is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                fetchAll: () => [],
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: 1,
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if url is not a string", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                fetchAll: () => [],
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: () => [],
//                url: 1,
//            });
//        assert.throws(test);
//    });
//    it("Should not throw if dependancies are correct", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                fetchAll: () => [],
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.doesNotThrow(test);
//    });
//});
