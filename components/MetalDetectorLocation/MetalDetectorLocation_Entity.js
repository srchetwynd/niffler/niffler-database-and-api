//"use strict";
///**
// * @function buildMakeInterpretation
// * @description creates a function which makes an interpretations
// * @return {Function}
// */
//function buildMakeMetalDetectorLocation() {
//    /**
//     * @function makeInterpretation
//     * @description makes an interpretation
//     * @param {Object} metalDetectorLocation
//     * @param {Number} metalDetectorLocation.id
//     * @param {String} metalDetectorLocation.interpretation
//     * @return {Object}
//     */
//    return function makeMetalDetectorLocation({ id, metalDetectorLocation }) {
//        if (typeof id !== "number") {
//            throw new Error(`id must be a number got: ${typeof id}`);
//        }
//        if (typeof metalDetectorLocation !== "string") {
//            throw new Error(
//                `metalDetectorLocation must be a string got: ${typeof metalDetectorLocation}`
//            );
//        }
//
//        return Object.freeze({
//            id,
//            metalDetectorLocation,
//        });
//    };
//}
//
//module.exports = buildMakeMetalDetectorLocation;
