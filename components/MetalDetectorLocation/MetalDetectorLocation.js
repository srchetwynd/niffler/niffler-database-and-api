//"use strict";
//
///**
// * @function bulidMetalDetectorLocation
// * @description Builds the metalDetectorLocation functions
// * @param {Object} dependancies
// * @param {Function} dependancies.fetchAll
// * @return {Object}
// */
//function buildMetalDetectorLocation({ fetchAll }) {
//    if (typeof fetchAll !== "function") {
//        throw new Error(`fetchAll must be a function got: ${typeof fetchAll}`);
//    }
//    /**
//     * @function getMetalDetectorLocation
//     * @description returns all metalDetectorLocations
//     * @return {Array}
//     */
//    function getMetalDetectorLocation() {
//        return fetchAll();
//    }
//    return {
//        getMetalDetectorLocation,
//    };
//}
//
//module.exports = buildMetalDetectorLocation;
