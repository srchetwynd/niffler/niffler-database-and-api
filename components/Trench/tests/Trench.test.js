//const trench = require("../Trench");
//const sinon = require("sinon");
//const assert = require("assert");
//const { db } = require("../../Database");
//
//describe("trench handler", () => {
//    describe("getTrench", () => {
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should get a trench by id", async () => {
//            sinon
//                .stub(db, "oneOrNone")
//                .callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 2,
//                offset: 0,
//                id: 2,
//            };
//            const result = await trench.getTrench(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if trench is not found", async () => {
//            sinon.stub(db, "oneOrNone").callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 2,
//                offset: 0,
//                id: 2,
//            };
//            const test = () => trench.getTrench(req);
//            await assert.rejects(test);
//        });
//
//        it("Should get trenchs by site", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 2,
//                offset: 0,
//                siteID: 3,
//            };
//            const result = await trench.getTrench(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("Should reject if not enough information", async () => {
//            sinon.stub(db, "any").callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 2,
//                offset: 0,
//            };
//            const test = () => trench.getTrench(req);
//            await assert.rejects(test);
//        });
//    });
//    describe.skip("putTrench", () => {});
//    describe.skip("postTrench", () => {});
//    describe("deleteTrench", () => {
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should delete a trench", async () => {
//            sinon
//                .stub(db, "result")
//                .callsFake(() => Promise.resolve({ rowCount: 1 }));
//            const req = {
//                company: "test",
//                id: 1,
//            };
//            const test = () => trench.deleteTrench(req);
//            assert.doesNotReject(test);
//        });
//
//        it("Should reject if no trench exists", async () => {
//            sinon
//                .stub(db, "result")
//                .callsFake(() => Promise.resolve({ rowCount: 0 }));
//            const req = {
//                company: "test",
//                id: 1,
//            };
//            const test = () => trench.deleteTrench(req);
//            assert.rejects(test);
//        });
//    });
//});
