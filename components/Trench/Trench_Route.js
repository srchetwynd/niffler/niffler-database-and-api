//var express = require("express");
//var router = express.Router();
//
//const exp = require("../Express");
//const pagination = require("../../middleware/pagination");
//const trench = require("./Trench");
//const links = require("../../middleware/links");
//const urls = require("../../helpers/urls");
//
//router.use(
//    urls.trench.url,
//    links.buildDataLinks([]),
//    links.buildResourceLinks([]),
//    links.buildDataRelationships([
//        "coffin",
//        "context",
//        "cut",
//        "drawing",
//        "deposit",
//        "gis",
//        "masonry",
//        "point",
//        "polygon",
//        "polyline",
//        "photo",
//        "project",
//        "site",
//        "section",
//        "skeleton",
//        "timber",
//    ])
//);
//
//router.get(
//    urls.trench.url,
//    pagination.setLimitOffset,
//    (req, res, next) => {
//        exp.helpers.addFunction(
//            trench.getTrench,
//            {
//                company: req.company,
//                limit: req.limit,
//                offset: req.offset,
//                id: req.query.id,
//                siteID: req.params.siteID,
//            },
//            req
//        );
//        return next();
//    },
//    exp.getResponder
//);
//
//router.put(
//    urls.trench.url,
//    (req, res, next) => {
//        exp.helpers.addFucntion(
//            trench.putTrench,
//            {
//                company: req.company,
//                id: req.query.id,
//                siteID: req.params.siteID,
//                projectID: req.params.projectID,
//                ...req.body,
//            },
//            req
//        );
//        return next();
//    },
//    exp.putResponder
//);
//
//router.post(
//    urls.trench.url,
//    (req, res, next) => {
//        exp.helpers.addFucntion(
//            trench.postTrench,
//            {
//                company: req.company,
//                number: req.body.number,
//                site: req.params.siteID,
//                project: req.params.projectID,
//            },
//            req
//        );
//        return next();
//    },
//    exp.postResponder
//);
//
//router.delete(
//    urls.trench.url,
//    (req, res, next) => {
//        exp.helpers.addFunction(
//            trench.deleteTrench,
//            {
//                company: req.company,
//                id: req.query.id,
//            },
//            req
//        );
//        return next();
//    },
//    exp.deleteResponder
//);
//
//module.exports = router;
