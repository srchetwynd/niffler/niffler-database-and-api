//const Trench = require("./Trench_DAO");
//
//const createError = require("http-errors");
//
//async function getTrench(options) {
//    const trench = new Trench()
//        .setSchema(options.company)
//        .setLimit(options.limit)
//        .setOffset(options.offset);
//    if (options.id) {
//        const data = await trench.setColumns("id", options.id).fetchOne();
//        if (!data) {
//            throw createError(404, "Trench not found");
//        }
//        return data;
//    } else if (options.siteID) {
//        return trench.setColumns("site", options.siteID).fetchAll();
//    } else {
//        throw createError(
//            400,
//            "Not enough information provided to show trenchs"
//        );
//    }
//}
//
//function putTrench(options) {
//    return new Trench()
//        .setSchema(options.company)
//        .setColumns({
//            id: options.id,
//            site: options.siteID,
//            project: options.projectID,
//            ...options.body,
//        })
//        .update();
//}
//
//function postTrench(options) {
//    return new Trench()
//        .setSchema(options.company)
//        .setColumns({
//            number: options.body.number,
//            site: options.siteID,
//            project: options.projectID,
//        })
//        .insert();
//}
//
//async function deleteTrench(options) {
//    const result = await new Trench()
//        .setSchema(options.company)
//        .setColumns("id", options.id)
//        .delete();
//    if (result.rowCount === 0) {
//        throw createError(404, "Trench not found");
//    }
//    return;
//}
//
//module.exports = {
//    getTrench,
//    postTrench,
//    putTrench,
//    deleteTrench,
//};
