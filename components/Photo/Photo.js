//const Photo = require("./Photo_DAO");
//const space = require("../../helpers/space");
//
//const createError = require("http-errors");
//const fs = require("fs");
//
//async function getPhoto(options) {
//    let photo = new Photo()
//        .setSchema(options.company)
//        .setLimit(options.limit)
//        .setOffset(options.offset);
//    if (options.id) {
//        let data = await photo.setColumns("id", options.id).fetchOne();
//        if (!data) {
//            throw createError(404, "Photo not found");
//        }
//        if (options.get === "image") {
//            return space.getDownloadLink(data.storage_id);
//        }
//        return data;
//    } else if (options.context) {
//        return photo.setColumns("context", options.context).fetchAll();
//    } else if (options.trenchID) {
//        return photo.setColumns("trench", options.trenchID).fetchAll();
//    } else {
//        throw createError(400, "Not enough data to get photos");
//    }
//}
//
//async function postPhoto(options) {
//    const photo = new Photo().setSchema(options.company).setColumns({
//        project: options.projectID,
//        site: options.siteID,
//        trench: options.trenchID,
//        ...options.body,
//    });
//    const result = photo.insert();
//    await space.upload({
//        content: fs.createReadStream(options.photoFile),
//        name: photo.storage_id,
//    });
//
//    return result;
//}
//
//async function deletePhoto(options) {
//    const result = await new Photo()
//        .setSchema(options.company)
//        .setColumns("id", options.id)
//        .delete();
//    if (result.rowCount === 0) {
//        throw createError(404, "Photo not found");
//    }
//    return;
//}
//
//module.exports = {
//    deletePhoto,
//    postPhoto,
//    getPhoto,
//};
