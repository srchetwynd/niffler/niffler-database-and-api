//const photo = require("../Photo");
//const sinon = require("sinon");
//const assert = require("assert");
//const { db } = require("../../Database");
//const space = require("../../../helpers/space");
//const fs = require("fs");
//
//describe("photo handler", () => {
//    describe("getPhoto", () => {
//        const dbStub = {};
//
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("should reject if not enough info", () => {
//            const req = {
//                company: "test",
//                limit: 10,
//                offsest: 0,
//            };
//            const test = () => photo.getPhoto(req);
//            assert.rejects(test);
//        });
//
//        it("should reject if no photo found", async () => {
//            dbStub.oneOrNone = sinon.stub(db, "oneOrNone");
//            dbStub.oneOrNone.callsFake(() => Promise.resolve());
//            const req = {
//                company: "test",
//                limit: 10,
//                offsest: 0,
//                id: 5,
//            };
//            const test = () => photo.getPhoto(req);
//            await assert.rejects(test);
//        });
//
//        it("should return a photo", async () => {
//            dbStub.oneOrNone = sinon.stub(db, "oneOrNone");
//            dbStub.oneOrNone.callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offsest: 0,
//                id: 5,
//            };
//            const result = await photo.getPhoto(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("should return a download link", async () => {
//            dbStub.oneOrNone = sinon.stub(db, "oneOrNone");
//            dbStub.oneOrNone.callsFake(() =>
//                Promise.resolve({ storage_id: 4 })
//            );
//            sinon
//                .stub(space, "getDownloadLink")
//                .callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offsest: 0,
//                id: 5,
//                get: "image",
//            };
//            const result = await photo.getPhoto(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it.skip("should return all photos of a context", async () => {
//            dbStub.any = sinon.stub(db, "any");
//            dbStub.any.callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offsest: 0,
//                context: 5,
//            };
//            const result = await photo.getPhoto(req);
//            assert.strictEqual(result, "test");
//        });
//
//        it("should return all photos of a trench", async () => {
//            dbStub.any = sinon.stub(db, "any");
//            dbStub.any.callsFake(() => Promise.resolve("test"));
//            const req = {
//                company: "test",
//                limit: 10,
//                offsest: 0,
//                trenchID: 5,
//            };
//            const result = await photo.getPhoto(req);
//            assert.strictEqual(result, "test");
//        });
//    });
//    describe("postPhoto", () => {
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("Should insert a new photo", async () => {
//            const dbAny = sinon
//                .stub(db, "any")
//                .callsFake(() => Promise.resolve("test"));
//            const spaceUpload = sinon
//                .stub(space, "upload")
//                .callsFake(() => Promise.resolve());
//            sinon.stub(fs, "createReadStream");
//            const req = {
//                company: "test",
//                projectID: 5,
//                siteID: 4,
//                trenchID: 5,
//                roll: 4,
//                photoFile: "test",
//            };
//            const result = await photo.postPhoto(req);
//            assert.strictEqual(result, "test");
//            assert.strictEqual(dbAny.calledOnce, true);
//            assert.strictEqual(spaceUpload.calledOnce, true);
//        });
//    });
//    describe("deletePhoto", () => {
//        const dbStub = {};
//
//        afterEach(() => {
//            sinon.restore();
//        });
//
//        it("should delete a photo", () => {
//            dbStub.result = sinon.stub(db, "result");
//            dbStub.result.callsFake(() => Promise.resolve({ rowCount: 1 }));
//            const req = {
//                company: "test",
//                id: 5,
//            };
//            const test = () => photo.deletePhoto(req);
//            assert.doesNotReject(test);
//        });
//
//        it("should reject on no photo", () => {
//            dbStub.result = sinon.stub(db, "result");
//            dbStub.result.callsFake(() => Promise.resolve({ rowCount: 0 }));
//            const req = {
//                company: "test",
//                id: 5,
//            };
//            const test = () => photo.deletePhoto(req);
//            assert.rejects(test);
//        });
//    });
//});
