//const express = require("express");
//const router = express.Router();
//
//const exp = require("../Express");
//const photo = require("./Photo");
//const pagination = require("../../middleware/pagination");
//const links = require("../../middleware/links");
//const urls = require("../../helpers/urls");
//
//router.use(
//    urls.photo.url,
//    links.buildDataRelationships(["project", "site", "trench", "context"]),
//    links.buildDataLinks(["photoTypes", "orientation"]),
//    links.buildResourceLinks(["photoTypes", "orientation"])
//);
//
//router.get(
//    urls.photo.url,
//    pagination.setLimitOffset,
//    (req, res, next) => {
//        exp.helpers.addFunction(
//            photo.getPhoto,
//            {
//                company: req.company,
//                limit: req.limit,
//                offset: req.offset,
//                id: req.query.id,
//                context: req.query.context,
//                trenchID: req.params.trenchID,
//                get: req.query.get,
//            },
//            req
//        );
//        return next();
//    },
//    exp.getResponder
//);
//
//router.post(
//    urls.photo.url,
//    (req, res, next) => {
//        exp.helpers.addFunction(
//            photo.postPhoto,
//            {
//                company: req.company,
//                project: req.params.projectID,
//                site: req.params.siteID,
//                trench: req.params.trenchID,
//                ...req.body,
//            },
//            req
//        );
//        return next();
//    },
//    exp.postResponder
//);
//
//router.delete(
//    urls.photo.url,
//    (req, res, next) => {
//        exp.helpers.addFunction(
//            photo.deletePhoto,
//            {
//                company: req.company,
//                id: req.query.id,
//            },
//            req
//        );
//        return next();
//    },
//    exp.deleteResponder
//);
//
//module.exports = router;
