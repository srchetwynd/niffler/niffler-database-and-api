"use strict";

/** @module helpers */

/**
 * @function expectedType
 * @description validates if a type is correct
 * @throws TypeError if incorrect type
 * @param {string|Array<string>} type the accepted type/s
 * @param {*} value the value to be tested
 * @param {string} name the name of the variable tested
 */
function expectType(type, value, name) {
    if (typeof type === "string") {
        type = [type];
    }
    const result = type.reduce((acc, el) => {
        if (el === "array") {
            return acc || Array.isArray(value);
        } else {
            return acc || typeof value === el;
        }
    }, false);
    if (!result) {
        throw new TypeError(
            `Expected ${name} to be ${type.join(" or ")}. Got: ${typeof value}.`
        );
    }
}

module.exports = {
    expectType,
};
