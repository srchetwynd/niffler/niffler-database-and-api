//"use strict";
//
//const helpers = require("./Helpers");
///**
// * @function makeRoute
// * @description Adds the aboveBelow route to the router
// * @param {Object} dependancies
// * @param {Function} dependancies.router
// * @param {Function} dependancies.getAboveBelow
// * @param {Function} dependancies.getResponder
// * @param {Function} dependancies.setResource
// * @param {Function} dependancies.setFunction
// * @param {String} dependancies.url
// */
//function makeRoute({
//    router,
//    getAboveBelow,
//    getResponder,
//    setResource,
//    setFunction,
//    url,
//} = {}) {
//    if (typeof router !== "object") {
//        throw new Error(`router must be a object got: ${typeof router}`);
//    }
//    if (typeof getAboveBelow !== "function") {
//        throw new Error(
//            `getAboveBelow must be a function got: ${typeof getAboveBelow}`
//        );
//    }
//    if (typeof getResponder !== "function") {
//        throw new Error(
//            `getResponder must be a function got: ${typeof getResponder}`
//        );
//    }
//    if (typeof setResource !== "function") {
//        throw new Error(
//            `setResource must be a function got: ${typeof setResource}`
//        );
//    }
//    if (typeof setFunction !== "function") {
//        throw new Error(
//            `setFunction must be a function got: ${typeof setFunction}`
//        );
//    }
//    if (typeof url !== "string") {
//        throw new Error(`url must be a string got: ${typeof url}`);
//    }
//
//    router.get(
//        url,
//        setResource("AboveBelow"),
//        helpers.convertValues,
//        setFunction(getAboveBelow),
//        getResponder
//    );
//
//    return router;
//}
//
//module.exports = makeRoute;
