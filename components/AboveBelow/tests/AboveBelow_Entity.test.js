//const buildMakeAboveBelow = require("../AboveBelow_Entity");
//
//const assert = require("assert");
//
//describe("AboveBelow Entity", () => {
//    it("Should not throw with correct dependancies", () => {
//        const test = () => buildMakeAboveBelow();
//        assert.doesNotThrow(test);
//    });
//    describe("makeAboveBelow", () => {
//        let makeAboveBelow;
//        before(() => {
//            makeAboveBelow = buildMakeAboveBelow();
//        });
//        it("Should throw if above is not a number", () => {
//            const test = () =>
//                makeAboveBelow({
//                    above: "test",
//                    below: 2,
//                });
//            assert.throws(test);
//        });
//        it("should throw is below is not a number", () => {
//            const test = () =>
//                makeAboveBelow({
//                    above: 1,
//                    below: "test",
//                });
//            assert.throws(test);
//        });
//        it("should throw is above and below are the same", () => {
//            const test = () =>
//                makeAboveBelow({
//                    above: 1,
//                    below: 1,
//                });
//            assert.throws(test);
//        });
//        it("should not throw is args are correct", () => {
//            const test = () =>
//                makeAboveBelow({
//                    above: 1,
//                    below: 2,
//                });
//            assert.doesNotThrow(test);
//        });
//        it("should return an object", () => {
//            const result = makeAboveBelow({
//                above: 1,
//                below: 2,
//            });
//            assert.strictEqual(typeof result, "object");
//        });
//        it("should not allow the above to be changed", () => {
//            const result = makeAboveBelow({
//                above: 1,
//                below: 2,
//            });
//            assert.strictEqual(result.above, 1);
//        });
//        it("should not allow the below to be changed", () => {
//            const result = makeAboveBelow({
//                above: 1,
//                below: 2,
//            });
//            result.below = 5;
//            assert.strictEqual(result.below, 2);
//        });
//    });
//});
