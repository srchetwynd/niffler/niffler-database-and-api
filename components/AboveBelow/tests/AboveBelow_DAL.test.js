//const buildAboveBelow = require("../AboveBelow_DAL");
//
//const assert = require("assert");
//
//describe("AboveBelow_DAL", () => {
//    describe("buildAboveBelowDAL", () => {
//        it("Should throw if no dependancies passed", () => {
//            const test = () => buildAboveBelow();
//            assert.throws(test);
//        });
//        it("Should throw if db is not an object", () => {
//            const test = () =>
//                buildAboveBelow({
//                    db: "test",
//                    makeAboveBelow: () => ({}),
//                });
//            assert.throws(test);
//        });
//        it("Should throw if makeAboveBelow is not a function", () => {
//            const test = () =>
//                buildAboveBelow({
//                    db: {},
//                    makeAboveBelow: "test",
//                });
//            assert.throws(test);
//        });
//        it("Should not throw if dependancies are correct", () => {
//            const test = () => {
//                const result = buildAboveBelow({
//                    db: {},
//                    makeAboveBelow: () => ({}),
//                });
//                assert.strictEqual(typeof result, "object");
//                assert.strictEqual(typeof result.getByTrench, "function");
//            };
//            assert.doesNotThrow(test);
//        });
//        describe("Returned object", () => {
//            let obj;
//            before(() => {
//                obj = buildAboveBelow({
//                    db: {
//                        any: () => ["test"],
//                    },
//                    makeAboveBelow: (test) => test,
//                });
//            });
//            it("Should have a getByTrench function", () => {
//                assert.strictEqual(typeof obj.getByTrench, "function");
//            });
//            describe("getbyTrench", () => {
//                it("Should reject if no dependancies", async () => {
//                    const test = () => obj.getByTrench();
//                    await assert.rejects(test);
//                });
//                it("Should reject if company is not a string", async () => {
//                    const test = () =>
//                        obj.getByTrench({
//                            company: 1,
//                            limit: 1,
//                            offset: 1,
//                            trench: 1,
//                        });
//                    await assert.rejects(test);
//                });
//                it("Should reject if limit is not a number", async () => {
//                    const test = () =>
//                        obj.getByTrench({
//                            company: "test",
//                            limit: "1",
//                            offset: 1,
//                            trench: 1,
//                        });
//                    await assert.rejects(test);
//                });
//                it("Should reject if offset is not a number", async () => {
//                    const test = () =>
//                        obj.getByTrench({
//                            company: "test",
//                            limit: 1,
//                            offset: "1",
//                            trench: 1,
//                        });
//                    await assert.rejects(test);
//                });
//                it("Should reject if trench is not a number", async () => {
//                    const test = () =>
//                        obj.getByTrench({
//                            company: "test",
//                            limit: 1,
//                            offset: 1,
//                            trench: "1",
//                        });
//                    await assert.rejects(test);
//                });
//                it("Should not reject", async () => {
//                    const test = () =>
//                        obj.getByTrench({
//                            company: "test",
//                            limit: 1,
//                            offset: 1,
//                            trench: 1,
//                        });
//                    await assert.doesNotReject(test);
//                });
//            });
//        });
//    });
//});
