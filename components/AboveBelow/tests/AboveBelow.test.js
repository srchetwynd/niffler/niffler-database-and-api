//const aboveBelow = require("../AboveBelow");
//const assert = require("assert");
//
//describe("aboveBelow", () => {
//    describe("buildAboveBelow", () => {
//        it("Should throw if no dependancies", () => {
//            const test = () => aboveBelow();
//            assert.throws(test);
//        });
//        it("Should throw if getByTrench is not a function", () => {
//            const test = () =>
//                aboveBelow({
//                    getByTrench: "test",
//                });
//            assert.throws(test);
//        });
//        it("Should not throw if getByTrench is a function", () => {
//            const test = () => {
//                const result = aboveBelow({
//                    getByTrench: () => ({}),
//                });
//                assert.strictEqual(typeof result, "object");
//                assert.strictEqual(typeof result.getAboveBelow, "function");
//            };
//            assert.doesNotThrow(test);
//        });
//        describe("returned object", () => {
//            let obj;
//            before(() => {
//                obj = aboveBelow({
//                    getByTrench: () => ({}),
//                });
//            });
//            it("should have a function getAboveBelow", () => {
//                assert.strictEqual(typeof obj.getAboveBelow, "function");
//            });
//            describe("getAboveBelow", () => {
//                it("Should throw if company is not a string", async () => {
//                    const test = () =>
//                        obj.getAboveBelow({
//                            company: 1,
//                            limit: 1,
//                            offset: 1,
//                            trench: 1,
//                        });
//                    await assert.throws(test);
//                });
//                it("Should throw if limit is not a number", async () => {
//                    const test = () =>
//                        obj.getAboveBelow({
//                            company: "test",
//                            limit: "1",
//                            offset: 1,
//                            trench: 1,
//                        });
//                    await assert.throws(test);
//                });
//                it("Should throw if offset is not a number", async () => {
//                    const test = () =>
//                        obj.getAboveBelow({
//                            company: "test",
//                            limit: 1,
//                            offset: "1",
//                            trench: 1,
//                        });
//                    await assert.throws(test);
//                });
//                it("Should throw if trench is not a number", async () => {
//                    const test = () =>
//                        obj.getAboveBelow({
//                            company: "test",
//                            limit: 1,
//                            offset: 1,
//                            trench: "1",
//                        });
//                    await assert.throws(test);
//                });
//                it("Should not throw", async () => {
//                    const test = () =>
//                        obj.getAboveBelow({
//                            company: "test",
//                            limit: 1,
//                            offset: 1,
//                            trench: 1,
//                        });
//                    await assert.doesNotThrow(test);
//                });
//            });
//        });
//    });
//});
