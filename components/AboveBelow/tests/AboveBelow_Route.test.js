//const makeRoute = require("../AboveBelow_Route");
//
//const assert = require("assert");
//
//describe("AboveBelow Route", () => {
//    it("Should throw if no dependancies", () => {
//        const test = () => makeRoute();
//        assert.throws(test);
//    });
//    it("Should throw if router is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: 1,
//                getAboveBelow: () => [],
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if getAboveBelow is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                getAboveBelow: 1,
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if getResponder is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                getAboveBelow: () => [],
//                getResponder: 1,
//                setResource: () => [],
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if setResource is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                getAboveBelow: () => [],
//                getResponder: () => [],
//                setResource: 1,
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if setFunction is not a function", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                getAboveBelow: () => [],
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: 1,
//                url: "test",
//            });
//        assert.throws(test);
//    });
//    it("Should throw if url is not a string", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                getAboveBelow: () => [],
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: () => [],
//                url: 1,
//            });
//        assert.throws(test);
//    });
//    it("Should not throw if dependancies are correct", () => {
//        const test = () =>
//            makeRoute({
//                router: {
//                    get: () => [],
//                },
//                getAboveBelow: () => [],
//                getResponder: () => [],
//                setResource: () => [],
//                setFunction: () => [],
//                url: "test",
//            });
//        assert.doesNotThrow(test);
//    });
//});
