//"use strict";
///**
// * @function buildAboveBelowDAL
// * @description builds the Above Below Data access layer
// * @param {Object} dependancies
// * @param {Object} dependancies.db
// * @param {Function} dependancies.makeAboveBelow
// * @returns {Object}
// */
//function buildAboveBelowDAL({ db, makeAboveBelow } = {}) {
//    if (typeof db !== "object") {
//        throw new Error(`db must be an object got: ${typeof db}`);
//    }
//    if (typeof makeAboveBelow !== "function") {
//        throw new Error(
//            `makeAboveBelow must be a function got: ${typeof makeAboveBelow}`
//        );
//    }
//
//    /**
//     * @function getByTrench
//     * @description Gets aboves belows for a trench
//     * @param {Object} dependancies
//     * @param {Number} dependancies.trench
//     * @param {Number} dependancies.limit
//     * @param {Number} dependancies.offset
//     * @param {String} dependancies.company
//     * @returns {Object}
//     */
//    async function getByTrench({ trench, limit, offset, company } = {}) {
//        if (typeof trench !== "number") {
//            throw new Error(`trench must be a number got: ${typeof trench}`);
//        }
//        if (typeof limit !== "number") {
//            throw new Error(`limit must be a number got: ${typeof limit}`);
//        }
//        if (typeof offset !== "number") {
//            throw new Error(`offset must be a number got: ${typeof offset}`);
//        }
//        if (typeof company !== "string") {
//            throw new Error(`company must be a string got: ${typeof company}`);
//        }
//        const results = await db.any(
//            `
//            SELECT
//                cab.above,
//                cab.below,
//                ca.number,
//                cb.number
//            FROM
//                $4~.context_above_below as cab
//            INNER JOIN
//                $4~.contexts
//            ON
//                cab.above = contexts.id
//            AS
//                ca
//            INNER JOIN
//                $4~.contexts
//            ON
//                cab.below = contexts.id
//            AS
//                cb
//            WHERE
//                ca = $1
//            LIMIT
//                $2
//            OFFSET
//                $3
//        `,
//            [trench, limit, offset, company]
//        );
//
//        return results.map((el) =>
//            makeAboveBelow({ above: el.above, below: el.below })
//        );
//    }
//
//    return {
//        getByTrench,
//    };
//}
//
//module.exports = buildAboveBelowDAL;
