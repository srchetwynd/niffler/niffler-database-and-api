//"use strict";
///**
// * @function buildAboveBelow
// * @description Builds the above below functions
// * @param {Object} dependancies
// * @param {Function} dependancies.getByTrench
// * @returns {Object}
// */
//function buildAboveBelow({ getByTrench } = {}) {
//    if (typeof getByTrench !== "function") {
//        throw new Error(
//            `getByTrench must be a function got: ${typeof getByTrench}`
//        );
//    }
//    /**
//     * @function getAboveBelow
//     * @description returns all above belows for a trench
//     * @param {Object} options
//     * @param {String} options.company
//     * @param {Number} options.trench
//     * @param {Number} options.limit
//     * @param {Number} options.offset
//     * @return {Promise<Array>}
//     */
//    function getAboveBelow(options) {
//        if (typeof options.company !== "string") {
//            throw new Error(
//                `options.company must be a string got: ${typeof options.company}`
//            );
//        }
//        if (typeof options.limit !== "number") {
//            throw new Error(
//                `options.limit must be a number got: ${typeof options.limit}`
//            );
//        }
//        if (typeof options.offset !== "number") {
//            throw new Error(
//                `options.offset must be a number got: ${typeof options.offset}`
//            );
//        }
//        if (typeof options.trench !== "number") {
//            throw new Error(
//                `options.trench must be a number got: ${typeof options.trench}`
//            );
//        }
//        return getByTrench({
//            company: options.company,
//            limit: options.limit,
//            offset: options.offset,
//            trench: options.trench,
//        });
//    }
//
//    return {
//        getAboveBelow,
//    };
//}
//
//module.exports = buildAboveBelow;
