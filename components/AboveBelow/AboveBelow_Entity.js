//"use strict";
///**
// * @function buildMakeAboveBelow
// * @description creates a function which makes abovebelows
// * @return {Function}
// */
//function buildMakeAboveBelow() {
//    /**
//     * @function makeAboveBelow
//     * @description Makes an above below
//     * @param {Object} aboveBelow
//     * @param {Number} aboveBelow.above
//     * @param {Number} aboveBelow.below
//     * @return {Object}
//     */
//    return function makeAboveBelow({ above, below }) {
//        if (typeof above !== "number") {
//            throw new Error(`Above must be a number got: ${typeof above}`);
//        }
//        if (typeof below !== "number") {
//            throw new Error(`Below must be a number got: ${typeof below}`);
//        }
//        if (above === below) {
//            throw new Error("Above an below cannot be the same");
//        }
//
//        return Object.freeze({
//            above,
//            below,
//        });
//    };
//}
//
//module.exports = buildMakeAboveBelow;
