///**
// * Takes the request object and extracts the limit and offset, if none are preset the offset defaults to 0 and limit to
// * 10
// *
// * @param req
// * @param res
// * @param next
// */
//function setLimitOffset(req, res, next) {
//    if (!req.query.offset) {
//        req.offset = 0;
//    } else {
//        req.offset = req.query.offset;
//    }
//    if (!req.query.limit) {
//        req.limit = 10;
//    } else {
//        req.limit = req.query.limit;
//    }
//    return next();
//}
//
//module.exports = {
//    setLimitOffset,
//};
