/*
const { LinkObject, setMethods } = require("../components/JsonApi");
const urls = require("../helpers/urls");

const validLinks = [
    "interpretations",
    "photoTypes",
    "sievingLocations",
    "unit",
    "orientation",
    "metalDetectorLocations",
    "degreeOfContamination",
];

const validRelationships = [
    "project",
    "site",
    "trench",
    "photo",
    "drawing",
    "section",
    "gis",
    "point",
    "polyline",
    "polygon",
    "coffin",
    "cut",
    "deposit",
    "masonry",
    "skeleton",
    "timber",
    "context",
    "aboveBelow",
    "sameAs",
    "user",
    "context_detail",
    "aboveBelow",
];

const typed = [
    "point",
    "polyline",
    "polygone",
    "coffin",
    "cut",
    "deposit",
    "masonry",
    "skeleton",
    "timber",
];

function buildLinks(type) {
    if (!["dataLinks", "resourceLinks"].includes(type)) {
        throw new Error("Invalid link type");
    }
    return (links) => {
        links.forEach((el) => {
            if (!validLinks.includes(el)) {
                throw new Error(`Invalid Link: ${el}`);
            }
        });
        return (req, res, next) => {
            req[type] = {};
            req[type].self = setMethods(new LinkObject())
                .setHref(req.fullURL)
                .build();
            req[type] = links.reduce((acc, el) => {
                acc[el] = setMethods(new LinkObject())
                    .setHref(urls[el].buildUrl())
                    .build();
                return acc;
            }, req[type]);
            return next();
        };
    };
}

function buildRelationships(links) {
    links.forEach((el) => {
        if (!validRelationships.includes(el)) {
            throw new Error(`Invalid Link: ${el}`);
        }
    });
    return (req, res, next) => {
        req.dataRelationships = {};
        links.forEach((el) => {
            const options = { params: req.params };
            if (typed.includes(el)) {
                options.query = { type: el };
            }
            req.dataRelationships[el] = (inOptions) => {
                inOptions = {
                    params: {
                        ...inOptions.params,
                        ...options.params,
                    },
                };
                return setMethods(new LinkObject())
                    .setHref(urls[el].buildUrl(inOptions))
                    .build();
            };
        });
        return next();
    };
}

module.exports = {
    buildDataLinks: buildLinks("dataLinks"),
    buildDataRelationships: buildRelationships,
    buildResourceLinks: buildLinks("resourceLinks"),
};
*/
