//const jwt = require("jsonwebtoken");
//const createError = require("http-errors");
//
//const config = require("../config");
//
///**
// * @param req
// * @param res
// * @param next
// */
//function authenticate(req, res, next) {
//    if (!req.headers.authorization) {
//        return next(createError(403, "User Unauthorised"));
//    }
//
//    let token = req.headers.authorization.split(" ")[1];
//
//    try {
//        token = jwt.verify(token, config.secret);
//        req.company = token.data.company;
//        req.userID = token.data.userid;
//    } catch (err) {
//        return next(createError(403, err.message));
//    }
//
//    return next();
//}
//
//module.exports = {
//    authenticate,
//};
