////const links = require("../../middleware/links");
////const assert = require("assert");
////
////describe("Links Middleware", () => {
////    describe("buildDataLinks", () => {
////        it("Should be a function", () => {
////            assert.strictEqual(typeof links.buildDataLinks, "function");
////        });
////        it("Should error on invalid argument", () => {
////            const test = () => links.buildDataLinks("InValid");
////            assert.throws(test);
////        });
////        it("Should error on invalid link type", () => {
////            const test = () => links.buildDataLinks(["inValid"]);
////            assert.throws(test);
////        });
////        it("Should error an invalid link type amounst valid ones", () => {
////            const test = () =>
////                links.buildDataLinks(["unit", "invalid", "sievingLocations"]);
////            assert.throws(test);
////        });
////        it("Should return a function on success", () => {
////            assert.strictEqual(
////                typeof links.buildDataLinks(["unit"]),
////                "function"
////            );
////        });
////        describe("returned function", () => {
////            it("Should pass nothing to next", () => {
////                const middleware = links.buildDataLinks([
////                    "unit",
////                    "sievingLocations",
////                ]);
////                const next = (result) => {
////                    assert.strictEqual(
////                        result,
////                        undefined,
////                        "Unexpected value passed to next"
////                    );
////                    assert.strictEqual(
////                        typeof req.dataLinks,
////                        "object",
////                        "Unexpected value in req.dataLinks"
////                    );
////                    assert.strictEqual(
////                        typeof req.dataLinks.unit,
////                        "object",
////                        "Unexpected value in req.datalinks.unit"
////                    );
////                    assert.strictEqual(
////                        typeof req.dataLinks.sievingLocations,
////                        "object",
////                        "Unexpected value in req.datalinks.sievingLocations"
////                    );
////                };
////                const req = {
////                    fullURL: "test.test",
////                };
////                middleware(req, null, next);
////            });
////        });
////    });
////    describe("buildResourceLinks", () => {
////        it("Should be a function", () => {
////            assert.strictEqual(typeof links.buildResourceLinks, "function");
////        });
////        it("Should error on invalid argument", () => {
////            const test = () => links.buildResourceLinks("InValid");
////            assert.throws(test);
////        });
////        it("Should error on invalid link type", () => {
////            const test = () => links.buildResourceLinks(["inValid"]);
////            assert.throws(test);
////        });
////        it("Should error an invalid link type amounst valid ones", () => {
////            const test = () =>
////                links.buildResourceLinks([
////                    "unit",
////                    "invalid",
////                    "sievingLocations",
////                ]);
////            assert.throws(test);
////        });
////        it("Should return a function on success", () => {
////            assert.strictEqual(
////                typeof links.buildResourceLinks(["unit"]),
////                "function"
////            );
////        });
////        describe("returned function", () => {
////            it("Should pass nothing to next", () => {
////                const middleware = links.buildResourceLinks([
////                    "unit",
////                    "sievingLocations",
////                ]);
////                const next = (result) => {
////                    assert.strictEqual(
////                        result,
////                        undefined,
////                        "Unexpected value passed to next"
////                    );
////                    assert.strictEqual(
////                        typeof req.resourceLinks,
////                        "object",
////                        "Unexpected value in req.resourceLinks"
////                    );
////                    assert.strictEqual(
////                        typeof req.resourceLinks.unit,
////                        "object",
////                        "Unexpected value in req.resourcelinks.unit"
////                    );
////                    assert.strictEqual(
////                        typeof req.resourceLinks.sievingLocations,
////                        "object",
////                        "Unexpected value in req.resourcelinks.sievingLocations"
////                    );
////                };
////                const req = {
////                    fullURL: "test.test",
////                };
////                middleware(req, null, next);
////            });
////        });
////    });
////    describe("buildDataRelationship", () => {
////        it("Should be a function", () => {
////            assert.strictEqual(typeof links.buildDataRelationships, "function");
////        });
////        it("Should error on invalid argument", () => {
////            const test = () => links.buildDataRelationships("InValid");
////            assert.throws(test);
////        });
////        it("Should error on invalid link type", () => {
////            const test = () => links.buildDataRelationships(["inValid"]);
////            assert.throws(test);
////        });
////        it("Should error an invalid link type amounst valid ones", () => {
////            const test = () =>
////                links.buildDataRelationships(["project", "invalid", "site"]);
////            assert.throws(test);
////        });
////        it("Should return a function on success", () => {
////            assert.strictEqual(
////                typeof links.buildDataRelationships(["project"]),
////                "function"
////            );
////        });
////        describe("returned function", () => {
////            it("Should pass nothing to next", () => {
////                const middleware = links.buildDataRelationships([
////                    "project",
////                    "site",
////                ]);
////                const next = (result) => {
////                    assert.strictEqual(
////                        result,
////                        undefined,
////                        "Unexpected value passed to next"
////                    );
////                    assert.strictEqual(
////                        typeof req.dataRelationships,
////                        "object",
////                        "Unexpected value in req.dataRelationships"
////                    );
////                    assert.strictEqual(
////                        typeof req.dataRelationships.project,
////                        "function",
////                        "Unexpected value in req.dataRelationships.project"
////                    );
////                    assert.strictEqual(
////                        typeof req.dataRelationships.site,
////                        "function",
////                        "Unexpected value in req.dataRelationships.site"
////                    );
////                };
////                const req = {
////                    params: {
////                        projectid: 4,
////                    },
////                    fullURL: "test.test",
////                };
////                middleware(req, null, next);
////            });
////            describe("returned function", () => {
////                it("Should return an object with a href property and a meta property", () => {
////                    const middleware = links.buildDataRelationships([
////                        "project",
////                        "site",
////                    ]);
////                    const next = () => {
////                        const result = req.dataRelationships.project({
////                            params: {},
////                        });
////                        assert.strictEqual(typeof result, "object");
////                        assert.strictEqual(
////                            typeof result.href,
////                            "string",
////                            "Expected Href to be a string"
////                        );
////                        assert.strictEqual(
////                            typeof result.meta,
////                            "object",
////                            "Expected Meta to be an object"
////                        );
////                    };
////                    const req = {
////                        params: {
////                            projectid: 4,
////                        },
////                        fullURL: "test.test",
////                    };
////                    middleware(req, null, next);
////                });
////            });
////        });
////    });
////});
