////const jwt = require("jsonwebtoken");
////const config = require("../../config");
////const authenticate = require("../../middleware/authenticate");
////const assert = require("assert");
////
////describe("Authenticate Middleware", () => {
////    it("Should next an error if no authroization header", () => {
////        const next = (result) => {
////            assert.strictEqual(result instanceof Error, true);
////        };
////        authenticate.authenticate({ headers: {} }, null, next);
////    });
////    it("Should next an error if authorization header is wrong", () => {
////        const next = (result) => {
////            assert.strictEqual(result instanceof Error, true);
////        };
////        authenticate.authenticate(
////            {
////                headers: {
////                    authorization: "bearer Cats",
////                },
////            },
////            null,
////            next
////        );
////    });
////    it("Should next an error if jwt is expired", () => {
////        const next = (result) => {
////            assert.strictEqual(result instanceof Error, true);
////        };
////        const token = jwt.sign(
////            {
////                data: {
////                    company: "test",
////                    userid: 1,
////                },
////            },
////            config.secret,
////            {
////                expiresIn: "0h",
////            }
////        );
////        const req = {
////            headers: {
////                authorization: `bearer ${token}`,
////            },
////        };
////
////        authenticate.authenticate(req, null, next);
////    });
////    it("Should pass if jwt is correct", () => {
////        const next = (result) => {
////            assert.strictEqual(
////                result,
////                undefined,
////                "Something was passed to next"
////            );
////            assert.strictEqual(req.company, "test", "Incorrect company set");
////            assert.strictEqual(req.userID, 1, "Incorrect user id set");
////        };
////        const token = jwt.sign(
////            {
////                data: {
////                    company: "test",
////                    userid: 1,
////                },
////            },
////            config.secret,
////            {
////                expiresIn: "8h",
////            }
////        );
////        const req = {
////            headers: {
////                authorization: `bearer ${token}`,
////            },
////        };
////
////        authenticate.authenticate(req, null, next);
////    });
////});
