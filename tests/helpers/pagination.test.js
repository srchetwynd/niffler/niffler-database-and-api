////const assert = require("assert");
////const pagination = require("../../helpers/pagination");
////
////describe("pganination helper", () => {
////    describe("getPagination", function () {
////        it("Should return next if data is an array limit offset in url", function () {
////            const result = pagination.getPagination({
////                data: [1, 2, 3],
////                req: {
////                    fullURL:
////                        "https://www.digabase.com/api/project?limit=3&offset=0",
////                    limit: 3,
////                    offset: 0,
////                },
////            });
////            assert.strictEqual(
////                result.next,
////                "https://www.digabase.com/api/project?limit=3&offset=3"
////            );
////        });
////
////        it("Should not return previous if offset provided is 0", function () {
////            const result = pagination.getPagination({
////                data: [1, 2, 3],
////                req: {
////                    fullURL:
////                        "https://www.digabase.com/api/project?limit=3&offset=0",
////                    limit: 3,
////                    offset: 0,
////                },
////            });
////            assert.strictEqual(result.previous, null);
////        });
////
////        it("Should not return previous if offset would be negative", function () {
////            const result = pagination.getPagination({
////                data: [1, 2, 3],
////                req: {
////                    fullURL:
////                        "https://www.digabase.com/api/project?limit=3&offset=0",
////                    limit: 3,
////                    offset: 1,
////                },
////            });
////            assert.strictEqual(result.previous, null);
////        });
////
////        it("Should return next if data is an array no limit offset in url", function () {
////            const result = pagination.getPagination({
////                data: [1, 2, 3],
////                req: {
////                    fullURL: "https://www.digabase.com/api/project",
////                    limit: 3,
////                    offset: 1,
////                },
////            });
////            assert.strictEqual(
////                result.next,
////                "https://www.digabase.com/api/project?offset=4&limit=3"
////            );
////        });
////
////        it("Should return next if data is an array no offset in url", function () {
////            const result = pagination.getPagination({
////                data: [1, 2, 3],
////                req: {
////                    fullURL: "https://www.digabase.com/api/project?limit=3",
////                    limit: 3,
////                    offset: 3,
////                },
////            });
////            assert.strictEqual(
////                result.next,
////                "https://www.digabase.com/api/project?limit=3&offset=6"
////            );
////        });
////
////        it("Should return previous if data is an array offset in url", function () {
////            const result = pagination.getPagination({
////                data: [1, 2, 3],
////                req: {
////                    fullURL:
////                        "https://www.digabase.com/api/project?offset=6&limit=3",
////                    limit: 3,
////                    offset: 6,
////                },
////            });
////            assert.strictEqual(
////                result.previous,
////                "https://www.digabase.com/api/project?offset=3&limit=3"
////            );
////        });
////
////        it("Should not return next if data is object", () => {
////            const result = pagination.getPagination({
////                data: {},
////                req: {
////                    fullURL:
////                        "https://www.digabase.com/api/project?limit=3&offset=0",
////                },
////            });
////            assert.strictEqual(result.next, null);
////        });
////    });
////    describe("makePaginationLink", () => {
////        it("Should return links", () => {
////            const links = {
////                next: "testNext",
////                previous: "testPrevious",
////            };
////            const result = pagination.makePaginationLinks(links);
////            assert.deepStrictEqual(result, {
////                next: {
////                    href: "testNext",
////                    meta: {
////                        method: ["GET"],
////                        fields: undefined,
////                    },
////                },
////                previous: {
////                    href: "testPrevious",
////                    meta: {
////                        method: ["GET"],
////                        fields: undefined,
////                    },
////                },
////            });
////        });
////    });
////});
