require("dotenv").config();

module.exports = {
    env: process.env.ENV || "development",
    pgUser: process.env.PGUSER || "",
    pgHost: process.env.PGHOST || "",
    pgPassword: process.env.PGPASSWORD || "",
    pgDatabase: process.env.PGDATABASE || "",
    pgPort: process.env.PGPORT || "",
    pgString: process.env.DATABASE_URL || "",
    secret: process.env.SECRET || "",
    spaceKey: process.env.SPACEKEY || "",
    spaceSecret: process.env.SPACESECRET || "",
    spaceName: process.env.SPACENAME || "",
    sentryDSN: process.env.SENTRY_DSN || "",
};
