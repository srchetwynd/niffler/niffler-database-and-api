const { db } = require("../components/Database");
const fs = require("fs");

/**
 *
 */
function createMigrationsTable() {
    return db.query(`
        CREATE TABLE IF NOT EXISTS migrations (
            id SERIAL PRIMARY KEY,
            migration character varying(100) NOT NULL,
            created date DEFAULT NOW()
        )
    `);
}

/**
 *
 */
async function getRanMigrations() {
    const ranMigrations = await db.query(`
        SELECT
            *
        FROM
            migrations
    `);
    return ranMigrations.reduce((acc, el) => {
        acc.push(el.migration);
        return acc;
    }, []);
}

/**
 * @param t
 * @param name
 */
async function insertMigration(t, name) {
    return t.query(
        `
        INSERT INTO migrations
            (migration)
        VALUES
            ($1)
    `,
        [name]
    );
}

/**
 *
 */
async function main() {
    await createMigrationsTable();
    const ranMigrations = await getRanMigrations();
    const toRunMigrations = fs.readdirSync(__dirname).reduce((acc, file) => {
        if (
            file !== "migrate.js" &&
            file.match(/\.js$/) &&
            !ranMigrations.includes(file)
        ) {
            acc.push(file);
        }
        return acc;
    }, []);

    for (let i = 0; i < toRunMigrations.length; i++) {
        try {
            await db.tx(async (t) => {
                const migration = require(`./${toRunMigrations[i]}`);
                for (let i = 0; i < migration.up.length; i++) {
                    await migration.up[i](t);
                }
                await insertMigration(t, toRunMigrations[i]);
            });
        } catch (err) {
            // eslint-disable-next-line no-console
            console.log(`Error Running: ${toRunMigrations[i]}`);
            // eslint-disable-next-line no-console
            console.log(err.message);
        }
    }
}

main();
