const extensions = async (db) => {
    await db.query(`
        CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
    `);
    await db.query(`
        CREATE EXTENSION IF NOT EXISTS "postgis";
    `);
    return;
};

const tables = async (db) => {
    await db.query(`
        CREATE TABLE IF NOT EXISTS public.context_types (
            id serial PRIMARY KEY,
            context_type character varying(25) NOT NULL
        );
    `);

    await db.query(`
        CREATE TABLE IF NOT EXISTS public.degree_of_contamination (
            id serial PRIMARY KEY,
            degree character varying(10) NOT NULL
        );
    `);

    await db.query(`

        CREATE TABLE IF NOT EXISTS public.interpretations (
            id serial PRIMARY KEY,
            interpretation character varying(20) NOT NULL
        );
    `);

    await db.query(`
        CREATE TABLE IF NOT EXISTS public.lengths (
            id serial PRIMARY KEY,
            unit character varying(25) NOT NULL
        );
    `);

    await db.query(`
        CREATE TABLE IF NOT EXISTS public.metal_detecting_locations (
            id serial PRIMARY KEY,
            location character varying(10) NOT NULL
        );
    `);

    await db.query(`
        CREATE TABLE IF NOT EXISTS public.orientation (
            id serial PRIMARY KEY,
            orientation_direction character varying(3) NOT NULL
        );
    `);

    await db.query(`
        CREATE TABLE IF NOT EXISTS public.photo_types (
            id serial PRIMARY KEY,
            photo_type character varying(25) NOT NULL
        );
    `);

    await db.query(`
        CREATE TABLE IF NOT EXISTS public.sieving_locations (
            id serial PRIMARY KEY,
            location character varying(10) NOT NULL
        );
    `);
    return;
};
const inserts = async (db) => {
    // Public context_types
    await db.query(`
        INSERT INTO public.context_types (id, context_type) VALUES (1, 'Coffin');
    `);
    await db.query(`
        INSERT INTO public.context_types (id, context_type) VALUES (2, 'Cut');
    `);
    await db.query(`
        INSERT INTO public.context_types (id, context_type) VALUES (3, 'Deposit');
    `);
    await db.query(`
        INSERT INTO public.context_types (id, context_type) VALUES (4, 'Masonry');
    `);
    await db.query(`
        INSERT INTO public.context_types (id, context_type) VALUES (5, 'Skeleton');
    `);
    await db.query(`
        INSERT INTO public.context_types (id, context_type) VALUES (6, 'Timber');
    `);

    // Public degree_of_contamination
    await db.query(`
        INSERT INTO public.degree_of_contamination (id, degree) VALUES (1, 'None');
    `);
    await db.query(`
        INSERT INTO public.degree_of_contamination (id, degree) VALUES (2, 'Some');
    `);
    await db.query(`
        INSERT INTO public.degree_of_contamination (id, degree) VALUES (3, 'Heavy');
    `);

    // Public interpretation
    await db.query(`
        INSERT INTO public.interpretations (id, interpretation) VALUES (1, 'Internal');
    `);
    await db.query(`
        INSERT INTO public.interpretations (id, interpretation) VALUES (2, 'External');
    `);
    await db.query(`
        INSERT INTO public.interpretations (id, interpretation) VALUES (3, 'Structual');
    `);
    await db.query(`
        INSERT INTO public.interpretations (id, interpretation) VALUES (4, 'Other (Specify)');
    `);

    // Public Lengths
    await db.query(`
        INSERT INTO public.lengths (id, unit) VALUES (1, 'mm');
    `);
    await db.query(`
        INSERT INTO public.lengths (id, unit) VALUES (2, 'cm');
    `);
    await db.query(`
        INSERT INTO public.lengths (id, unit) VALUES (3, 'm');
    `);

    // Public Metat Detecting Locations
    await db.query(`
        INSERT INTO public.metal_detecting_locations (id, location) VALUES (1, 'In Situ');
    `);
    await db.query(`
        INSERT INTO public.metal_detecting_locations (id, location) VALUES (2, 'On Site');
    `);
    await db.query(`
        INSERT INTO public.metal_detecting_locations (id, location) VALUES (3, 'Off Site');
    `);

    // Public Orientations
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (1, 'N');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (2, 'NNE');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (3, 'NE');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (4, 'NEE');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (5, 'E');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (6, 'SEE');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (7, 'SE');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (8, 'SSE');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (9, 'S');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (10, 'SSW');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (11, 'SW');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (12, 'SWW');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (13, 'W');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (14, 'NWW');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (15, 'NW');
    `);
    await db.query(`
        INSERT INTO public.orientation (id, orientation_direction) VALUES (16, 'NNW');
    `);

    // Public photo_types
    await db.query(`
        INSERT INTO public.photo_types (id, photo_type) VALUES (1, 'Digital Black and White');
    `);
    await db.query(`
        INSERT INTO public.photo_types (id, photo_type) VALUES (2, 'Digital Colour');
    `);
    await db.query(`
        INSERT INTO public.photo_types (id, photo_type) VALUES (3, 'Film Black and White');
    `);
    await db.query(`
        INSERT INTO public.photo_types (id, photo_type) VALUES (4, 'Film Colour');
    `);

    // Sieving Locations
    await db.query(`
        INSERT INTO public.sieving_locations (id, location) VALUES (1, 'On Site');
    `);
    await db.query(`
        INSERT INTO public.sieving_locations (id, location) VALUES (2, 'Off Site');
    `);
    return;
};

module.exports.up = [extensions, tables, inserts];
