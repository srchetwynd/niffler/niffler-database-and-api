const tables = async (db) => {
    await db.query(`
        CREATE SCHEMA logging;
    `);

    await db.query(`
        CREATE TABLE IF NOT EXISTS logging.requests (
            id SERIAL PRIMARY KEY,
            method character varying(10),
            url character varying(255),
            user_agent character varying(255),
            remote_address character varying(255),
            response_time int4,
            status_code int4
        );
    `);
    return;
};

module.exports.up = [tables];
