const companyTable = (db) => {
    return db.query(`
        CREATE TABLE IF NOT EXISTS public.companies (
            id SERIAL PRIMARY KEY,
            schema character varying(50) NOT NULL,
            name character varying(255) NOT NULL,
            created date DEFAULT NOW()
        )
    `);
};

module.exports.up = [companyTable];
