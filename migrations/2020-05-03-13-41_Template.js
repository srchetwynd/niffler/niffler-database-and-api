/**
 * @param db
 */
async function getSchemas(db) {
    const schemaResults = await db.query(`
        SELECT
            *
        FROM
            public.companies
    `);
    const schemas = schemaResults.reduce((acc, el) => {
        acc.push(el.schema);
        return acc;
    }, []);
    schemas.push("template");
    return schemas;
}

const tables = async (db) => {
    const schemas = await getSchemas(db);

    return schemas.reduce(async (acc, el) => {
        await db.query(`
            CREATE SCHEMA ${el};
        `);

        await db.query(`
            CREATE TABLE ${el}.coffins (
                id serial NOT NULL,
                description character varying(250),
                discussion character varying(250),
                preservation integer,
                finds_none boolean,
                finds_breast_plate boolean,
                finds_grip boolean,
                finds_grip_plate boolean,
                finds_hinge boolean,
                finds_bracket boolean,
                finds_fixing_nails boolean,
                finds_shroud_pins boolean,
                finds_fabric boolean
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.context_above_below (
                above integer NOT NULL,
                below integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.context_in_section (
                context integer NOT NULL,
                section integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.context_same_as (
                context_1 integer NOT NULL,
                context_2 integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.contexts (
                id serial NOT NULL,
                trench integer NOT NULL,
                feature integer,
                context_type integer NOT NULL,
                recorded_by integer NOT NULL,
                checked_by integer,
                recorded_date date,
                checked_date date,
                number integer NOT NULL,
                project integer NOT NULL,
                site integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.cuts (
                id serial NOT NULL,
                shape_in_plan character varying(100),
                corners character varying(100),
                dimentions_depth character varying(100),
                break_of_slope_top character varying(100),
                sides character varying(100),
                break_of_slope_base character varying(100),
                orientation character varying(100),
                inclination_of_axis character varying(100),
                truncation character varying(100),
                comments character varying(100),
                interpretation integer,
                interpretation_comment character varying(250),
                discussion character varying(250)
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.deposits (
                id serial NOT NULL,
                compaction character varying(100),
                colour character varying(100),
                composition character varying(100),
                inclusions character varying(100),
                thickness character varying(100),
                comments character varying(100),
                weather character varying(100),
                method character varying(100),
                interpretation integer,
                interpretation_comment character varying(100),
                discussion character varying(250),
                finds_none boolean,
                finds_pot boolean,
                finds_bone boolean,
                finds_glass boolean,
                finds_metal boolean,
                finds_cbm boolean,
                finds_other_bm boolean,
                finds_wood boolean,
                finds_leather boolean,
                finds_other character varying(100),
                metal_detecting integer,
                finds_sieving integer
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.drawing_contexts (
                drawing integer NOT NULL,
                context integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.drawings (
                id serial NOT NULL,
                number character varying(20) NOT NULL,
                project integer NOT NULL,
                site integer NOT NULL,
                trench integer NOT NULL,
                drawn_by integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.features (
                id serial NOT NULL,
                site integer NOT NULL,
                project integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.masonry (
                id serial NOT NULL,
                materials character varying(100),
                size_materials character varying(100),
                finish character varying(100),
                bond character varying(100),
                form character varying(100),
                direction character varying(100),
                bonding_material character varying(100),
                dimensions character varying(100),
                other character varying(250),
                interpretation integer,
                interpretation_comment character varying(250),
                worked_stones character varying(100)
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.photo_context (
                photo integer NOT NULL,
                context integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.photos (
                id serial NOT NULL,
                photo_type integer,
                roll integer,
                frame character varying,
                direction integer,
                date date,
                description character varying(100),
                "user" integer,
                landscape boolean,
                camera character varying(100),
                trench integer NOT NULL,
                site integer NOT NULL,
                project integer NOT NULL,
                storage_id uuid DEFAULT public.uuid_generate_v4()
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.points (
                id serial NOT NULL,
                project integer NOT NULL,
                site integer NOT NULL,
                trench integer NOT NULL,
                context integer,
                point public.geometry NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.polygons (
                id serial NOT NULL,
                project integer NOT NULL,
                site integer NOT NULL,
                trench integer NOT NULL,
                context integer,
                polygon public.geometry NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.polylines (
                id serial NOT NULL,
                project integer NOT NULL,
                site integer NOT NULL,
                trench integer NOT NULL,
                context integer,
                polyline public.geometry NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.projects (
                id serial NOT NULL,
                name character varying(50) NOT NULL,
                code character varying(25) NOT NULL,
                description character varying(500) NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.samples (
                id serial NOT NULL,
                context integer NOT NULL,
                number integer,
                percentage integer,
                dimention_length integer,
                dimention_width integer,
                dimention_height integer,
                dimention_unit integer,
                size_litres integer,
                number_of_buckets integer,
                excavation_method character varying(100),
                conditions character varying(100),
                taken_from integer,
                contamination_modern integer,
                contamination_other_deposits integer,
                inclusions_bone boolean,
                inclusions_ceramic boolean,
                inclusions_wood boolean,
                inclusions_organic boolean,
                inclusions_other character varying(100),
                date date,
                user_taken_sample integer,
                trench integer NOT NULL,
                site integer NOT NULL,
                project integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.sections (
                id serial NOT NULL,
                trench integer NOT NULL,
                drawing integer NOT NULL,
                site integer NOT NULL,
                project integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.sites (
                id serial NOT NULL,
                project integer NOT NULL,
                number integer NOT NULL,
                location character varying(100) NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.skeletons (
                id serial NOT NULL,
                orientation integer,
                body character varying(100),
                head character varying(100),
                right_arm_hand character varying(100),
                left_arm_hand character varying(100),
                right_leg character varying(100),
                left_leg character varying(100),
                feet character varying(100),
                degeneration_in_situ character varying(100),
                degeneration_post_lift character varying(100),
                comments character varying(100),
                coffin integer,
                grave integer
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.timbers (
                id serial NOT NULL,
                timber_type character varying(100),
                setting character varying(100),
                orientation character varying(100),
                cross_section character varying(100),
                condition character varying(100),
                dimensions_length real,
                dimensions_width real,
                dimensions_height real,
                dimensions_units integer,
                conversion character varying(100),
                tool_marks character varying(100),
                joints_fixings character varying(100),
                intentional_marks character varying(100),
                surface_treatment character varying(100),
                comments character varying(250),
                methods character varying(100),
                interpretation character varying(250),
                reuse character varying(100),
                bark boolean,
                sapwood boolean,
                knotty boolean,
                straight_grained boolean
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.trenchs (
                id serial NOT NULL,
                site integer NOT NULL,
                number integer NOT NULL,
                project integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.user_permissions (
                "user" integer NOT NULL,
                site integer NOT NULL
            );
        `);

        await db.query(`
            CREATE TABLE ${el}.users (
                id serial NOT NULL,
                email_address character varying NOT NULL,
                password character varying NOT NULL
            );
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.coffins
                ADD CONSTRAINT coffins_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.context_above_below
                ADD CONSTRAINT context_above_below_pk PRIMARY KEY (above, below);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.context_in_section
                ADD CONSTRAINT context_in_section_pk PRIMARY KEY (section, context);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.context_same_as
                ADD CONSTRAINT context_same_as_pk PRIMARY KEY (context_1, context_2);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.contexts
                ADD CONSTRAINT contexts_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.cuts
                ADD CONSTRAINT cuts_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.deposits
                ADD CONSTRAINT deposits_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.drawing_contexts
                ADD CONSTRAINT drawing_contexts_pk PRIMARY KEY (drawing, context);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.drawings
                ADD CONSTRAINT drawings_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.features
                ADD CONSTRAINT features_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.masonry
                ADD CONSTRAINT masonry_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photo_context
                ADD CONSTRAINT photo_context_pk PRIMARY KEY (context, photo);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photos
                ADD CONSTRAINT photo_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.points
                ADD CONSTRAINT points_pkey PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polygons
                ADD CONSTRAINT polygons_pkey PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polylines
                ADD CONSTRAINT polylines_pkey PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.projects
                ADD CONSTRAINT projects_pkey PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT samples_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.sections
                ADD CONSTRAINT sections_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.sites
                ADD CONSTRAINT sites_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.skeletons
                ADD CONSTRAINT skeletons_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.timbers
                ADD CONSTRAINT timbers_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.trenchs
                ADD CONSTRAINT trenchs_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.user_permissions
                ADD CONSTRAINT user_permissions_pk PRIMARY KEY ("user", site);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.users
                ADD CONSTRAINT users_pk PRIMARY KEY (id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.users
                ADD CONSTRAINT users_un UNIQUE (email_address);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.context_above_below
                ADD CONSTRAINT above_fk FOREIGN KEY (above) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.context_above_below
                ADD CONSTRAINT below_fk FOREIGN KEY (below) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.contexts
                ADD CONSTRAINT checked_by_fk FOREIGN KEY (checked_by) REFERENCES ${el}.users(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.skeletons
                ADD CONSTRAINT coffin_fk FOREIGN KEY (coffin) REFERENCES ${el}.coffins(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.coffins
                ADD CONSTRAINT coffins_fk FOREIGN KEY (id) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT contamination_modern_fk FOREIGN KEY (contamination_modern) REFERENCES public.degree_of_contamination(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT contamination_other_deposits_fk FOREIGN KEY (contamination_other_deposits) REFERENCES public.degree_of_contamination(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.context_same_as
                ADD CONSTRAINT context_1_fk FOREIGN KEY (context_1) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.context_same_as
                ADD CONSTRAINT context_2_fk FOREIGN KEY (context_2) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.points
                ADD CONSTRAINT context_fk FOREIGN KEY (context) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polygons
                ADD CONSTRAINT context_fk FOREIGN KEY (context) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polylines
                ADD CONSTRAINT context_fk FOREIGN KEY (context) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.context_in_section
                ADD CONSTRAINT context_in_section_fk FOREIGN KEY (context) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.context_in_section
                ADD CONSTRAINT context_in_section_fk_1 FOREIGN KEY (section) REFERENCES ${el}.sections(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.contexts
                ADD CONSTRAINT contexts_fk FOREIGN KEY (trench) REFERENCES ${el}.trenchs(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.skeletons
                ADD CONSTRAINT cut_fk FOREIGN KEY (grave) REFERENCES ${el}.cuts(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.cuts
                ADD CONSTRAINT cuts_fk FOREIGN KEY (id) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.deposits
                ADD CONSTRAINT deposits_fk FOREIGN KEY (id) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photos
                ADD CONSTRAINT direction_fk FOREIGN KEY (direction) REFERENCES public.orientation(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.drawing_contexts
                ADD CONSTRAINT drawing_contexts_fk FOREIGN KEY (context) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.drawing_contexts
                ADD CONSTRAINT drawing_contexts_fk_1 FOREIGN KEY (drawing) REFERENCES ${el}.drawings(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.sections
                ADD CONSTRAINT drawing_fk FOREIGN KEY (id) REFERENCES ${el}.drawings(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.contexts
                ADD CONSTRAINT feature_fk FOREIGN KEY (feature) REFERENCES ${el}.features(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.features
                ADD CONSTRAINT features_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.cuts
                ADD CONSTRAINT interpretation_fk FOREIGN KEY (interpretation) REFERENCES public.interpretations(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.deposits
                ADD CONSTRAINT interpretation_fk FOREIGN KEY (interpretation) REFERENCES public.interpretations(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.masonry
                ADD CONSTRAINT interpretation_fk FOREIGN KEY (interpretation) REFERENCES public.interpretations(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.masonry
                ADD CONSTRAINT masonry_fk FOREIGN KEY (id) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.deposits
                ADD CONSTRAINT "metal-detecting_fk" FOREIGN KEY (metal_detecting) REFERENCES public.metal_detecting_locations(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.skeletons
                ADD CONSTRAINT orientation_fk FOREIGN KEY (orientation) REFERENCES public.orientation(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photo_context
                ADD CONSTRAINT photo_context_fk FOREIGN KEY (photo) REFERENCES ${el}.photos(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photo_context
                ADD CONSTRAINT photo_context_fk_1 FOREIGN KEY (context) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.features
                ADD CONSTRAINT project_fk FOREIGN KEY (project) REFERENCES ${el}.projects(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.contexts
                ADD CONSTRAINT project_fk FOREIGN KEY (project) REFERENCES ${el}.projects(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.drawings
                ADD CONSTRAINT project_fk FOREIGN KEY (project) REFERENCES ${el}.projects(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photos
                ADD CONSTRAINT project_fk FOREIGN KEY (project) REFERENCES ${el}.projects(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT project_fk FOREIGN KEY (project) REFERENCES ${el}.projects(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.points
                ADD CONSTRAINT project_fk FOREIGN KEY (project) REFERENCES ${el}.projects(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polygons
                ADD CONSTRAINT project_fk FOREIGN KEY (project) REFERENCES ${el}.projects(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polylines
                ADD CONSTRAINT project_fk FOREIGN KEY (project) REFERENCES ${el}.projects(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.contexts
                ADD CONSTRAINT recorded_by_fk FOREIGN KEY (recorded_by) REFERENCES ${el}.users(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT samples_fk FOREIGN KEY (context) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.sections
                ADD CONSTRAINT sections_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.sections
                ADD CONSTRAINT sections_fk_1 FOREIGN KEY (project) REFERENCES ${el}.projects(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.deposits
                ADD CONSTRAINT "sieving-location_fk" FOREIGN KEY (finds_sieving) REFERENCES public.sieving_locations(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.contexts
                ADD CONSTRAINT site_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.drawings
                ADD CONSTRAINT site_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photos
                ADD CONSTRAINT site_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT site_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.points
                ADD CONSTRAINT site_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polygons
                ADD CONSTRAINT site_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polylines
                ADD CONSTRAINT site_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.sites
                ADD CONSTRAINT sites_fk FOREIGN KEY (project) REFERENCES ${el}.projects(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.skeletons
                ADD CONSTRAINT skeletons_fk FOREIGN KEY (id) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT taken_from_fk FOREIGN KEY (taken_from) REFERENCES ${el}.contexts(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.timbers
                ADD CONSTRAINT timbers_fk FOREIGN KEY (id) REFERENCES ${el}.contexts(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.sections
                ADD CONSTRAINT trench_fk FOREIGN KEY (trench) REFERENCES ${el}.trenchs(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photos
                ADD CONSTRAINT trench_fk FOREIGN KEY (trench) REFERENCES ${el}.trenchs(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT trench_fk FOREIGN KEY (trench) REFERENCES ${el}.trenchs(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.points
                ADD CONSTRAINT trench_fk FOREIGN KEY (trench) REFERENCES ${el}.trenchs(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polygons
                ADD CONSTRAINT trench_fk FOREIGN KEY (trench) REFERENCES ${el}.trenchs(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.polylines
                ADD CONSTRAINT trench_fk FOREIGN KEY (trench) REFERENCES ${el}.trenchs(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.drawings
                ADD CONSTRAINT trench_fk_1 FOREIGN KEY (trench) REFERENCES ${el}.trenchs(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.trenchs
                ADD CONSTRAINT trenchs_fk FOREIGN KEY (site) REFERENCES ${el}.sites(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.contexts
                ADD CONSTRAINT type_fk FOREIGN KEY (context_type) REFERENCES public.context_types(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photos
                ADD CONSTRAINT type_fk FOREIGN KEY (photo_type) REFERENCES public.photo_types(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.timbers
                ADD CONSTRAINT units_fk FOREIGN KEY (dimensions_units) REFERENCES public.lengths(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT units_fk FOREIGN KEY (dimention_unit) REFERENCES public.lengths(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.drawings
                ADD CONSTRAINT user_fk FOREIGN KEY (drawn_by) REFERENCES ${el}.users(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.photos
                ADD CONSTRAINT user_fk_1 FOREIGN KEY ("user") REFERENCES ${el}.users(id);
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.user_permissions
                ADD CONSTRAINT user_permissions_fk FOREIGN KEY ("user") REFERENCES ${el}.users(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.user_permissions
                ADD CONSTRAINT user_permissions_fk_1 FOREIGN KEY (site) REFERENCES ${el}.sites(id) ON DELETE CASCADE;
        `);

        await db.query(`
            ALTER TABLE ONLY ${el}.samples
                ADD CONSTRAINT user_taken_fk FOREIGN KEY (user_taken_sample) REFERENCES ${el}.users(id);
        `);
        acc.push("Done");
        return acc;
    }, []);
};

module.exports.up = [tables];
