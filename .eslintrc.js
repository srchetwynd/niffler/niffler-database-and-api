module.exports = {
    env: {
        commonjs: true,
        es6: true,
        node: true,
        mocha: true,
    },
    plugins: ["prettier", "class-property", "jsdoc"],
    extends: ["eslint:recommended", "plugin:jsdoc/recommended"],
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    parserOptions: {
        ecmaVersion: 2018,
    },
    rules: {
        "prettier/prettier": "error",
        "no-console": "error",
        complexity: ["error", 15],
        indent: ["error", 4],
        "linebreak-style": ["error", "unix"],
        quotes: ["error", "double"],
        semi: ["error", "always"],
    },
};
